srcs=$(wildcard src/*.c)

obj_dir=.obj
objs=$(addprefix $(obj_dir)/, $(srcs:.c=.o))

cc=gcc
extra_dbg_flags=-rdynamic -lunwind -ldw
warning_flags=-Wall -Wextra -Wpedantic -Werror -Wno-pointer-sign -Wno-unused-parameter
flags=-std=c17 -g $(warning_flags) $(extra_dbg_flags)
#flags=-std=c17 -g -Og -Wall -Wextra -Wpedantic -Werror -Wno-pointer-sign
exec=cyb


$(obj_dir):
	mkdir -p $(obj_dir)/src

$(obj_dir)/%.o: %.c $(obj_dir) 
	$(cc) -o $@ -c $< $(flags)

$(exec): $(objs)
	$(cc) $^ $(flags) -o $@

all: $(exec)

tests=argc arg_order array binary_ops data fn_call for if lib \
    local_var logical print_num stdin struct sys_write var_sizes while
tests_execs=$(addprefix tests/test_, $(tests))
run_tests: $(exec)
	@$(foreach test, $(tests), \
        echo "compiling 'tests/test_$(test).cyb':"; \
		./$(exec) tests/test_$(test).cyb -o tests/test_$(test); \
	)
	@gcc tools/run_tests.c -o $(obj_dir)/run_tests -Wall -Werror
	@$(obj_dir)/run_tests

clean:
	rm -rf $(objs) $(obj_dir) $(exec) $(tests_execs)

.PHONY: all clean cyb run_tests
