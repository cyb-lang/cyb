#pragma once

#include "common.h"

#include <stdint.h>

// A dynamic array that grows itself when needed. the API is explaining in the comment below:
// - it must be initialized, using `create_list`, before being used for anything.
//   (lists created via `new_list_from_array` or `create_new_copy` don't need further initilization)
//   (note that a list contains some extra internal fields that detect if the it is being used
//    while not initialized, and reports that error via a crash)
//
// - some usage examples are at the bottom of this comment
//
// the following struct fields are available to be used:
// - `capacity`: the amount of items the list can hold without needing to allocate more memory.
//          (read-only! to increase the capacity use the `ensure_capacity` function)
// - `length`: how many items are actually stored in the list
//          (read-only! this updated when elements are added/removed from the list)
// - `pos`: can be usefull for using a list as a buffer. the internal code never touches this.
// - `data-internal`: pointer to data.
//          this pointer is not stable! it can change when adding elements or capacity.
//          (read-only! only use if you need to read the whole list data in one go)
//
// the following functions/macros are available:
// - `void create_list(type)`
//              create and initialize a list
// - `list_from_array(type, array_ptr, array_size)`
//              like `create_list` but alloc and copy elements from an array right after init
// - `List create_new_copy(List* src)'
//              copy over all the element of `src` into a newly allocated and initialized list
// - `void ensure_capacity(List* list, size_t new_capacity)`
//              make sure list can hold `new_capacity` items and allocate more space if needed
// - `void reserve(List* list, size_t n_elems_to_reserve)`
//              increase `length` by `n_elems_to_reserve`. space is allocated if needed
// - `append(list, elem)`
//              append one item, allocating if needed.
//              `elem` cannot be an expression! because we're taking its pointer.
//              (for e.g. instead of `append(&list, 12);` you must do `int var = 12; append(&list, var);`)
// - `void append_list(List* list, List* other_list)`
//              append a whole `other_list` into `list`. allocates if needed.
//              `other_list` is never changed.
// - `at_index(list, index, type)`
//              get item at index `index`. can be used as if it was a normal array indexing
//              `at_index(...) = var;`, `ptr = &at_index(...)`, `at_index(...).field` all work
// - `indirect_index(list_ptr, index_list_ptr, index_list_idx, type)`
//              given two lists, `list_ptr` and `index_list_ptr` return `at_index` of `list_ptr`
//              using `index_list_ptr` as the index. if instead of list we had array it would
//              be the equivalent of doing: `list[index_list[idx]]`
// - `pop_back(list, type)`
//              remove the last item on the list (`length` is decreased) and return it
//              example: `char last_char = pop_back(&char_list, char);`
// - `void remove_index(List* list, size_t index)`
//              remove item at index `index` and move all following items back
//              one slot so the list remains contiguous
// - `void clear_list(List* list)`
//              set `length` and all items to 0, but don't free any memory.
//              the list can used again without needing to re-initialize.
// - `void free_list(List* list)`
//              free all resources used by the list. using the list after this
//              is essentially the same as a `user-after-free` bug.
//
// usage examples:
//
// List offsets = create_list(uint32_t);
// // add values using append
// uint32_t first_value = 0xdeadbeef;
// append(&offsets, first_value);
//
// // read and add items via at_index
// uint32_t first = at_index(&offsets, 0);
// ensure_capacity(&offsets, 2);
// at_index(&offset, 1) = first;
// uint32_t first_ptr = &at_index(&offsets, 0);
// *first_ptr = 0xbaddecaf;
//
// // copy some elements from an array
// uint32_t off_array[4] = {0, 1, 2, 3};
// List hardcoded_offs = list_from_array(uint32_t, off_array, 4);
//
// // join the lists
// append_list(offsets, hardcoded_offs);
// free_list(hardcoded_offs);
//
// // remove the last value
// uint32_t last_value = pop_back(offsets);
//
// clear_list(&offsets);
// assert(offset.length == 0);
// assert(offset.capacity >= 0);

typedef struct {
    size_t capacity, length;
    size_t size_of_elem;

    size_t pos; // usefull in case we want go through the list

    uint8_t* data_internal; // don't use this to index, use at_index macro instead

    // for debug purposes only
    const char* debug_type_name_internal;
} List;

List new_list_from_pointer_internal(size_t elem_size, const char* type_name, void* ptr, size_t length);
#define create_list(type) \
    new_list_from_pointer_internal(sizeof(type), #type, NULL, 0)
#define list_from_array(type, array_ptr, array_size) \
    new_list_from_pointer_internal(sizeof(type), #type, array_ptr, array_size)

// make a new list with new allocated memory and copy over all the elements from the `src` list
List create_new_copy(List* src);

void ensure_capacity(List* list, size_t new_capacity);

// adds `n_elems_to_reserve` to the list length (and makes sure capacity can handle it)
void reserve(List* list, size_t n_elems_to_reserve);

void append_internal(List* list, void* elem_ptr);
#define append(list, elem) append_internal(list, &elem)

void append_list(List* list, List* other_list); // add other_list to list

void* checked_index_internal(List* list, size_t index, size_t type_size, const char* type_name);
#define at_index(list, index, type) ( ((type*) checked_index_internal(list, index, sizeof(type), #type))[index] )

#define indirect_index(list_ptr, index_list_ptr, index_list_idx, type) \
    at_index(list_ptr, at_index(index_list_ptr, index_list_idx, size_t), type)

void* pop_back_internal(List* list, size_t type_size, const char* type_name);
#define pop_back(list, type) ( *((type*) pop_back_internal(list, sizeof(type), #type)) )

// remove elem at index `index` and move all subsequent elements back one place.
void remove_index(List* list, size_t index);

// empty the list (i.e. length=0, zero out all the data) but keep the capacity
void clear_list(List* list);

void free_list(List* list);
