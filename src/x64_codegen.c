#include "x64_codegen.h"

void write_x64_rex(Buffer* buf, bool w, bool r, bool x, bool b) {
    buf->data[buf->pos++] = 0x40 | (w ? 0x8:0) | (r ? 0x4:0) | (x ? 0x2:0) | (b ? 0x1:0);
}
void write_x64_modrm(Buffer* buf, uint8_t mod, uint8_t reg, uint8_t rm) {
    mod %= 4; reg %= 8; rm %= 8;
    buf->data[buf->pos++] = (mod << 6) | (reg << 3) | rm;
}
// this has the exact same pattern as modrm, but I'm doing separate function for clarity
void write_x64_sib(Buffer* buf, uint8_t scale, uint8_t index, uint8_t base) {
    debug_assert(index != 7, "index = 0b100 in SIB is illegal\n");
    write_x64_modrm(buf, scale, index, base);
}

void write_x64_movabs2reg(Buffer* buffer, uint64_t imm, uint8_t reg) {
    write_x64_rex(buffer, 1, 0, 0, reg >= 8);
    buffer->data[buffer->pos++] = 0xb8 + (reg % 8);
    *((uint64_t*) (buffer->data + buffer->pos)) = imm;
    buffer->pos += 8;
}

void write_x64_jmp_rel32(Buffer* buffer, int32_t offset) {
    buffer->data[buffer->pos++] = 0xe9;
    *((int32_t*) (buffer->data + buffer->pos)) = offset;
    buffer->pos += 4;
}

void patch_jmp(Buffer* buffer, size_t patch_pos, size_t jmp_target) {
    size_t saved_bufpos = buffer->pos;

    buffer->pos = patch_pos;
    write_x64_jmp_rel32(buffer, jmp_target - (patch_pos + 5));

    buffer->pos = saved_bufpos;
}

// unlike a normal write_x64 function this does not change the buffer pos
// as it is patching already written assembly
void patch_call(Buffer* buffer, size_t patch_pos, int32_t call_target) {
    int32_t offset = call_target - (patch_pos + 5);
    buffer->data[patch_pos] = 0xe8; // call rel32 opcode
    *((int32_t*) (buffer->data + patch_pos + 1)) = offset;
}

// the main difference between jmp greater and jmp above is that
// jg checks the sign flags while ja checks the carry flag
// so if comparing two unsigned numbers we should use jmp above
// and jmp greater for signed integers
void write_x64_jcc_rel32(Buffer* buffer, uint8_t opcode, int32_t offset) {
    buffer->data[buffer->pos++] = 0x0f;
    buffer->data[buffer->pos++] = opcode;
    *((int32_t*) (buffer->data + buffer->pos)) = offset;
    buffer->pos += 4;
}
// opcodes for jcc
// 80 OF=1
// 81 OF=0
// 82 below
// 83 above/eql
// 84 eql
// 85 neql
// 86 below/eql
// 87 above
// 88 SF=1
// 89 SF=0
// 8a PF=1
// 8b PF=0
// 8c less
// 8d greater/eql
// 8e less/eql
// 8f greater
void write_jmp_conditional(Buffer* buffer, TokenType op, int32_t offset) {
    uint8_t opcode = 0x90;
    switch (op) {
    case Token_smaller: opcode = 0x83; break; // jmp above or equal
    case Token_smaller_equal: opcode = 0x87; break; // jmp above
    case Token_greater: opcode = 0x86; break; // jmp below or equal
    case Token_greater_equal: opcode = 0x82; break; // jmp below 
    case Token_is_equal: opcode = 0x85; break; // jmp not equal
    case Token_different: opcode = 0x84; break; // jmp equal

    default: debug_crash("token '%s' not permitted\n", token_type2str[op]);
    }
    write_x64_jcc_rel32(buffer, opcode, offset);
}
void patch_jmp_conditional(Buffer* buffer, size_t patch_pos, TokenType op, size_t jmp_target) {
    size_t saved_bufpos = buffer->pos;
    buffer->pos = patch_pos;
    write_jmp_conditional(buffer, op, jmp_target - (patch_pos + 6));
    buffer->pos = saved_bufpos;
}

void write_x64_Instr(Buffer* buf, x64_Instr ins) {
    //size_t saved_pos = buf->pos;

    //debug_print("x64_Instr {\n"
    //    "\t.has_0f_prefix=%u, .has_op_size_prefix=%u, .op_size_prefix=0x%x \n"
    //    "\t.has_rex=%u, .w=%u, .r=%u, .x=%u, .b=%u, \n"
    //    "\t.opcode=0x%x, \n"
    //    "\t.has_modrm=%u, .mod=%u, .reg=%u, .rm=%u, \n"
    //    "\t.has_sib=%u, .scale=%u, .index=%u, .base=%u, \n"
    //    "\t.has_disp=%u, .disp_size=%u, .disp=%ld, \n"
    //    "\t.has_imm=%u, .imm_size=%u, .imm=0x%lx, \n"
    //    "};\n",
    //    ins.has_0f_prefix, ins.has_op_size_prefix, ins.op_size_prefix,
    //    ins.has_rex, ins.w, ins.r, ins.x, ins.b,
    //    ins.opcode,
    //    ins.has_modrm, ins.mod, ins.reg, ins.rm,
    //    ins.has_sib, ins.scale, ins.index, ins.base,
    //    ins.has_disp, ins.disp_size, ins.disp,
    //    ins.has_imm, ins.imm_size, ins.imm
    //);

    if (ins.has_op_size_prefix) buf->data[buf->pos++] = ins.op_size_prefix;
    if (ins.has_rex) write_x64_rex(buf, ins.w, ins.r, ins.x, ins.b);
    if (ins.has_0f_prefix) buf->data[buf->pos++] = 0x0f;
    buf->data[buf->pos++] = ins.opcode;
    if (ins.has_modrm) write_x64_modrm(buf, ins.mod, ins.reg, ins.rm);
    if (ins.has_sib) write_x64_sib(buf, ins.scale, ins.index, ins.base);
    if (ins.has_disp) {
        switch (ins.disp_size) {
            case 1:
                *((int8_t*) (buf->data + buf->pos)) = (int8_t) ins.disp; buf->pos += 1;
                break;
            case 2:
                *((int16_t*) (buf->data + buf->pos)) = (int16_t) ins.disp; buf->pos += 2;
                break;
            case 4:
                *((int32_t*) (buf->data + buf->pos)) = (int32_t) ins.disp; buf->pos += 4;
                break;
            case 8:
                *((int64_t*) (buf->data + buf->pos)) = (int64_t) ins.disp; buf->pos += 8;
                break;
            default:
                debug_crash("can't write disp with size %u\n", ins.disp_size);
        }
    }
    if (ins.has_imm) {
        switch (ins.imm_size) {
            case 1:
                *((uint8_t*) (buf->data + buf->pos)) = (uint8_t) ins.imm; buf->pos += 1;
                break;
            case 2:
                *((uint16_t*) (buf->data + buf->pos)) = (uint16_t) ins.imm; buf->pos += 2;
                break;
            case 4:
                *((uint32_t*) (buf->data + buf->pos)) = (uint32_t) ins.imm; buf->pos += 4;
                break;
            case 8:
                *((uint64_t*) (buf->data + buf->pos)) = (uint64_t) ins.imm; buf->pos += 8;
                break;
            default:
                debug_crash("can't write imm with byte size %u\n", ins.imm_size);
        }
    }

    //debug_print("imm/disp sizes (%u/%u) wrote bytes: ", ins.imm_size, ins.disp_size);
    //for (size_t i = saved_pos; i < buf->pos; i++) { printf("0x%02x ", buf->data[i]); }
    //printf("\n");
}

typedef struct {
    uint8_t base, mod_reg;
    bool no_d_bit;
    bool no_imm_support;
    uint8_t base_imm, mod_reg_imm;
    bool defaults_to_64;
    bool has_0f_prefix;
    bool cant_address_high_byte;
    OperandLoc force_src, force_dst;
    uint8_t n_ops; // number of operands
} OpcodeInfo; 
// this table must be in the same order as the OpMnemonic enum
OpcodeInfo opcode_table[] = {
    { .base = 0x00,               .base_imm = 0x80,                   .n_ops = 2 }, // add
    { .base = 0x28,               .base_imm = 0x80, .mod_reg_imm = 5, .n_ops = 2 }, // sub

    { .base = 0x20,               .base_imm = 0x80, .mod_reg_imm = 4, .n_ops = 2 }, // and
    { .base = 0x30,               .base_imm = 0x80, .mod_reg_imm = 6, .n_ops = 2 }, // xor

    { .base = 0x88,               .base_imm = 0xc6,                   .n_ops = 2 }, // mov

    { .base = 0x44, .no_d_bit = true, .no_imm_support = true,
        .has_0f_prefix = true, .force_dst = Register,                 .n_ops = 2 }, // mov eql
    { .base = 0x45, .no_d_bit = true, .no_imm_support = true,
        .has_0f_prefix = true, .force_dst = Register,                 .n_ops = 2 }, // mov not eql

    { .base = 0x8d, .no_d_bit = true, .no_imm_support = true,
        .force_src = Memory, .force_dst = Register,                   .n_ops = 2 }, // lea

    { .base = 0xf6, .mod_reg = 4, .no_imm_support = true,             .n_ops = 1 }, // mul
    { .base = 0xf6, .mod_reg = 6, .no_imm_support = true,             .n_ops = 1 }, // div

    { .base = 0xfe, .mod_reg = 0, .no_imm_support = true,             .n_ops = 1 }, // inc 
    { .base = 0xfe, .mod_reg = 1, .no_imm_support = true,             .n_ops = 1 }, // dec 
    { .base = 0xf6, .mod_reg = 3, .no_imm_support = true,             .n_ops = 1 }, // neg

    { .base = 0x38,               .base_imm = 0x80, .mod_reg_imm = 7, .n_ops = 2 }, // cmp

    { .base = 0x90, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set overflow
    { .base = 0x91, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set not overflow
    { .base = 0x92, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set blw
    { .base = 0x93, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set carry
    { .base = 0x94, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set eql
    { .base = 0x95, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set not eql
    { .base = 0x96, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set blw eql
    { .base = 0x97, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set abv
    { .base = 0x98, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set sign
    { .base = 0x99, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set not sign
    { .base = 0x9a, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set parity
    { .base = 0x9b, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set not parity
    { .base = 0x9c, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set less
    { .base = 0x9d, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set not less
    { .base = 0x9e, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set less eql
    { .base = 0x9f, .has_0f_prefix = true,
        .no_imm_support = true, .no_d_bit = true,                   .n_ops = 1 }, // set greater

    { .base = 0xff,                                                                 // push
        // for push imm we use 0x68 so we default to 32bit imm
        .mod_reg = 6, .base_imm = 0x68,       .defaults_to_64 = true,
        .cant_address_high_byte = true,                               .n_ops = 1 },
    { .base = 0x8f,                                                                 // pop
        .mod_reg = 0, .no_imm_support = true, .defaults_to_64 = true,
        .cant_address_high_byte = true,                               .n_ops = 1 },

    { .base = 0xff, .mod_reg = 4, .base_imm = 0xe9,
                            .no_d_bit = true, .defaults_to_64 = true, .n_ops = 1 }, // jmp

    { .base = 0xc3,                                                   .n_ops = 0 }, // ret
    { .base = 0x05, .has_0f_prefix = true,                            .n_ops = 0 }, // syscall
};


x64_Instr configure_indexing(x64_Instr ins, uint8_t* rm_operand, IndexingInfo* indexing) {
    // rbp/r13 need to use SIB w/ disp=0 even for normal [reg] addressing
    if (*rm_operand % 8 == Reg_rbp && !indexing->use_simple && !indexing->use_complex) {
        indexing->use_simple = true; indexing->disp_size = 1;
    }

    if (indexing->use_simple) {
        debug_assert(!indexing->use_complex && !indexing->use_rip_relative, "\n");

        ins.has_disp = true;
        ins.disp_size = indexing->disp_size;

        if (ins.disp_size == 1) { ins.mod = 1; }
        else if (ins.disp_size == 4) { ins.mod = 2; }
        else { debug_crash("invalid disp_size=%d\n", ins.disp_size); }

        // because using rsp/r12 as the ModRM.rm field is how we specifiy
        // that we're going to use SIB, if we *actually* want to use these
        // registers as base for simple indexing, we also need a SIB byte
        // to disambiguate between these cases.
        if (*rm_operand % 8 == Reg_rsp) {
            ins.has_sib = true;
            indexing->index = 4;
        }
    }
    else if (indexing->use_complex) {
        debug_assert(!indexing->use_simple && !indexing->use_rip_relative, "\n");

        ins.base = *rm_operand % 8;
        ins.has_sib = true;

        ins.disp_size = indexing->disp_size;
        ins.has_disp = (ins.disp_size != 0);
        // to use rbp/r13 as the base a disp is always required. so we use 0.
        if (*rm_operand % 8 == Reg_rbp && indexing->disp_size == 0) {
            ins.disp_size = 1;
            ins.disp = 0;
            ins.has_disp = true;
        }

        if (ins.disp_size == 1) { ins.mod = 1; }
        else if (ins.disp_size == 4) { ins.mod = 2; }

        *rm_operand = 4 | (*rm_operand & 0x08);
    }
    else if (indexing->use_rip_relative) {
        debug_assert(!indexing->use_simple && !indexing->use_complex, "\n");

        ins.mod = 0;
        debug_assert(indexing->disp_size <= 4, "\n");
        ins.disp = indexing->disp;
        ins.disp_size = 4;
        ins.has_disp = true;
        *rm_operand = Reg_rbp;
    }
    else {
        ins.mod = 0;

        if (*rm_operand % 8 == Reg_rsp) {
            ins.has_sib = true;
            indexing->index = Reg_rsp;
        }
    }

    return ins;
}

void configure_ins_rex(x64_Instr* ins, uint8_t op_size, uint8_t reg_op, uint8_t index, uint8_t rm_op) {
    ins->w = (op_size == 64);
    ins->r = reg_op > 7;
    ins->x = index > 7;
    ins->b = rm_op > 7;

    ins->has_rex = (ins->w || ins->r || ins->x || ins->b);
}

void configure_ins_modrm(x64_Instr* ins, uint8_t reg_op, uint8_t rm_op) {
    ins->has_modrm = true;
    ins->reg = reg_op % 8;
    ins->rm = rm_op % 8;
}

void configure_ins_sib(x64_Instr* ins, IndexingInfo idx, uint8_t rm_op) {
    ins->scale = idx.scale;
    ins->index = idx.index % 8;
    ins->base = rm_op % 8;
    ins->disp = idx.disp;
}

void configure_ins_sib_raw(x64_Instr* ins, uint8_t scale, uint8_t index, uint8_t base) {
    ins->scale = scale;
    ins->index = index % 8;
    ins->base = base % 8;
}

void write_x64_op0(Buffer* buf, OpMnemonic op) {
    OpcodeInfo base = opcode_table[op];
    debug_assert(base.n_ops == 0, "\n");

    x64_Instr ins = {0};

    ins.opcode = base.base;
    ins.has_0f_prefix = base.has_0f_prefix;

    write_x64_Instr(buf, ins);
}

void write_x64_op1(Buffer* buf, OpMnemonic op, OperandType src, uint8_t operand_size, IndexingInfo indexing) {
    //debug_print(
    //    "[0x%04lx] %s: src={%s, %u}, op_size=%u, \n\t"
    //    "idx={%s%s%s.scale=%u, .index=%u, .disp=%d, .disp_size=%u}\n",
    //    buf->pos, op_mnemonic2str[op], operand_type2str[src.type], src.reg,
    //    operand_size,
    //    indexing.use_simple ? ".use_simple=true, ":"", indexing.use_complex ? ".use_complex=true, ":"",
    //    indexing.use_rip_relative ? ".use_rip_relative=true, ":"",
    //    indexing.scale, indexing.index, indexing.disp, indexing.disp_size);

    x64_Instr ins = {0};

    OpcodeInfo base = opcode_table[op];
    debug_assert(base.n_ops == 1, "\n");
    if (base.force_src != NullLoc) {
        debug_assert(src.type == base.force_src, "%s src.type *has* to be %s, not %s\n",
            op_mnemonic2str[op], operand_type2str[base.force_src], operand_type2str[src.type]);
    }

    uint8_t opcode_base, opcode_mod_reg;
    if (src.type == Imm) {
        debug_assert(!base.no_imm_support, "\n");
        opcode_base = base.base_imm; opcode_mod_reg = base.mod_reg_imm;
    }
    else { opcode_base = base.base; opcode_mod_reg = base.mod_reg; }

    uint8_t rm_operand, reg_operand = 0;
    rm_operand = (src.type == Imm) ? 0 : src.reg;
    if (src.type == Memory) {
        ins.op_size_prefix = 0x67;
    }
    else {
        ins.op_size_prefix = 0x66;
    }
    if (operand_size == 16) ins.has_op_size_prefix = true;

    uint8_t d_bit = (src.type == Memory);
    uint8_t s_bit = (operand_size != 8 && src.type != Imm);

    if (src.type == Memory) {
        ins = configure_indexing(ins, &rm_operand, &indexing);
    }
    else {
        ins.mod = 3;
    }

    if (src.type == Imm) {
        ins.has_imm = true;
        ins.imm = src.imm; ins.imm_size = src.imm_size;
    }

    ins.has_0f_prefix = base.has_0f_prefix;
    ins.opcode = opcode_base | s_bit;
    if (!base.no_d_bit) ins.opcode |= (d_bit << 1);

    configure_ins_rex(&ins, base.defaults_to_64 ? 0 : operand_size, reg_operand, indexing.index, rm_operand);
    if (base.defaults_to_64) {
        ins.w = false;
        ins.has_rex = (ins.w || ins.r || ins.x || ins.b);
    }

    configure_ins_modrm(&ins, reg_operand, rm_operand);
    if (src.type == Imm) ins.has_modrm = false;
    if (opcode_mod_reg != 0) ins.reg = opcode_mod_reg;

    configure_ins_sib(&ins, indexing, rm_operand);

    if (operand_size == 8 && !base.cant_address_high_byte) {
        if (src.type == Register && src.reg < 8 && src.reg >= 4) ins.has_rex = true;
    }

    write_x64_Instr(buf, ins);
}

void write_x64_op2(Buffer* buf, OpMnemonic op, OperandType src, OperandType dst, uint8_t operand_size, IndexingInfo indexing) {
    //debug_print(
    //    "[0x%04lx] %s: src={%s, %u}, dst={%s, %u}, op_size=%u, \n\t"
    //    "idx={%s%s%s.scale=%u, .index=%u, .disp=%d, .disp_size=%u}\n",
    //    buf->pos, op_mnemonic2str[op], operand_type2str[src.type], src.reg, operand_type2str[dst.type], dst.reg,
    //    operand_size,
    //    indexing.use_simple ? ".use_simple=true, ":"", indexing.use_complex ? ".use_complex=true, ":"",
    //    indexing.use_rip_relative ? ".use_rip_relative=true, ":"",
    //    indexing.scale, indexing.index, indexing.disp, indexing.disp_size);

    // note: operand_size is in bits
    debug_assert(!(src.type == Memory && dst.type == Memory), "\n");
    debug_assert(dst.type != Imm, "\n");
    debug_assert(indexing.disp_size == 0 || indexing.disp_size == 1 || indexing.disp_size == 4, "\n");

    OpcodeInfo base = opcode_table[op];
    debug_assert(base.n_ops == 2, "\n");
    if (base.force_src != NullLoc) {
        debug_assert(src.type == base.force_src, "%s src.type *has* to be %s, not %s\n",
            op_mnemonic2str[op], operand_type2str[base.force_src], operand_type2str[src.type]);
    }
    if (base.force_dst != NullLoc) {
        debug_assert(dst.type == base.force_dst, "%s dst.type *has* to be %s, not %s\n",
            op_mnemonic2str[op], operand_type2str[base.force_dst], operand_type2str[dst.type]);
    }

    uint8_t opcode_base, opcode_mod_reg;
    if (src.type == Imm) {
        debug_assert(!base.no_imm_support, "\n");
        opcode_base = base.base_imm; opcode_mod_reg = base.mod_reg_imm;
    }
    else { opcode_base = base.base; opcode_mod_reg = base.mod_reg; }

    x64_Instr ins = {0};

    uint8_t rm_operand, reg_operand;
    if (src.type == Memory) {
        rm_operand = src.reg;
        reg_operand = dst.reg;
        //ins.op_size_prefix = 0x67;
        ins.op_size_prefix = 0x66; // memory addresses always use 64 bit version?
    }
    else {
        rm_operand = dst.reg;
        reg_operand = src.reg;
        ins.op_size_prefix = 0x66;
    }
    if (operand_size == 16) ins.has_op_size_prefix = true;

    uint8_t d_bit = (src.type == Memory && dst.type == Register);
    //uint8_t s_bit = (operand_size != 8 && src.type != Imm); I dunno
    uint8_t s_bit = (operand_size != 8);

    bool has_memory_operand = (src.type == Memory || dst.type == Memory);
    uint8_t original_rm_operand = rm_operand;
    if (has_memory_operand) {
        ins = configure_indexing(ins, &rm_operand, &indexing);
    }
    else {
        ins.mod = 3;
    }

    if (src.type == Imm) {
        ins.has_imm = true;
        ins.imm = src.imm; ins.imm_size = src.imm_size;
    }

    ins.has_0f_prefix = base.has_0f_prefix;
    ins.opcode = opcode_base | s_bit;
    if (!base.no_d_bit) ins.opcode |= (d_bit << 1);

    configure_ins_rex(&ins, operand_size, reg_operand, indexing.index, rm_operand);

    configure_ins_modrm(&ins, reg_operand, rm_operand);
    if (opcode_mod_reg != 0) ins.reg = opcode_mod_reg;

    if (has_memory_operand) configure_ins_sib(&ins, indexing, original_rm_operand);
    else configure_ins_sib(&ins, indexing, rm_operand);

    // don't use the high bit version of 8bit registers
    if (operand_size == 8 && !base.cant_address_high_byte) {
        if (src.type == Register && src.reg < 8 && src.reg >= 4) ins.has_rex = true;
        if (dst.type == Register && dst.reg < 8 && dst.reg >= 4) ins.has_rex = true;
    }

    write_x64_Instr(buf, ins);
}

void write_x64_movzx(Buffer* buf, OperandType src, uint8_t src_bits, uint8_t dst_reg, uint8_t dst_bits, IndexingInfo idx) {
    debug_assert(dst_bits > src_bits, "dst_bits=%u, src_bits=%u\n", dst_bits, src_bits);
    debug_assert(src.type != Imm, "src.type=%s\n", operand_type2str[src.type]);

    // zero extending 32 bit to 64 bit is done with the regular mov instruction
    if (src_bits == 32) {
        OperandType dst = { .type = Register, .reg = dst_reg };
        write_x64_op2(buf, OpMov, src, dst, 32, idx);
        return;
    }

    x64_Instr ins = { .has_0f_prefix = true };

    if (src.type == Memory) ins = configure_indexing(ins, &src.reg, &idx);

    ins.opcode = (src_bits == 8) ? 0xb6 : 0xb7;

    if (dst_bits == 16) {
        ins.has_op_size_prefix = true;
        ins.op_size_prefix = 0x66;
    }

    configure_ins_rex(&ins, dst_bits, dst_reg, idx.index, src.reg);
    configure_ins_modrm(&ins, dst_reg, src.reg);
    configure_ins_sib(&ins, idx, src.reg);

    write_x64_Instr(buf, ins);
}

void write_x64_movsx(Buffer* buf, OperandType src, uint8_t src_bits, uint8_t dst_reg, uint8_t dst_bits, IndexingInfo idx) {
    debug_assert(dst_bits > src_bits, "dst_bits=%u, src_bits=%u\n", dst_bits, src_bits);
    debug_assert(src.type != Imm, "src.type=%s\n", operand_type2str[src.type]);

    x64_Instr ins = { .has_0f_prefix = (src_bits != 32) };

    if (src.type == Memory) ins = configure_indexing(ins, &src.reg, &idx);

    if (src_bits == 32) ins.opcode = 0x63;
    else if (src_bits == 16) ins.opcode = 0xbf;
    else if (src_bits == 8) ins.opcode = 0xbe;

    if (dst_bits == 16) {
        ins.has_op_size_prefix = true;
        ins.op_size_prefix = 0x66;
    }

    configure_ins_rex(&ins, dst_bits, dst_reg, idx.index, src.reg);
    configure_ins_modrm(&ins, dst_reg, src.reg);
    configure_ins_sib(&ins, idx, src.reg);

    write_x64_Instr(buf, ins);
}

void write_x64_mov_ds_32(Buffer* buf, uint8_t opcode, uint32_t addr, uint8_t reg, uint8_t bit_size) {
    x64_Instr ins = {
        .opcode = opcode,
        .has_rex = true, .has_modrm = true, .has_sib = true,
        .has_imm = true,
        .imm_size = 4, .imm = addr,
    };
    if (bit_size == 16) {
        ins.has_op_size_prefix = true;
        ins.op_size_prefix = 0x66;
    }
    configure_ins_rex(&ins, bit_size, reg, 0, 0);
    configure_ins_modrm(&ins, reg, 4);
    configure_ins_sib_raw(&ins, 0, 4, 5);

    write_x64_Instr(buf, ins);
}

void write_x64_mov_from_ds_32(Buffer* buf, uint32_t addr, uint8_t reg, uint8_t bit_size) {
    write_x64_mov_ds_32(buf, 0x8b, addr, reg, bit_size);
}
void write_x64_mov_to_ds_32(Buffer* buf, uint32_t addr, uint8_t reg, uint8_t bit_size) {
    write_x64_mov_ds_32(buf, 0x89, addr, reg, bit_size);
}
