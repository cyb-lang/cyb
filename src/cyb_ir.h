#pragma once

#include "list.h"
#include "elf_debug_info.h"

#include <stdbool.h>

typedef struct {
    bool is_ptr;

    bool is_array;
    size_t array_size;

    bool is_struct; // if yes, none of the fields below (except member_types) are valid 
    uint8_t* struct_name;
    List members; // list of IrTypeInfo

    bool is_signed;
    uint32_t bit_size;
} IrTypeInfo;

uint32_t type_info_bits(IrTypeInfo type_info);
IrTypeInfo base_type_of(IrTypeInfo type);
IrTypeInfo pointer_to_type(IrTypeInfo type);

IrTypeInfo base_type_of(IrTypeInfo type);

typedef enum {
    IrLiteralInt,
    IrLiteralString,
} IrLiteralType;
extern const char* ir_literal_type2str[2];

typedef enum {
    IrRefData,
    IrRefGlobal,
    IrRefLocal,
} IrObjRefType;
extern const char* ir_obj_ref_type2str[3];

typedef struct {
    IrObjRefType type;
    size_t idx;
} IrObjRef;

typedef enum {
    IrLocNone,
    IrLocReg,
    IrLocStack,
    IrLocMem,
} IrObjLoc;
extern const char* ir_obj_loc2str[4];

typedef enum {
    IrLiteral,
    IrAddrStub,
    IrVar,
    IrVarRef,
    IrIndexOf, // uses ref is for indexed var, index_ref is for index var
    IrMemberOf, // uses ref for struct var
} IrObjType;
extern const char* ir_obj_type2str[6];
typedef struct {
    IrObjType type;
    IrTypeInfo type_info;

    IrLiteralType literal_type;
    uint64_t int_literal;
    uint8_t* str_literal;

    bool is_global_array_hack;
    List global_array_hack; // list of IrObj (elements of array)

    // for addr stub, copy stub, var ref, indexof, memberof
    IrObjRef ref;

    // data used only for indexof
    bool index_is_const;
    uint32_t const_index;
    IrObjRef index_ref;

    // only used for memberof
    uint32_t member_idx;

    // only valid for local variables (i.e. IrVar)
    // the index of the last IrInstr that references this IrObj
    size_t last_instr_used;

    // things for codegen
    IrObjLoc loc;
    uint8_t reg;
    int32_t stack_offset;
    size_t buf_offset; // offset in data buffer
} IrObj;

typedef enum {
    // arguments are passed in registers in the following order:
    // rdi, rsi, rdx, r10, r9, r8
    // syscall number is passed in rax. rcx and r11 are not saved.
    // return value gets put in rax.
    CallConvSyscall,

    // all arguments are passed via the stack, pushed right to left.
    // return value gets put in r15
    // note: see comment in `ir_write_call_cyb` for a detailed view
    // of how the stack is organized for this calling convention.
    CallConvCyb,

    CallConvCDecl,
} CallConvType;
extern const char* call_conv2str[3];

typedef enum {
    UnOpInc,
    UnOpNeg,
    UnOpLogicalNot,
    UnOpAddr,
} UnOpType;
extern const char* un_op_type2str[4];
typedef enum {
    BinOpPlus,
    BinOpMinus,
    BinOpLess,
    BinOpLessEql,
    BinOpGreater,
    BinOpGreaterEql,
    BinOpEql,
    BinOpNotEql,
    BinOpMod,
    BinOpMul,
    BinOpDiv,
    BinOpLogicalOr,
    BinOpLogicalAnd,
} BinOpType;
extern const char* bin_op_type2str[13];

typedef enum {
    IrAlloc,
    IrCall,
    IrReturn,
    IrAssign,
    IrBranch,
    IrCondBranch,
    IrUnOp,
    IrBinOp,
    IrCast,
} IrInstrType;
extern const char* ir_instr_type2str[9];
typedef struct {
    IrInstrType type;
    // some instruction types always produce a result, which is stored as local
    size_t local_idx;

    // alloc data
    IrTypeInfo alloc_type_info;

    // call data
    CallConvType call_conv;
    uint32_t syscall_code; // in case we're calling a linux syscall
    uint8_t* callee_name;
    bool using_fnptr;
    List args; // list of IrObj's to use in call
    List callee_arg_types; // list of IrTypeInfo

    // return data
    IrObj ret_obj;

    // assign data
    IrObj assign_dst_obj, assign_src_obj;

    // branch data
    size_t target_instr_idx;
    IrObj cond_obj; // branch if this obj is false (only valid for IrCondBranch)

    // unary op data
    UnOpType un_op_type;
    IrObj un_op_obj;

    // bin op data
    BinOpType bin_op_type;
    IrObj left_obj, right_obj;

    // cast data
    IrObjRef cast_src_ref;
    IrTypeInfo cast_dst_type;

    // used to generate debug information
    SourceLocation src_loc;
} IrInstr;

typedef struct {
    uint8_t* name;
    CallConvType call_conv;
    IrTypeInfo return_type;
    List args_type_info; // list of IrTypeInfo
    List instrs; // list of IrInstr

    // local and intermediate variables (function arguments always occupy the first slots)
    List local_objs; // list of IrObj

    bool is_extern;

    // used during codegen
    size_t buf_offset;
    List instr_offsets; // list of size_t
    uint32_t stack_space;
    bool reg_in_use[16];
    bool reg_dirty[16];
} IrFunction;

typedef struct {
    List data_objs, global_objs; // lists of IrObj

    List functions; // list of IrFunction
    size_t main_fn_idx;
} IrInfo;

typedef struct {
    uint8_t* name;
    size_t plt_offset; // offset into plt_buffer
} PltEntry;

typedef struct {
    Buffer asm_buffer;
    Buffer data_buffer;
    Buffer plt_buffer;
    List plt_entries; // list of PltEntry's
    size_t entry_point;
} RenderResult;
RenderResult render_ir_x64(IrInfo* ir_info, DebugInfo* dbg_info);

// in print_cyb_ir.c
void print_ir_info(IrInfo ir_info);
void print_ir_type(IrTypeInfo type, IrInfo ir_info);
void print_ir_obj(IrObj obj, IrInfo ir_info);
void print_ir_instr(IrInstr instr, IrInfo ir_info);
void print_ir_function(IrFunction function, IrInfo ir_info);
