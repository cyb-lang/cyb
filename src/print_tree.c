#include "parser.h"

#include <stdlib.h>

#define print_list(file, l) { \
    fprintf(file, "{ "); \
    for (size_t i = 0; i < l.length; i++) { \
        fprintf(file, "%lu%s ", at_index(&l, i, size_t), (i == l.length - 1) ? "" : ","); \
    } \
    fprintf(file, "}"); \
}

void print_parse_info(ParseInfo info, FILE* file) {
    fprintf(file, "ParseInfo = {\n");
    fprintf(file, "  functions.length = %lu\n", info.functions.length);
    fprintf(file, "  stmts.length = %lu\n", info.stmts.length);
    fprintf(file, "  variables.length = %lu\n", info.variables.length);
    fprintf(file, "  top_level_decls = "); print_list(file, info.top_level_decls) fprintf(file, "\n");

    fprintf(file, "\n");
    for (size_t i = 0; i < info.struct_types.length; i++) {
        StructType s_type = at_index(&info.struct_types, i, StructType);
        fprintf(file, "  struct_types[%lu]: @ %s:%lu:%lu\n", i, expand_src(s_type));
        fprintf(file, "    name = %s, bit_size = %lu\n", s_type.name, s_type.bit_size);
        for (size_t j = 0; j < s_type.members.length; j++) {
            StructMember s_member = at_index(&s_type.members, j, StructMember);
            TypeInfo sm_type = s_member.type_info;
            fprintf(file, "    (type = %s, name = %s, n_bits = %u) %s, byte_offset = %lu\n",
                type_type2str[sm_type.type], sm_type.name, sm_type.n_bits,
                s_member.name, s_member.byte_offset);
        }
    }

    fprintf(file, "\n");
    for (size_t i = 0; i < info.fnptr_types.length; i++) {
        FnPtrType fp_type = at_index(&info.fnptr_types, i, FnPtrType);
        fprintf(file, "  fnptr_types[%lu]:\n", i);
        if (fp_type.arg_types.length == 0) fprintf(file, "     arg_types = {}\n");
        else {
            fprintf(file, "    arg_types = {\n");
            for (size_t j = 0; j < fp_type.arg_types.length; j++) {
                TypeInfo arg_type = at_index(&fp_type.arg_types, j, TypeInfo);
                fprintf(file, "      {type = %s, name = %s, n_bits = %u, ptr_level = %u}\n",
                        type_type2str[arg_type.type], arg_type.name, arg_type.n_bits, arg_type.ptr_level);
            }
            fprintf(file, "    }\n");
        }
        TypeInfo ret_type = fp_type.return_type;
        fprintf(file, "    return_type = {type = %s, name = %s, n_bits = %u, ptr_level = %u}\n",
                type_type2str[ret_type.type], ret_type.name, ret_type.n_bits, ret_type.ptr_level);
    }

    fprintf(file, "\n");
    for (size_t i = 0; i < info.functions.length; i++) {
        Function f = at_index(&info.functions, i, Function);
        fprintf(file, "  function[%lu]:\n    name = %s\n", i, f.name);
        if (f.is_extern) fprintf(file, "    is_extern = true\n");
        fprintf(file, "    stmts = "); print_list(file, f.stmts); fprintf(file, "\n");
        fprintf(file, "    start_stmts = %lu, end_stmts = %lu\n", f.start_stmts, f.end_stmts);
        fprintf(file, "    args = "); print_list(file, f.args); fprintf(file, "\n");
        fprintf(file, "    return_type.n_bits = %u\n", f.return_type.n_bits);
    }

    fprintf(file, "\n");
    for (size_t i = 0; i < info.stmts.length; i++) {
        Stmt s = at_index(&info.stmts, i, Stmt);
        fprintf(file, "  stmt[%lu]: %s:%lu:%lu\n    type = %s\n", i, expand_src(s), stmt_type2str[s.type]);
        if (s.type == Stmt_FunctionCall) {
            fprintf(file, "    function_name = %s\n", s.name);
            fprintf(file, "    args = "); print_list(file, s.list); fprintf(file, "\n");
            if (s.using_fnptr) {
                fprintf(file, "    using_fnptpr = true, variable = %lu (%s)\n",
                    s.variable,
                    at_index(&info.variables, s.variable, Variable).name
                );
            }
        }
        else if (s.type == Stmt_If || s.type == Stmt_Else || s.type == Stmt_While || s.type == Stmt_For) {
            fprintf(file, "    cond = %lu\n", s.cond_stmt);
            if (s.type == Stmt_If || s.type == Stmt_Else) fprintf(file, "    else_stmt = %lu\n", s.else_stmt);
            fprintf(file, "    stmts = "); print_list(file, s.list); fprintf(file, "\n");
            if (s.type == Stmt_For) fprintf(file, "    init = %lu, step = %lu\n", s.init_stmt, s.step_stmt);
        }
        else if (s.type == Stmt_Return) {
            fprintf(file, "    return_stmt = %lu\n", s.return_stmt);
        }
        else if (s.type == Stmt_Variable) {
            fprintf(file, "    variable = %lu (%s)\n", s.variable, at_index(&info.variables, s.variable, Variable).name);
            if (s.assign_stmt != 0) fprintf(file, "    assign_stmt = %lu\n", s.assign_stmt);
        }
        else if (s.type == Stmt_Decl) {
            fprintf(file, "    variable = %lu\n    assign_stmt = %lu\n", s.variable, s.assign_stmt);
        }
        else if (s.type == Stmt_Assignment) {
            fprintf(file, "    op_left = %lu, op_right = %lu\n", s.op_left, s.op_right);
        }
        else if (s.type == Stmt_UnaryOp) {
            fprintf(file, "    op_token = %s\n    op_left = %lu\n", token_type2str[s.op_token], s.op_left);
        }
        else if (s.type == Stmt_BinaryOp) {
            fprintf(file, "    op_token = %s\n    op_left = %lu, op_right = %lu\n", token_type2str[s.op_token], s.op_left, s.op_right);
        }
        else if (s.type == Stmt_ArrayInit) {
            fprintf(file, "    stmts = "); print_list(file, s.list); fprintf(file, "\n");
        }
        else if (s.type == Stmt_Indexing) {
            fprintf(file, "    indexed = %lu, index = %lu\n", s.indexed_stmt, s.index_stmt);
        }

        if (s.is_global) {
            fprintf(file, "    is_global = true\n");
        }
    }

    fprintf(file, "\n");
    for (size_t i = 0; i < info.variables.length; i++) {
        Variable v = at_index(&info.variables, i, Variable);
        fprintf(file, "  variable[%lu]:\n    name = %s\n", i, v.name);
        if (!v.not_first) {
            TypeInfo t_info = v.type_info;
            fprintf(file, "    type_info = { type = %s, name = %s, n_bits = %u",
                type_type2str[t_info.type], t_info.name, t_info.n_bits);
            if (t_info.is_signed) fprintf(file, ", is_signed = true");
            if (t_info.ptr_level > 0) fprintf(file, ", ptr_level = %u", t_info.ptr_level);
            if (t_info.is_array) fprintf(file, ", array_size = %lu", t_info.array_size);
            if (t_info.type == Type_Struct) fprintf(file, ", struct_idx = %lu", t_info.struct_idx);
            if (t_info.type == Type_FnPtr) fprintf(file, ", fnptr_idx = %lu", t_info.fnptr_idx);
            fprintf(file, " }\n");

            switch (v.loc.type) {
                case Loc_None: fprintf(file, "    Loc_None??\n"); break;
                case Loc_Register: fprintf(file, "    register = %lu\n", v.loc.reg); break;
                case Loc_Stack: fprintf(file, "    stack_pos = %ld\n", v.loc.stack_pos); break;
                case Loc_Memory: fprintf(file, "    addr = 0x%lx\n", v.loc.addr); break;
                case Loc_Imm: fprintf(file, "    imm = 0x%lx\n", v.loc.imm); break;
            }
        }
        else fprintf(file, "    first = %lu\n", v.first);

        if (v.is_value) fprintf(file, "    value = %lu\n", v.value);
        if (v.is_reference) fprintf(file, "    is_reference = true\n");
    }

    fprintf(file, "}\n");
}

typedef struct {
    ParseInfo info;

    List indents; // uint32_t
    List last; // bool
    bool is_last;
} PrintContext;

char* ld_corner = "└";
char* straight_h = "─";
char* straight_v = "│";
char* v_split = "├";
char* h_split = "┬";

void print_indenting(PrintContext* pc) {
    for (size_t i = 0; i < pc->indents.length; i++) {
        uint32_t indent = at_index(&pc->indents, i, uint32_t);
        bool last = at_index(&pc->last, i, bool);

        if (last) printf(" ");
        else printf("%s", straight_v);
        for (uint32_t n = 0; n < indent - 1; n++) printf(" ");

        if (i == pc->indents.length - 1) { // last indent
            if (pc->is_last) printf("%s", ld_corner);
            else printf("%s", v_split);
        }

    }
    printf(" ");
}

#define print_tree_list(pc, name, b_last, list, child_list, child_print_func, child_type) { \
    pc->is_last = b_last; \
    print_indenting(pc); \
\
    printf("%s", name); \
    uint32_t to_push = strlen(name); append(&pc->indents, to_push); \
    bool to_push_last = b_last; append(&pc->last, to_push_last); \
\
    for (size_t i = 0; i < list.length; i++) { \
        size_t idx = at_index(&list, i, size_t); \
        pc->is_last = (i == list.length - 1); \
        child_print_func(pc, at_index(&pc->info.child_list, idx, child_type)); \
    } \
\
    pc->indents.length--; \
    pc->last.length--; \
}

#define print_tree_child_stmt(pc, name, b_last, child_last, idx, indent_size) { \
    uint32_t to_push = indent_size; append(&pc->indents, to_push); \
    bool to_push_last = b_last; append(&pc->last, to_push_last); \
\
    pc->is_last = child_last; \
    print_tree_stmt(pc, at_index(&pc->info.stmts, idx, Stmt)); \
\
    pc->indents.length--; \
    pc->last.length--; \
}

#define print_tree_child_var(pc, name, b_last, child_last, idx, indent_size) { \
    uint32_t to_push = indent_size; append(&pc->indents, to_push); \
    bool to_push_last = b_last; append(&pc->last, to_push_last); \
\
    pc->is_last = child_last; \
    print_tree_variable(pc, at_index(&pc->info.variables, idx, Variable)); \
\
    pc->indents.length--; \
    pc->last.length--; \
}

void print_varname(uint8_t* name) {
    char c = *name;
    do {
        char c = *name;
        if (c == '\n') printf("\\n");
        else printf("%c", c);
        name++;
    } while (c != '\0');
}

void print_tree_variable(PrintContext* pc, Variable v) {
    print_indenting(pc);
    //print_varname(v.name); printf("\n");
    printf("%s\n", v.name);
}

void print_tree_stmt(PrintContext* pc, Stmt s) {
    char buf[1000];
    char* cursor = buf;
    cursor += snprintf(cursor, 1000, "%s", stmt_type2str[s.type]);
    if (s.name != NULL) cursor += snprintf(cursor, 1000, " %s", s.name);
    cursor += snprintf(cursor, 1000, "\n");
    *cursor = '\0';

    print_tree_list(pc, buf, pc->is_last, s.list, stmts, print_tree_stmt, Stmt);

    if (s.type == Stmt_Decl && s.assign_stmt != 0) {
        print_tree_child_stmt(pc, buf, pc->is_last, true, s.assign_stmt, strlen(buf));
    }
    else if (s.type == Stmt_Variable) {
        //print_tree_child_var(pc, buf, pc->is_last, true, s.variable, strlen(buf));
    }
}

void print_tree_function(PrintContext* pc, Function f) {
    print_indenting(pc);
    printf("%s\n", f.name);
    uint32_t to_push = strlen(f.name) + 1; append(&pc->indents, to_push);
    append(&pc->last, pc->is_last);

    print_tree_list(pc, "arguments\n", false, f.args, variables, print_tree_variable, Variable);

    print_tree_list(pc, "stmts\n", true, f.stmts, stmts, print_tree_stmt, Stmt);

    pc->indents.length--;
    pc->last.length--;
}

void print_tree(ParseInfo info) {
    PrintContext pc = { .info = info };
    pc.indents = create_list(uint32_t);
    pc.last = create_list(bool);

    printf("Root\n");
    uint32_t to_push;
    bool last;

    printf("%s top_level_decls\n", v_split);
    to_push = 16; append(&pc.indents, to_push);
    last = false; append(&pc.last, last);
    for (size_t i = 0; i < info.top_level_decls.length; i++) {
        size_t decl_idx = at_index(&info.top_level_decls, i, size_t);
        pc.is_last = (i == info.top_level_decls.length - 1);
        print_tree_stmt(&pc, at_index(&info.stmts, decl_idx, Stmt));
    }
    pc.indents.length--;
    pc.last.length--;

    printf("%s functions\n", ld_corner);
    to_push = 10; append(&pc.indents, to_push);
    last = true; append(&pc.last, last);
    for (size_t i = 0; i < info.functions.length; i++) {
        pc.is_last = (i == info.functions.length - 1);
        print_tree_function(&pc, at_index(&info.functions, i, Function));
    }
    pc.indents.length--;
    pc.last.length--;

    free_list(&pc.indents);
}

void dump_tree_as_dot_stmt(FILE* file, ParseInfo info, size_t stmt_idx);

void dump_tree_as_dot_list_of_stmt_custom(FILE* file, ParseInfo info, List list, size_t stmt_idx, char* custom_str) {
    if (list.length == 0) return;

    // when doing the normal order that statements get put bottom-to-top
    // in the graph. so we just do them in reverse, no biggie.
    for (size_t i = list.length; i > 0; i--) {
        size_t s_idx = at_index(&list, i - 1, size_t); 
        dump_tree_as_dot_stmt(file, info, s_idx);
        fprintf(file, "    %s_%lu -> stmt_%lu;\n", custom_str, stmt_idx, s_idx);
    }

    // make a cluster to surrent list elements in a dashed-line box
    fprintf(file, "    subgraph cluster_stmt_%lu {\n", stmt_idx);
    fprintf(file, "      label=\"\"; style=dashed; edge [style=invis];\n");
    fprintf(file, "      ");
    for (size_t i = 0; i < list.length; i++) {
        fprintf(file, "stmt_%lu; ", at_index(&list, i, size_t));
    }
    fprintf(file, "\n    }\n");
}

void dump_tree_as_dot_list_of_stmt(FILE* file, ParseInfo info, size_t stmt_idx) {
    List list = at_index(&info.stmts, stmt_idx, Stmt).list;
    dump_tree_as_dot_list_of_stmt_custom(file, info, list, stmt_idx, "stmt");
}

void dump_tree_as_dot_type_str(FILE* file, ParseInfo info, TypeInfo type) {
    if (type.is_comptime_const) { fprintf(file, "comptime_const "); return; }

    for (size_t i = 0; i < type.ptr_level; i++) fprintf(file, "&");

    if (type.name) fprintf(file, "%s ", type.name);
    else fprintf(file, "%s ", type_type2str[type.type]);

    if (type.is_array) fprintf(file, "[%lu] ", type.array_size);
}

void dump_tree_as_dot_variable(FILE* file, ParseInfo info, size_t var_idx) {
    Variable var = at_index(&info.variables, var_idx, Variable);
    if (!var.not_first) dump_tree_as_dot_type_str(file, info, var.type_info);

    if (var.type_info.type == Type_String) fprintf(file, "'%s'", var.name);
    else if (var.name) fprintf(file, "%s", var.name);
}

void dump_tree_as_dot_stmt(FILE* file, ParseInfo info, size_t stmt_idx) {
    Stmt stmt = at_index(&info.stmts, stmt_idx, Stmt);

    fprintf(file, "    stmt_%lu [", stmt_idx);
    fprintf(file, "label=\"%s", stmt_type2str[stmt.type]);
    switch (stmt.type) {
        case Stmt_FunctionCall: case Stmt_StructDef: {
            fprintf(file, "|'%s'", stmt.name);
            break;
        }
        case Stmt_Variable: case Stmt_Decl: {
            fprintf(file, "|");
            dump_tree_as_dot_variable(file, info, stmt.variable);
            break;
        }
        case Stmt_UnaryOp: case Stmt_BinaryOp: {
            fprintf(file, "|%s", token_type2str[stmt.op_token]);
            break;
        }
        case Stmt_DotOp: {
            fprintf(file, "|{var: ");
            dump_tree_as_dot_variable(file, info, stmt.variable);
            fprintf(file, "|field: '%s'}", stmt.name);
            break;
        }
        case Stmt_None: case Stmt_If: case Stmt_Else: case Stmt_While:
        case Stmt_For: case Stmt_Break: case Stmt_Return: case Stmt_Assignment:
        case Stmt_ArrayInit: case Stmt_Indexing:
            break;
    }
    fprintf(file, "|%s:%lu:%lu\"", expand_src(stmt));
    fprintf(file, "];\n"); // end stmt attr_list

    // write connections and child nodes
    switch (stmt.type) {
        case Stmt_FunctionCall: {
            dump_tree_as_dot_list_of_stmt(file, info, stmt_idx);
            break;
        }
        case Stmt_If: case Stmt_While: {
            dump_tree_as_dot_stmt(file, info, stmt.cond_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"cond\"];\n", stmt_idx, stmt.cond_stmt);
            dump_tree_as_dot_list_of_stmt(file, info, stmt_idx);
            break;
        }
        case Stmt_Else: {
            dump_tree_as_dot_list_of_stmt(file, info, stmt_idx);
            break;
        }
        case Stmt_For: {
            dump_tree_as_dot_stmt(file, info, stmt.init_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"init\"];\n", stmt_idx, stmt.init_stmt);
            dump_tree_as_dot_stmt(file, info, stmt.cond_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"cond\"];\n", stmt_idx, stmt.cond_stmt);
            dump_tree_as_dot_stmt(file, info, stmt.step_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"step\"];\n", stmt_idx, stmt.step_stmt);
            dump_tree_as_dot_list_of_stmt(file, info, stmt_idx);
            break;
        }
        case Stmt_Return: {
            dump_tree_as_dot_stmt(file, info, stmt.return_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu;\n", stmt_idx, stmt.return_stmt);
            break;
        }
        case Stmt_Decl: {
            if (stmt.assign_stmt != 0) {
                dump_tree_as_dot_stmt(file, info, stmt.assign_stmt);
                fprintf(file, "    stmt_%lu -> stmt_%lu;\n", stmt_idx, stmt.assign_stmt);
            }
            break;
        }
        case Stmt_StructDef: case Stmt_ArrayInit: {
            dump_tree_as_dot_list_of_stmt(file, info, stmt_idx);
            break;
        }
        case Stmt_UnaryOp: {
            dump_tree_as_dot_stmt(file, info, stmt.op_left);
            fprintf(file, "    stmt_%lu -> stmt_%lu;\n", stmt_idx, stmt.op_left);
            break;
        }
        case Stmt_Assignment: case Stmt_BinaryOp: {
            dump_tree_as_dot_stmt(file, info, stmt.op_left);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"left\"];\n", stmt_idx, stmt.op_left);
            dump_tree_as_dot_stmt(file, info, stmt.op_right);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"right\"];\n", stmt_idx, stmt.op_right);
            break;
        }
        case Stmt_Indexing: {
            dump_tree_as_dot_stmt(file, info, stmt.indexed_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"indexed\"];\n", stmt_idx, stmt.indexed_stmt);
            dump_tree_as_dot_stmt(file, info, stmt.index_stmt);
            fprintf(file, "    stmt_%lu -> stmt_%lu [label=\"index\"];\n", stmt_idx, stmt.index_stmt);
            break;
        }

        case Stmt_None: case Stmt_DotOp: case Stmt_Variable: case Stmt_Break:
            break;
    }
}

void dump_tree_as_dot_function(FILE* file, ParseInfo info, Function fn, size_t fn_idx) {
    fprintf(file, "  fn_%lu [", fn_idx);

    fprintf(file, "label=\"Function '%s'", fn.name);
    for (size_t i = 0; i < fn.args.length; i++) {
        fprintf(file, "|");
        dump_tree_as_dot_variable(file, info, at_index(&fn.args, i, size_t));
    }
    if (!fn.is_syscall) fprintf(file, "|%s:%lu:%lu", expand_src(fn));
    fprintf(file, "\"");
    fprintf(file, "];\n"); // end of fn attr list

    fprintf(file, "  subgraph cluster_fn_%lu {\n", fn_idx);
    fprintf(file, "    label=\"Function '%s' @ %s:%lu:%lu\";\n", fn.name, expand_src(fn));

    dump_tree_as_dot_list_of_stmt_custom(file, info, fn.stmts, fn_idx, "fn");

    fprintf(file, "  }\n"); // close fn cluster
}

void dump_tree_as_dot(ParseInfo info, char* filename) {
    FILE* file = fopen(filename, "w");

    fprintf(file, "digraph {\n");
    fprintf(file, "  rankdir=LR;\n");
    fprintf(file, "  node [ shape=record ];\n");

    if (info.top_level_decls.length != 0) {
        fprintf(file, "\n");
        fprintf(file, "  subgraph cluster_top_decls {\n");
        fprintf(file, "    style=dashed;\n");
        fprintf(file, "    label=\"Top Level Declarations\";\n");
        for (size_t i = 0; i < info.top_level_decls.length; i++) {
            size_t stmt_idx = at_index(&info.top_level_decls, i, size_t);
            dump_tree_as_dot_stmt(file, info, stmt_idx);
        }
        fprintf(file, "  }\n");
    }

    for (size_t i = 0; i < info.functions.length; i++) {
        fprintf(file, "\n");
        Function fn = at_index(&info.functions, i, Function);
        dump_tree_as_dot_function(file, info, fn, i);
    }

    fprintf(file, "}"); // close digraph

    fclose(file);
}
