#include "list.h"

#include <stdlib.h> // for realloc/free
#include <string.h> // for memcpy

// these are macros instead of functions so that debug_assert show the calling line number
#define assert_list(list_ptr) { \
    debug_assert(list_ptr, "list pointer passed in is NULL\n"); \
    debug_assert(list_ptr->size_of_elem > 0, "list was not init'd\n"); \
}
#define assert_type(list_ptr, type_size, type_name) { \
    debug_assert(type_size == list_ptr->size_of_elem, \
        "type size mismatch: passed in type: sizeof(%s)=%lu vs list type: sizeOf(%s)=%lu\n", \
        type_name, type_size, list_ptr->debug_type_name_internal, list_ptr->size_of_elem \
    ); \
}

void* ptr_at_index(List* list, size_t index) {
    return (list->data_internal + (index * list->size_of_elem));
}

List new_list_from_pointer_internal(size_t elem_size, const char* type_name, void* ptr, size_t length) {
    List list = { .size_of_elem = elem_size };
    list.debug_type_name_internal = type_name;

    if (ptr != NULL) {
        ensure_capacity(&list, length);
        memcpy(list.data_internal, ptr, elem_size * length);
        list.length = length;
    }

    return list;
}

List create_new_copy(List* src) {
    assert_list(src);

    return new_list_from_pointer_internal(
        src->size_of_elem, src->debug_type_name_internal, src->data_internal, src->length
    );
}

void ensure_capacity(List* list, size_t new_capacity) {
    assert_list(list);

    if (list->capacity >= new_capacity) return;

    size_t better_capacity = list->capacity;
    do {
        better_capacity = better_capacity * 2 + 1;
    } while (better_capacity < new_capacity);

    list->data_internal = realloc(list->data_internal, better_capacity * list->size_of_elem);
    list->capacity = better_capacity;

    debug_assert(list->data_internal, "no memory for List (tried to alloc %lu bytes)\n",
        better_capacity * list->size_of_elem);
}

void reserve(List* list, size_t n_elems_to_reserve) {
    assert_list(list);

    list->length += n_elems_to_reserve;
    ensure_capacity(list, list->length);
}

void append_internal(List* list, void* elem_ptr) {
    assert_list(list);

    ensure_capacity(list, list->length + 1);
    debug_assert(list->data_internal, "\n");

    void* write_ptr = ptr_at_index(list, list->length);
    memcpy(write_ptr, elem_ptr, list->size_of_elem);

    list->length += 1;
}

void append_list(List* list, List* other_list) {
    assert_list(list);
    assert_type(list, other_list->size_of_elem, other_list->debug_type_name_internal);

    if (other_list->length == 0) return;

    ensure_capacity(list, list->length + other_list->length);

    void* write_ptr = ptr_at_index(list, list->length);
    memcpy(write_ptr, other_list->data_internal, list->size_of_elem * other_list->length);

    list->length += other_list->length;
}

void* checked_index_internal(List* list, size_t index, size_t type_size, const char* type_name) {
    assert_list(list);
    assert_type(list, type_size, type_name);
    debug_assert(index < list->length, "index=%lu vs length=%lu\n", index, list->length);
    return list->data_internal;
}

void* pop_back_internal(List* list, size_t type_size, const char* type_name) {
    assert_list(list);
    debug_assert(list->length > 0, "can't pop_back on empty list\n");
    assert_type(list, type_size, type_name);

    list->length -= 1;

    return ptr_at_index(list, list->length);
}

void remove_index(List* list, size_t index) {
    assert_list(list);
    debug_assert(index < list->length, "index=%lu vs length=%lu\n", index, list->length);

    void* dst = ptr_at_index(list, index);
    void* src = ptr_at_index(list, index + 1);
    size_t elem_to_move = (list->length - 1) - index;
    memmove(dst, src, elem_to_move * list->size_of_elem);

    list->length -= 1;
}

void clear_list(List* list) {
    assert_list(list);
    memset(list->data_internal, 0, list->size_of_elem * list->length);
    list->length = 0;
    list->pos = 0;
}

void free_list(List* list) {
    assert_list(list);
    free(list->data_internal);
    list->data_internal = NULL;
    list->length = 0;
    list->capacity = 0;
    list->pos = 0;
}
