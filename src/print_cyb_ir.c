#include "cyb_ir.h"

typedef struct {
    size_t cur_instr_idx;
    int32_t last_cur_instr_idx;
    List branch_targets; // list of size_t
    bool do_instr_idx;
} PrintContext;

static PrintContext print_ctx = { .last_cur_instr_idx = -1, .do_instr_idx = true };

void print_start_line() {
    printf("    ");
    if (print_ctx.do_instr_idx) {
        if (((int32_t) print_ctx.cur_instr_idx) == print_ctx.last_cur_instr_idx) {
            printf(".  ");
        }
        else {
            printf("%02lu:", print_ctx.cur_instr_idx);
            print_ctx.last_cur_instr_idx = print_ctx.cur_instr_idx;
        }
        printf(" ");
    }
    else {
        printf("    ");
    }

    for (uint32_t i = 0; i < print_ctx.branch_targets.length; i++) printf("    ");
}

void print_ir_type(IrTypeInfo type, IrInfo ir_info) {
    if (type.is_ptr) printf("&");
    if (type.is_array) printf("[%lu]", type.array_size);
    if (type.is_struct) {
        printf("%s", type.struct_name);
        return;
    }
    printf("%c%u", type.is_signed ? 'i':'u', type.bit_size);
}

void print_string_raw(char* str) {
    char* cursor = str;
    while (*cursor != '\0') {
        if (*cursor == '\n') printf("\\n");
        else if (*cursor == '\t') printf("\\t");
        else if (*cursor == '\b') printf("\\b");
        else printf("%c", *cursor);
        cursor++;
    }
}

void print_ir_ref(IrObjRef ref, IrInfo ir_info) {
    printf("%s", ir_obj_ref_type2str[ref.type]);
    printf("_#%lu", ref.idx);
}

void print_ir_obj(IrObj obj, IrInfo ir_info) {
    print_ir_type(obj.type_info, ir_info);
    printf(" ");

    printf("%s(", ir_obj_type2str[obj.type]);
    switch (obj.type) {
        case IrLiteral: {
            switch (obj.literal_type) {
                case IrLiteralInt:
                    if (obj.type_info.is_signed) printf("%ld", obj.int_literal);
                    else printf("%lu", obj.int_literal);
                    break;
                case IrLiteralString:
                    printf("'");
                    print_string_raw(obj.str_literal);
                    printf("'");
                    break;
            }
            break;
        }
        case IrAddrStub: case IrVarRef: {
            print_ir_ref(obj.ref, ir_info);
            break;
        }
        case IrVar: {
            break;
        }
        case IrIndexOf: {
            print_ir_ref(obj.ref, ir_info);
            printf(", ");
            if(obj.index_is_const) printf("%u", obj.const_index);
            else print_ir_ref(obj.index_ref, ir_info);
            break;
        }
        case IrMemberOf: {
            print_ir_ref(obj.ref, ir_info);
            printf(", %u", obj.member_idx);
            break;
        }
    }
    printf(")");
}

void print_ir_instr(IrInstr instr, IrInfo ir_info) {
    print_start_line();

    switch (instr.type) {
        case IrAlloc: case IrCall: case IrUnOp: case IrBinOp: case IrCast:
            printf("local_#%lu = ", instr.local_idx);
            break; 

        // these instructions don't have a result stored in locals 
        case IrAssign: case IrBranch: case IrCondBranch: case IrReturn:
            break;
    } 

    printf("%s ", ir_instr_type2str[instr.type]);

    switch (instr.type) {
        case IrAlloc: {
            print_ir_type(instr.alloc_type_info, ir_info);
            break;
        }
        case IrCall: {
            if (instr.call_conv != CallConvCyb) printf("%s ", call_conv2str[instr.call_conv]);
            printf("%s(\n", instr.callee_name);
            for (size_t i = 0; i < instr.args.length; i++) {
                IrObj ir_obj = at_index(&instr.args, i, IrObj);
                print_start_line();
                printf("    "); // indent
                print_ir_obj(ir_obj, ir_info);
                printf(",\n");
            }
            print_start_line();
            printf(")");
            break;
        }
        case IrReturn: {
            print_ir_obj(instr.ret_obj, ir_info);
            break;
        }
        case IrAssign: {
            print_ir_obj(instr.assign_src_obj, ir_info);
            printf(" -> ");
            print_ir_obj(instr.assign_dst_obj, ir_info);
            break;
        }
        case IrBranch: {
            printf("IrInstr_#%lu ", instr.target_instr_idx);
            break;
        }
        case IrCondBranch: {
            printf("IrInstr_#%lu, ", instr.target_instr_idx);
            print_ir_obj(instr.cond_obj, ir_info);
            break;
        }
        case IrUnOp: {
            printf("%s ", un_op_type2str[instr.un_op_type]);
            print_ir_obj(instr.un_op_obj, ir_info);
            break;
        }
        case IrBinOp: {
            printf("%s ", bin_op_type2str[instr.bin_op_type]);
            print_ir_obj(instr.left_obj, ir_info);
            printf(", ");
            print_ir_obj(instr.right_obj, ir_info);
            break;
        }
        case IrCast: {
            print_ir_ref(instr.cast_src_ref, ir_info);
            printf(" -> ");
            print_ir_type(instr.cast_dst_type, ir_info);
            break;
        }
    }
    printf("\n");
}

void print_ir_function(IrFunction function, IrInfo ir_info) {
    printf("function '%s' %s{\n", function.name, function.is_extern ? "(extern) ":"");
    printf("    args: ");
    for (size_t i = 0; i < function.args_type_info.length; i++) {
        IrTypeInfo ir_type = at_index(&function.args_type_info, i, IrTypeInfo);
        print_ir_type(ir_type, ir_info);
        if (i != function.args_type_info.length - 1) printf(", ");
    }
    printf("\n");
    printf("    returns: "); print_ir_type(function.return_type, ir_info);
    printf("\n");
    printf("    callconv=%s", call_conv2str[function.call_conv]);
    printf("\n");

    printf("\n");

    print_ctx.branch_targets = create_list(size_t);
    for (size_t i = 0; i < function.instrs.length; i++) {
        print_ctx.cur_instr_idx = i;

        if (print_ctx.branch_targets.length > 0) {
            size_t last_idx = print_ctx.branch_targets.length - 1;
            size_t target_instr_idx = at_index(&print_ctx.branch_targets, last_idx, size_t);
            if (i == target_instr_idx) remove_index(&print_ctx.branch_targets, last_idx);
        }

        IrInstr ir_instr = at_index(&function.instrs, i, IrInstr);
        print_ir_instr(ir_instr, ir_info);

        if (ir_instr.type == IrCondBranch) {
            append(&print_ctx.branch_targets, ir_instr.target_instr_idx);
        }
    }
    print_ctx.last_cur_instr_idx = -1;
    free_list(&print_ctx.branch_targets);

    printf("}\n");
}

void print_ir_info(IrInfo info) {
    for (size_t i = 0; i < info.data_objs.length; i++) {
        IrObj ir_obj = at_index(&info.data_objs, i, IrObj);
        printf("data_#%lu = ", i);
        print_ir_obj(ir_obj, info);
        printf("\n");
    }

    printf("\n");

    for (size_t i = 0; i < info.global_objs.length; i++) {
        IrObj ir_obj = at_index(&info.global_objs, i, IrObj);
        printf("global_#%lu = ", i);
        print_ir_obj(ir_obj, info);
        printf("\n");
    }

    printf("\n");

    for (size_t i = 0; i < info.functions.length; i++) {
        IrFunction ir_fn = at_index(&info.functions, i, IrFunction);
        print_ir_function(ir_fn, info);
        printf("\n");
    }
}
