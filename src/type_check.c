// the type checking stage does some important things to ParseInfo besides
// making sure types match (or can be converted). these things are:
//  - filling the "function_idx" field on every Stmt_FunctionCall stmt with the
//    index of the function being called, which is used in later stages of compilation
//  - adding special Function's to ParseInfo for the syscall functions (i.e. they
//    start with "sys_") as needed by examining Stmt_FunctionCall stmts
//  - linking struct TypeInfo's to the StructType using struct_idx on decls
//  - filling in the "array_type" field for ArrayInit statements

#include "parser.h"

bool can_implicit_cast(TypeInfo src, TypeInfo dst) {
    // allow conversion from comptime strings to &u8
    if (src.type == Type_String && dst.type != Type_String) {
        return (dst.ptr_level == 1 && !dst.is_signed && dst.n_bits == 8);
    }

    // pointers can convert into other pointers, regardless of the type they point to 
    if (src.ptr_level != 0 && dst.ptr_level != 0) return true;
    // but pointer to non-pointer cast are not allowed except for these special cases:
    // - converting array into a pointer
    if (src.is_array && dst.ptr_level != 0) return true;
    // - converting a pointer into a u64
    if (src.ptr_level != 0 && (dst.type == Type_Integer && dst.n_bits == 64)) return true;
    // - converting a comptime int into a pointer
    if (src.is_comptime_const && dst.ptr_level != 0) return true;
    // - converting any pointer to FnPtr pointer
    if (src.ptr_level != 0 && dst.type == Type_FnPtr) return true;
    // - converting any unsigned integer to FnPtr pointer
    if (src.type == Type_Integer && !src.is_signed && dst.type == Type_FnPtr) return true;

    if (src.type != dst.type) return false;
    
    switch (src.type) {
        case Type_Integer:
            if (src.is_comptime_const && dst.is_comptime_const) return true;

            if (!src.is_signed && dst.is_signed) {
                if (dst.n_bits > src.n_bits) return true;
                else if (dst.n_bits == src.n_bits) {
                    return (src.comptime_value & (1 << (dst.n_bits - 1))) == 0;
                }
                else return false;
            }
            else {
                return (dst.n_bits >= src.n_bits);
            }

        case Type_Float: return (dst.n_bits >= src.n_bits);

        case Type_None: 
        case Type_String: 
        case Type_Bool: return true;

        case Type_Struct: return false;

        case Type_FnPtr: return (dst.ptr_level != 0);
    }

    return false;
}

bool types_match(TypeInfo a, TypeInfo b) {
    if (a.type != b.type) return false;

    // comptime constant take up whatever type the need to in the expression
    // (unless, of course, they don't fit)
    if (a.is_comptime_const) return can_implicit_cast(a, b);
    if (b.is_comptime_const) return can_implicit_cast(b, a);

    if (a.ptr_level != b.ptr_level) return false;

    switch (a.type) {
        case Type_Integer: return (a.is_signed == b.is_signed && a.n_bits == b.n_bits);
        case Type_Float: return (a.n_bits == b.n_bits);

        case Type_None: 
        case Type_String: 
        case Type_Bool: return true;

        case Type_Struct: return (strcmp(a.name, b.name) == 0);

        case Type_FnPtr: return (a.fnptr_idx == b.fnptr_idx);
    }

    return false;
}

// macro instead of a function call so that debug_assert prints the right line/function
#define check_types(src, dst, src_loc) { \
    if (!types_match(src, dst) && !can_implicit_cast(src, dst)) { \
        debug_crash( \
            "can't convert from " \
            "%s (%s%s%s, bits=%u, ptr_lvl=%u) to " \
            "%s (%s%s%s, bits=%u, ptr_lvl=%u) " \
            "@ %s:%lu:%lu\n", \
            type_type2str[src.type], src.is_array ? "array of ":"", src.name ? (char*)src.name:"", \
                src.is_comptime_const ? "comptime_const":"", \
                src.n_bits, src.ptr_level, \
            type_type2str[dst.type], dst.is_array ? "array of ":"", dst.name ? (char*)dst.name:"", \
                dst.is_comptime_const ? "comptime_const":"", \
                dst.n_bits, dst.ptr_level, \
            src_loc.filename, src_loc.line, src_loc.column); \
    } \
}

static bool is_syscall(uint8_t* fn_name, uint16_t* syscall_num) {
    const char* supported_syscalls[] = {
        "sys_read",
        "sys_write",
        "sys_exit",
    };
    const size_t supported_syscalls_n = 3;

    uint16_t syscall_numbers[] = { 0, 1, 60 };

    for (size_t i = 0; i < supported_syscalls_n; i++) {
        if (strcmp(fn_name, supported_syscalls[i]) == 0) {
            *syscall_num = syscall_numbers[i];
            return true;
        }
    }

    return false;
}

typedef struct {
    bool already_done;
    size_t info_fn_idx;
    Function fn;
} SyscallFnTableInfo;

#define n_syscall_fns 335
SyscallFnTableInfo syscall_fn_table[n_syscall_fns];

Function get_syscall_fn(ParseInfo* info, uint16_t syscall_num) {
    debug_assert(syscall_num < n_syscall_fns, "pos_in_table=%u\n", syscall_num);

    // if we already did this syscall just return it instead of duplicating it
    SyscallFnTableInfo fn_table_info = syscall_fn_table[syscall_num];
    if (fn_table_info.already_done) {
        return at_index(&info->functions, fn_table_info.info_fn_idx, Function);
    }

    TypeInfo type_u32 = { .type = Type_Integer, .n_bits = 32, .name = "u32" };
    TypeInfo type_u64 = { .type = Type_Integer, .n_bits = 64, .name = "u64" };
    TypeInfo type_u8_ptr = { .type = Type_Integer, .n_bits = 8, .name = "u8", .ptr_level = 1 };

    Function fn = { .is_syscall = true, .return_type = type_u64, .code = syscall_num };
    fn.arg_types = create_list(TypeInfo);
    fn.stmts = create_list(size_t);
    fn.args = create_list(size_t);

    switch (syscall_num) {
        case 0:
            fn.name = "sys_read";
            append(&fn.arg_types, type_u32);
            append(&fn.arg_types, type_u8_ptr);
            append(&fn.arg_types, type_u64);
            break;

        case 1:
            fn.name = "sys_write";
            append(&fn.arg_types, type_u32);
            append(&fn.arg_types, type_u8_ptr);
            append(&fn.arg_types, type_u64);
            break;

        case 60:
            fn.name = "sys_exit";
            append(&fn.arg_types, type_u32);
            break;

        default:
            debug_crash("syscall # %u not implemented\n", syscall_num);
    }

    // cache it for next time
    fn_table_info.already_done = true;
    fn_table_info.info_fn_idx = info->functions.length;
    fn_table_info.fn = fn;
    syscall_fn_table[syscall_num] = fn_table_info;

    append(&info->functions, fn);

    return fn;
}

TypeInfo type_check_stmt(ParseInfo* info, Stmt s, size_t fn_idx);

// because we can't pass a pointer to the Stmt without having to change
// this entire file, we're gonna have to find it in the list using
// its source location (which should be unique).
Stmt* find_stmt_ptr_in_list(ParseInfo* info, Stmt s) {
    SourceLocation src = s.src_loc;
    for (size_t i = 0; i < info->stmts.length; i++) {
        Stmt* other_ptr = &at_index(&info->stmts, i, Stmt);
        SourceLocation other_src = other_ptr->src_loc;
        if (src_loc_eql(src, other_src)) return other_ptr;
    }
    debug_crash("couldn't find %s @ %s:%lu:%lu in list\n", stmt_type2str[s.type], expand_src(s));
}

// puts the first occurence of a declaration of 'lookup' in 'found_idx' that is not itself
// return true if it found that
bool var_lookup_decl(ParseInfo* info, Variable lookup, size_t fn_idx, size_t* found_idx) {
    Function fn = at_index(&info->functions, fn_idx, Function);
    // search function arguments
    for (size_t i = 0; i < fn.args.length; i++) {
        size_t idx = at_index(&fn.args, i, size_t);
        Variable var = at_index(&info->variables, idx, Variable);
        if (var.is_value || var.name == NULL) continue;
        if (strcmp(lookup.name, var.name) == 0) {
            *found_idx = idx; return true;
        }
    }

    // search the function scope
    for (size_t i = fn.start_stmts; i < fn.end_stmts; i++) {
        // note: the way I'm doing this is gonna leak variables declared in blocks
        // like loops and if/else stmts into the function scope
        Stmt stmt = at_index(&info->stmts, i, Stmt);
        if (stmt.type != Stmt_Decl) continue; 
        Variable var = at_index(&info->variables, stmt.variable, Variable);
        if (var.is_value || var.name == NULL) continue;
        if (strcmp(lookup.name, var.name) == 0) {
            // if the first var we find that matches is at the same exact place in the
            // source, then our searched failed
            SourceLocation v_src = var.src_loc, l_src = lookup.src_loc;
            if (v_src.line == l_src.line && v_src.column == l_src.column) break;

            *found_idx = stmt.variable; return true;
        }
    }

    // then in file scope
    for (size_t i = 0; i < info->top_level_decls.length; i++) {
        Stmt stmt = indirect_index(&info->stmts, &info->top_level_decls, i, Stmt);
        if (stmt.type != Stmt_Decl && stmt.type != Stmt_Variable) continue; 
        Variable var = at_index(&info->variables, stmt.variable, Variable);
        if (var.name == NULL) continue;
        if (strcmp(lookup.name, var.name) == 0) {
            *found_idx = stmt.variable; return true;
        }
    }

    return false;
}

TypeInfo type_check_fn_call(ParseInfo* info, Stmt s, size_t fn_idx) {
    // find the function we're calling
    bool found = false; size_t found_idx = 0;
    for (size_t i = 0; i < info->functions.length; i++) {
        Function other_fn = at_index(&info->functions, i, Function);
        if (strcmp(s.name, other_fn.name) == 0) {
            found = true; found_idx = i;
            break;
        }
    }

    Function called = at_index(&info->functions, found_idx, Function);

    uint16_t syscall_num = 0;
    if (!found && is_syscall(s.name, &syscall_num)) {
        called = get_syscall_fn(info, syscall_num);
        found = true;
        found_idx = syscall_fn_table[syscall_num].info_fn_idx;
    }

    // if no function has this name, try searching for function pointers
    bool using_fnptr = false;
    if (!found) {
        Variable lookup_var = { .name = s.name, .src_loc = s.src_loc };
        found = var_lookup_decl(info, lookup_var, fn_idx, &found_idx);
        if (found) {
            using_fnptr = true;

            // 'called' is gonna be used to type check the call arguments
            Variable fnptr_var = at_index(&info->variables, found_idx, Variable);
            //debug_print("fnptr_var={
            if (fnptr_var.not_first) fnptr_var = at_index(&info->variables, fnptr_var.first, Variable);
            TypeInfo fnptr_type_info = fnptr_var.type_info;
            debug_assert(fnptr_type_info.type == Type_FnPtr, "\n");
            FnPtrType fnptr_type = at_index(&info->fnptr_types, fnptr_type_info.fnptr_idx, FnPtrType);
            called = (Function) {
                .name = s.name,
                .src_loc = fnptr_var.src_loc,
                .arg_types = fnptr_type.arg_types,
                .return_type = fnptr_type.return_type,
            };
        }
    }

    debug_assert(found, "call to '%s' @ %s:%lu:%lu but no function exists with this name\n",
        s.name, expand_src(s));

    // link the stmt to the correct function for later codegen
    Stmt* stmt_ptr = find_stmt_ptr_in_list(info, s);
    if (using_fnptr) {
        stmt_ptr->using_fnptr = true;
        stmt_ptr->variable = found_idx;
        stmt_ptr->function_idx = 0;
    }
    else stmt_ptr->function_idx = found_idx;

    // check all the argument types match (or can be implicitly converted)
    debug_assert(called.arg_types.length == s.list.length,
        "wrong number of arguments passed to function '%s' @ %s:%lu:%lu defined @ %s:%lu:%lu "
        "(expected %lu, passed %lu)\n",
        called.name, expand_src(s), expand_src(called), called.arg_types.length, s.list.length);
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt arg_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        TypeInfo call_arg_type = type_check_stmt(info, arg_stmt, fn_idx);
        TypeInfo fn_arg_type = at_index(&called.arg_types, i, TypeInfo);
        check_types(call_arg_type, fn_arg_type, arg_stmt.src_loc);
    }

    return called.return_type;
}

TypeInfo type_check_if(ParseInfo* info, Stmt s, size_t fn_idx) {
    // condition must be boolean (no implicit conversion from other types)
    Stmt cond_stmt = at_index(&info->stmts, s.cond_stmt, Stmt);
    TypeInfo cond_type = type_check_stmt(info, cond_stmt, fn_idx);
    debug_assert(cond_type.type == Type_Bool,
        "if condition must be a boolean @ %s:%lu:%lu\n", expand_src(cond_stmt));

    // check block
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt block_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        type_check_stmt(info, block_stmt, fn_idx);
    }

    // check else clause
    if (s.else_stmt != 0) {
        // not calling type_check_else because it could another if (for else if clauses)
        type_check_stmt(info, at_index(&info->stmts, s.else_stmt, Stmt), fn_idx);
    }

    return (TypeInfo) { .type = Type_None };
}

TypeInfo type_check_else(ParseInfo* info, Stmt s, size_t fn_idx) {
    // check block
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt block_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        type_check_stmt(info, block_stmt, fn_idx);
    }

    return (TypeInfo) { .type = Type_None };
}

TypeInfo type_check_while(ParseInfo* info, Stmt s, size_t fn_idx) {
    // condition must be boolean (no implicit conversion from other types)
    Stmt cond_stmt = at_index(&info->stmts, s.cond_stmt, Stmt);
    TypeInfo cond_type = type_check_stmt(info, cond_stmt, fn_idx);
    debug_assert(cond_type.type == Type_Bool,
        "while condition must be a boolean @ %s:%lu:%lu\n", expand_src(cond_stmt));

    // check block
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt block_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        type_check_stmt(info, block_stmt, fn_idx);
    }

    return (TypeInfo) { .type = Type_None };
}

TypeInfo type_check_for(ParseInfo* info, Stmt s, size_t fn_idx) {
    // check loop init 
    Stmt init_stmt = at_index(&info->stmts, s.init_stmt, Stmt);
    type_check_stmt(info, init_stmt, fn_idx);

    // condition must be boolean (no implicit conversion from other types)
    Stmt cond_stmt = at_index(&info->stmts, s.cond_stmt, Stmt);
    TypeInfo cond_type = type_check_stmt(info, cond_stmt, fn_idx);
    debug_assert(cond_type.type == Type_Bool,
        "while condition must be a boolean @ %s:%lu:%lu\n", expand_src(cond_stmt));

    // check step
    Stmt step_stmt = at_index(&info->stmts, s.step_stmt, Stmt);
    type_check_stmt(info, step_stmt, fn_idx);

    // check block
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt block_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        type_check_stmt(info, block_stmt, fn_idx);
    }

    return (TypeInfo) { .type = Type_None };
}

TypeInfo type_check_return(ParseInfo* info, Stmt s, size_t fn_idx) {
    TypeInfo ret_type = {0};
    if (s.return_stmt != 0) {
        Stmt ret_stmt = at_index(&info->stmts, s.return_stmt, Stmt);
        ret_type = type_check_stmt(info, ret_stmt, fn_idx);
    }

    TypeInfo fn_ret_type = at_index(&info->functions, fn_idx, Function).return_type;
    check_types(ret_type, fn_ret_type, s.src_loc);

    return ret_type;
}

TypeInfo type_check_variable(ParseInfo* info, Stmt s, size_t fn_idx) {
    // check variable has already been declared
    Variable var = at_index(&info->variables, s.variable, Variable);
    bool is_ref = var.is_reference;
    if (!var.is_value && var.type_info.type != Type_String) {
        size_t var_idx = s.variable;

        size_t found_idx = 0;
        debug_assert(var_lookup_decl(info, var, fn_idx, &found_idx),
            "var '%s' used @ %s:%lu:%lu but never declared\n",
            var.name, expand_src(var));
        Variable first = at_index(&info->variables, found_idx, Variable);
        debug_assert(first.src_loc.line <= var.src_loc.line,
            "var '%s' used @ %s:%lu:%lu before decl (@ %s:%lu:%lu)\n",
            var.name, expand_src(var), expand_src(first));

        at_index(&info->variables, var_idx, Variable).not_first = true;
        at_index(&info->variables, var_idx, Variable).first = found_idx;
        var = at_index(&info->variables, found_idx, Variable);
    }

    debug_assert(var.type_info.type != Type_None,
        "\n'%s' @ (var/stmt) (%s:%lu:%lu/%s:%lu:%lu)\n", var.name,
        expand_src(var), expand_src(s));

    if (is_ref) var.type_info.ptr_level += 1;

    return var.type_info;
}

TypeInfo type_check_decl(ParseInfo* info, Stmt s, size_t fn_idx) {
    // check that variable name is available
    Variable var = at_index(&info->variables, s.variable, Variable);
    size_t found_idx = 0;
    bool found = var_lookup_decl(info, var, fn_idx, &found_idx);
    if (found) {
        Variable first = at_index(&info->variables, found_idx, Variable);
        debug_crash(
            "redeclaration of '%s' @ %s:%lu:%lu (first declared @ %s:%lu:%lu)\n",
            var.name, expand_src(var), expand_src(first));
    }

    // link struct type info
    if (var.type_info.type == Type_Struct) {
        bool found = false;
        size_t found_idx = 0;
        for (size_t i = 0; i < info->struct_types.length; i++) {
            StructType stype = at_index(&info->struct_types, i, StructType);
            if (strcmp(var.type_info.name, stype.name) == 0) {
                found = true;
                found_idx = i;
            }
        }

        debug_assert(found, "no type named '%s'. (used @ %s:%lu:%lu)\n",
            var.type_info.name, expand_src(s));

        at_index(&info->variables, s.variable, Variable).type_info.struct_idx = found_idx;
    }

    if (s.assign_stmt != 0) {
        Stmt assign_stmt = at_index(&info->stmts, s.assign_stmt, Stmt);
        TypeInfo assign_type = type_check_stmt(info, assign_stmt, fn_idx);
        check_types(assign_type, var.type_info, assign_stmt.src_loc);

        // when parsing, the var part of the decl might not have an array size specified.
        // now that the decl has passed type checking we can udpate it.
        if (assign_type.is_array && var.type_info.is_array) {
            Variable* var_ptr = &at_index(&info->variables, s.variable, Variable);
            var_ptr->type_info.array_size = assign_type.array_size;
            var.type_info = var_ptr->type_info;
        }
    }

    return var.type_info;
}

TypeInfo type_check_struct_def(ParseInfo* info, Stmt s) {
    StructType stype = at_index(&info->struct_types, s.struct_type_idx, StructType);

    // check that name is not alread in use
    for (size_t i = 0; i < info->struct_types.length; i++) {
        StructType search = at_index(&info->struct_types, i, StructType);
        if (src_loc_eql(stype.src_loc, search.src_loc)) break;
        if (strcmp(stype.name, search.name) == 0) {
            debug_crash("struct name '%s' (@ %s:%lu:%lu) already in use (@ %s:%lu:%lu)\n",
                stype.name, expand_src(stype), expand_src(search));
        }
    }

    // type check all the member declarations
    for (size_t i = 0; i < stype.members.length; i++) {
        StructMember member = at_index(&stype.members, i, StructMember);
        for (size_t j = i + 1; j < stype.members.length; j++) {
            StructMember search = at_index(&stype.members, j, StructMember);
            if (strcmp(member.name, search.name) == 0) {
                debug_crash("member name '%s' already used in struct type '%s'\n",
                    member.name, stype.name);
            }
        }
    }

    return (TypeInfo) {
        .type = Type_Struct,
        .name = s.name,
        .struct_idx = s.struct_type_idx,
    };
}

TypeInfo type_check_assignment(ParseInfo* info, Stmt s, size_t fn_idx) {
    Stmt left_stmt = at_index(&info->stmts, s.op_left, Stmt);
    TypeInfo left_type = type_check_stmt(info, left_stmt, fn_idx);
    Stmt right_stmt = at_index(&info->stmts, s.op_right, Stmt);
    TypeInfo right_type = type_check_stmt(info, right_stmt, fn_idx);
    check_types(right_type, left_type, right_stmt.src_loc);

    return left_type;
}

TypeInfo type_check_array_init(ParseInfo* info, Stmt s, size_t fn_idx) {
    // check that all members are of the same type
    TypeInfo biggest_type = {0};
    for (size_t i = 0; i < s.list.length; i++) {
        Stmt elem_stmt = indirect_index(&info->stmts, &s.list, i, Stmt);
        TypeInfo elem_type = type_check_stmt(info, elem_stmt, fn_idx);
        if (i == 0) { biggest_type = elem_type; continue; }

        check_types(elem_type, biggest_type, elem_stmt.src_loc);

        if (elem_type.n_bits > biggest_type.n_bits) biggest_type = elem_type;
    }

    TypeInfo array_type = biggest_type;
    array_type.is_array = true;
    array_type.array_size = s.list.length;
    array_type.is_comptime_const = false;

    find_stmt_ptr_in_list(info, s)->array_type = array_type;

    return array_type;
}

TypeInfo type_check_unary_op(ParseInfo* info, Stmt s, size_t fn_idx) {
    Stmt operand_stmt = at_index(&info->stmts, s.op_left, Stmt);
    TypeInfo operand_type = type_check_stmt(info, operand_stmt, fn_idx);

    // check that operation is valid for the operand type
    bool valid = false;
    switch (operand_type.type) {
        case Type_None: valid = true; break;
        case Type_Integer: valid = (s.op_token != Token_logical_not); break;
        case Type_Float: 
            valid = (s.op_token != Token_increment && s.op_token != Token_decrement);
            break;
        case Type_String: valid = false; break;
        case Type_Bool: valid = (s.op_token == Token_logical_not); break;
        case Type_Struct: valid = (s.op_token == Token_reference); break;
        case Type_FnPtr: valid = (s.op_token == Token_reference); break;
    }
    debug_assert(valid, "unary op %s is not valid on %s @ %s:%lu:%lu\n",
        token_type2str[s.op_token], type_type2str[operand_type.type], expand_src(s));

    if (s.op_token == Token_reference) operand_type.ptr_level += 1;
    if (s.op_token == Token_minus) operand_type.is_signed = true;

    return operand_type;
}

TypeInfo type_check_binary_op(ParseInfo* info, Stmt s, size_t fn_idx) {
    Stmt left_stmt = at_index(&info->stmts, s.op_left, Stmt);
    TypeInfo left_type = type_check_stmt(info, left_stmt, fn_idx);

    Stmt right_stmt = at_index(&info->stmts, s.op_right, Stmt);
    TypeInfo right_type = type_check_stmt(info, right_stmt, fn_idx);

    TokenType t = s.op_token;
    if (t == Token_div || t == Token_mod) {
        debug_assert(
            left_type.type == right_type.type && left_type.n_bits >= right_type.n_bits,
            "(%s, n_bits=%u) %s (%s, n_bits=%u) @ %s:%lu:%lu\n",
            type_type2str[left_type.type], left_type.n_bits,
            token_type2str[t],
            type_type2str[right_type.type], right_type.n_bits,
            expand_src(s));

        if (right_type.n_bits < left_type.n_bits) return right_type;
        else return left_type;
    }

    // no implicit conversion because it's not obvious which side would be cast to which
    debug_assert(types_match(left_type, right_type),
        "%s operands must have the same type @ %s:%lu:%lu: (%s, n_bits=%u) and (%s, n_bits=%u)\n",
        token_type2str[s.op_token], expand_src(s),
        type_type2str[left_type.type], left_type.n_bits,
        type_type2str[right_type.type], right_type.n_bits);
    
    // comparision binary ops (like ==, <, >, etc) return a boolean instead of the operand type
    if (t == Token_is_equal || t == Token_different
        || t == Token_smaller || t == Token_smaller_equal
        || t == Token_greater || t == Token_greater_equal) {
        return (TypeInfo) { .type = Type_Bool };
    }
    else {
        return left_type;
    }
}

TypeInfo type_check_indexing(ParseInfo* info, Stmt s, size_t fn_idx) {
    Stmt indexed_stmt = at_index(&info->stmts, s.indexed_stmt, Stmt);
    TypeInfo indexed_type = type_check_stmt(info, indexed_stmt, fn_idx);
    debug_assert(indexed_type.is_array || indexed_type.ptr_level != 0,
        "can only index arrays or pointers (@ %s:%lu:%lu)\n", expand_src(indexed_stmt));

    Stmt index_stmt = at_index(&info->stmts, s.index_stmt, Stmt);
    TypeInfo index_type = type_check_stmt(info, index_stmt, fn_idx);
    debug_assert(index_type.type == Type_Integer,
        "only integers can be used as indices (@ %s:%lu:%lu)\n", expand_src(index_stmt));

    TypeInfo element_type = indexed_type;
    element_type.is_array = false;
    if (element_type.ptr_level > 0) element_type.ptr_level--;
    return element_type;
}

TypeInfo type_check_dot_op(ParseInfo* info, Stmt s, size_t fn_idx) {
    // type check the variable part of this
    Stmt dummy = { .type = Stmt_Variable, .variable = s.variable };
    type_check_variable(info, dummy, fn_idx);

    // check that variable is a struct
    Variable var = at_index(&info->variables, s.variable, Variable);
    if (var.not_first) var = at_index(&info->variables, var.first, Variable);
    TypeInfo var_type = var.type_info;
    debug_assert(var_type.type == Type_Struct,
        "'%s' is not a struct, can't use '.' operation (@ %s:%lu:%lu)\n",
        var.name, expand_src(s));

    // check that member name exists
    StructType struct_type = at_index(&info->struct_types, var_type.struct_idx, StructType);
    bool found = false;
    size_t found_idx = 0;
    StructMember found_member = {0};
    for (size_t i = 0; i < struct_type.members.length; i++) {
        StructMember member = at_index(&struct_type.members, i, StructMember);
        if (strcmp(member.name, s.name) == 0) {
            found = true;
            found_idx = i;
            found_member = member;
        }
    }
    debug_assert(found,
        "%s:%lu:%lu: struct type '%s' (defined @ %s:%lu:%lu) does have a '%s' member\n",
        expand_src(s), struct_type.name, expand_src(struct_type), s.name);

    // fill in the 'struct_member_idx' field in Stmt_DotOp (need to use
    // source location to modify the stmt because we don't have it's idx or pointer)
    for (size_t i = 0; i < info->stmts.length; i++) {
        if (src_loc_eql(s.src_loc, at_index(&info->stmts, i, Stmt).src_loc)) {
            at_index(&info->stmts, i, Stmt).struct_member_idx = found_idx;
            break;
        }
    }

    // return type of accessed member
    return found_member.type_info;
}

TypeInfo type_check_stmt(ParseInfo* info, Stmt s, size_t fn_idx) {
    switch (s.type) {
        case Stmt_None: return (TypeInfo) { .type = Type_None };
        case Stmt_FunctionCall: return type_check_fn_call(info, s, fn_idx);
        case Stmt_If: return type_check_if(info, s, fn_idx);
        case Stmt_Else: return type_check_else(info, s, fn_idx);
        case Stmt_While: return type_check_while(info, s, fn_idx);
        case Stmt_For: return type_check_for(info, s, fn_idx);
        case Stmt_Break: return (TypeInfo) { .type = Type_None };
        case Stmt_Return: return type_check_return(info, s, fn_idx);
        case Stmt_Variable: return type_check_variable(info, s, fn_idx);
        case Stmt_Decl: return type_check_decl(info, s, fn_idx);
        case Stmt_StructDef: return type_check_struct_def(info, s);
        case Stmt_Assignment: return type_check_assignment(info, s, fn_idx);
        case Stmt_ArrayInit: return type_check_array_init(info, s, fn_idx);
        case Stmt_UnaryOp: return type_check_unary_op(info, s, fn_idx);
        case Stmt_BinaryOp: return type_check_binary_op(info, s, fn_idx);
        case Stmt_Indexing: return type_check_indexing(info, s, fn_idx);
        case Stmt_DotOp: return type_check_dot_op(info, s, fn_idx);
    }

    return (TypeInfo) {0};
}

void type_check_function(ParseInfo* info, Function fn, size_t fn_idx) {
    // for the 'main' function check that the arguments are one of the
    // allowed combinations
    if (strcmp(fn.name, "main") == 0) {
        debug_assert(fn.arg_types.length <= 2,
            "'main' function can take, at most, 2 arguments (not %lu)\n",
            fn.arg_types.length);

        // first argument, if present, must be argc and of type u32
        if (fn.arg_types.length > 0) {
            TypeInfo type_u32 = { .type = Type_Integer, .name = "u32", .n_bits = 32 };
            check_types(at_index(&fn.arg_types, 0, TypeInfo), type_u32, fn.src_loc);
        }

        // second argument, if present, must be argv and of type &u8
        if (fn.arg_types.length > 1) {
            TypeInfo type_u8_ref = {
                .type = Type_Integer, .name = "u8", .n_bits = 8, .ptr_level = 1,
            };
            check_types(at_index(&fn.arg_types, 1, TypeInfo), type_u8_ref, fn.src_loc);
        }
    }

    for (size_t i = 0; i < fn.stmts.length; i++) {
        Stmt stmt = indirect_index(&info->stmts, &fn.stmts, i, Stmt);
        type_check_stmt(info, stmt, fn_idx);
    }
}

void type_check_top_level_decl(ParseInfo* info, Stmt s) {
    // check for name conflicts
    Variable var = at_index(&info->variables, s.variable, Variable);
    for (size_t j = 0; j < info->top_level_decls.length; j++) {
        Stmt other_stmt = indirect_index(&info->stmts, &info->top_level_decls, j, Stmt);
        if (src_loc_eql(s.src_loc, other_stmt.src_loc)) break;
        Variable other_var = at_index(&info->variables, other_stmt.variable, Variable);
        if (strcmp(var.name, other_var.name) == 0) {
            debug_crash("variable '%s' @ %s:%lu:%lu already declared at %s:%lu:%lu\n",
                var.name, expand_src(var), expand_src(other_var));
        }
    }

    if (s.assign_stmt == 0) return;
    Stmt assign = at_index(&info->stmts, s.assign_stmt, Stmt);
    debug_assert(assign.type == Stmt_Variable || assign.type == Stmt_ArrayInit,
        "top level decls must be literals (%s instead)\n", stmt_type2str[assign.type]);
    // note: fn_idx=0 does *not* mean no function, but I don't know what else to pass here
    TypeInfo src_type = type_check_stmt(info, assign, 0);

    // check that the type of the right hand part of the assignment matches (or can
    // be implicitly casted to) the type of the declared variable
    Variable dst = at_index(&info->variables, s.variable, Variable);
    check_types(src_type, dst.type_info, dst.src_loc);

    // when parsing, the var part of the decl might not have an array size specified.
    // now that the decl has passed type checking we can udpate it.
    if (src_type.is_array && dst.type_info.is_array) {
        Variable* var_ptr = &at_index(&info->variables, s.variable, Variable);
        var_ptr->type_info.array_size = src_type.array_size;
        var.type_info = var_ptr->type_info;
    }
}

void do_type_checking(ParseInfo* info) {
    // make sure it's all init'd to 0, so we don't have any unpleasent debugging later
    memset(syscall_fn_table, 0, sizeof(SyscallFnTableInfo) * n_syscall_fns);

    // type check top level decls
    for (size_t i = 0; i < info->top_level_decls.length; i++) {
        Stmt stmt = indirect_index(&info->stmts, &info->top_level_decls, i, Stmt);
        if (stmt.type == Stmt_Decl) {
            type_check_top_level_decl(info, stmt);
        }
        else if (stmt.type == Stmt_StructDef) {
            type_check_struct_def(info, stmt);
        }
        else {
            debug_crash("%s should not be on top_level_decls\n", stmt_type2str[stmt.type]);
        }
    }

    // fill arg_types in all the functions to help type checking after
    for (size_t i = 0; i < info->functions.length; i++) {
        Function function = at_index(&info->functions, i, Function);
        List types = create_list(TypeInfo);
        for (size_t j = 0; j < function.args.length; j++) {
            TypeInfo arg_type = indirect_index(&info->variables, &function.args, j, Variable).type_info;
            append(&types, arg_type);
        }
        at_index(&info->functions, i, Function).arg_types = types;
    }

    for (size_t i = 0; i < info->functions.length; i++) {
        Function function = at_index(&info->functions, i, Function);

        // check function name isn't in use already (by another function or global variable)
        for (size_t j = 0; j < i; j++) {
            Function other_fn = at_index(&info->functions, j, Function);
            if (strcmp(function.name, other_fn.name) == 0) {
                debug_crash("function '%s' @ %s:%lu:%lu already declared at %s:%lu:%lu\n",
                    function.name, expand_src(function), expand_src(other_fn));
            }
        }
        for (size_t j = 0; j < info->top_level_decls.length; j++) {
            Stmt decl = indirect_index(&info->stmts, &info->top_level_decls, j, Stmt);
            Variable var = at_index(&info->variables, decl.variable, Variable);
            if (strcmp(function.name, var.name) == 0) {
                debug_crash("function '%s' @ %s:%lu:%lu already used @ %s:%lu:%lu\n",
                    function.name, expand_src(function), expand_src(decl));
            }
        }

        type_check_function(info, function, i);
    }
}
