#pragma once

#include "tokenizer.h"
#include "common.h"
#include "list.h"
#include "elf_debug_info.h"
#include "cyb_ir.h"

#include <stdbool.h>
#include <string.h>

typedef enum {
    Type_None,
    Type_Integer,
    Type_Float,
    Type_String,
    Type_Bool,
    Type_Struct,
    Type_FnPtr,
} Type_Type;
extern const char* type_type2str[7];

typedef struct {
    Type_Type type;
    uint8_t* name;

    bool is_signed;
    uint16_t n_bits;
    uint32_t ptr_level;

    bool is_array;
    size_t array_size;

    bool is_comptime_const;
    uint64_t comptime_value;

    size_t struct_idx; // index into ParseInfo's StructType list (filled during type checking)
    size_t fnptr_idx; // index into ParseInfo's FnPtrType list

    // used for debug information
    size_t bufpos;
} TypeInfo;

typedef enum {
    Stmt_None,
    Stmt_FunctionCall,
    Stmt_If,
    Stmt_Else,
    Stmt_While,
    Stmt_For,
    Stmt_Break,
    Stmt_Return,
    Stmt_Variable,
    Stmt_Decl,
    Stmt_StructDef,
    Stmt_Assignment,
    Stmt_ArrayInit,
    Stmt_UnaryOp,
    Stmt_BinaryOp,
    Stmt_Indexing,
    Stmt_DotOp,
} Stmt_Type;
// don't forget to update the string table when updating this enum
extern const char* stmt_type2str[18];

// the usage of the different fields on this struct is document below for each Stmt_Type
typedef struct {
    Stmt_Type type;
    SourceLocation src_loc;
    uint8_t* name;

    List list; // list of size_t's

    bool is_global;
    bool using_fnptr;

    union {
        size_t function_idx;
        size_t init_stmt;
        size_t return_stmt;
        size_t assign_stmt;
        size_t struct_type_idx;
        size_t struct_member_idx;
        size_t else_stmt;
        size_t op_left;
        size_t indexed_stmt;
    };
    size_t step_stmt;
    union {
        size_t cond_stmt;
        size_t variable;
        size_t op_right;
        size_t index_stmt;
    };

    TokenType op_token;
    TypeInfo array_type;
} Stmt;
// note: when referencing other stmts an index into ParseInfo's stmt table is used
//
// Stmt_FunctionCall:
// - name: identifier of function being called, used to find it later
// - list: list of arguments passed
// - function_idx: index into ParseInfo's func table (filled during type checking)
// - using_fnptr: 'true' if we're calling a function pointer variable
// - variable: fnptr variable, index into ParseInfo's variable table
// Stmt_If:
// - cond_stmt: stmt for the condition
// - list: block of stmts if condition is true
// - else_stmt: stmt for the else branch, could be another Stmt_If in case of 'else if' branch
// Stmt_Else
// - list: block of stmts
// Stmt_While:
// - cond_stmt: stmt for the condition part of the loop
// - list: block of stmts
// Stmt_For:
// - init_stmt, cond_stmt, step_stmt: stmts for the 3 parts of for loop
// - list: block of stmts
// Stmt_Break:
// -
// Stmt_Return:
// - return_stmt
// Stmt_Variable:
// - variable: index into ParseInfo's variable table
// Stmt_Decl:
// - is_global:
// - variable: index into ParseInfo's variable table
// - assign_stmt: if non-zero it's a variable assignment, and this is the right side of it
// Stmt_StructDef:
// - name: name of the new type created
// - list: Stmt_Decl's of the struct members
// - struct_type_idx: index into ParseInfo's StructType list
// Stmt_Assignment:
// - op_left: left hand side of assignment
// - op_right: right hand side of assignment
// Stmt_ArrayInit:
// - list: list of elements for the array
// - array_type: type of the array (filled during type checking)
// - variable: index into variable table (filled during function codegen)
// TODO: check if this ^ `variable` field for ArrayInit is needed
// Stmt_UnaryOp:
// - op_token: TokenType for the operation, used to identify which op to perform
// - op_left: first (and only) operand
// Stmt_BinaryOp:
// - op_token: TokenType for the operation, used to identify which op to perform
// - op_left, op_right: first and second operands
// Stmt_Indexing:
// - indexed_stmt: indexed stmt
// - index_stmt: the thing inside the brackets (stmt)
// - is_ref: hack to make references work
// Stmt_DotOp:
// - variable: variable that is of type struct (index into ParseInfo's variable table)
// - name: member name being accessed
// - struct_member_idx: what member in StructType list is used (filled during type checking)

typedef struct {
    uint8_t* name;
    TypeInfo type_info;
    size_t byte_offset;
} StructMember;

typedef struct {
    uint8_t* name;
    SourceLocation src_loc;
    List members; // list of StructMember's
    size_t bit_size;
} StructType;

typedef struct {
    List arg_types; // list of TypeInfo's
    TypeInfo return_type;
} FnPtrType;

typedef struct {
    SourceLocation src_loc;

    TypeInfo type_info;
    uint8_t* name;
    bool not_first; // first appearance of this variable
    size_t first; // index into var table with first mention of this variable
    bool is_value;
    uint64_t value;

    bool is_global;

    bool is_reference;

    // used during code generation later
    size_t bufpos; // for variables that go on the data buffer (only valid if not_first=false)
    uint32_t v_reg; // virtual register used for this var (also only valid if not_first=false)
    VarLocation loc;

    // @hack for IR
    IrObjRef obj_ref;
} Variable;

typedef struct {
    uint8_t* name;
    SourceLocation src_loc;

    List stmts; // index into stmt table
    List args; // index into variable table

    // because it might be usefull to loop over all stmts (primary or sub-stmts)
    // inside a given function we also provide these
    size_t start_stmts, end_stmts; // end_stmts is the first one that doesn't belong

    List arg_types; // TypeInfo's lifted from args list for convenience (filling in during type checking)
    TypeInfo return_type;

    bool is_extern;
    bool is_syscall; uint8_t code;
} Function;

typedef struct {
    uint8_t* filename;
    SourceLocation src_loc;
} Import;

typedef struct {
    List struct_types; // list of StructType's
    List fnptr_types; // list of FnPtrType's

    List functions; // list of Function's
    List stmts; // list of Stmt's
    List variables; // list of Variable's

    List top_level_decls; // list of size_t's: indices into ParseInfo.stmts

    List imports; // list of Import's

    DebugInfo dbg_info;
} ParseInfo;

ParseInfo parse_file(List tokens);
void merge_infos(ParseInfo* old_info, ParseInfo new_info); // merge new_info into old_info

void do_type_checking(ParseInfo* info); // in type_check.c

// in print_tree.c
void print_parse_info(ParseInfo info, FILE* f);
void print_tree(ParseInfo info); // experimental printing in tree form with special line characters
void dump_tree_as_dot(ParseInfo info, char* filename);
