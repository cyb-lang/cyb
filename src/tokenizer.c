#include "tokenizer.h"

#include <stdbool.h>
#include <string.h>

bool is_compound_assign(Token token) {
    TokenType t = token.type;
    return (t == Token_plus_equal || t == Token_minus_equal
        || t == Token_times_equal || t == Token_div_equal);
}

bool is_assign_operator(Token token) {
    TokenType t = token.type;
    return (t == Token_equal || is_compound_assign(token));
}

TokenType simple_op_from_compound(Token token) {
    switch (token.type) {
        case Token_plus_equal: return Token_plus;
        case Token_minus_equal: return Token_minus;
        case Token_times_equal: return Token_times;
        case Token_div_equal: return Token_div;
        default:
            debug_crash("%s\n", token_type2str[token.type]);
    }
}

bool is_left_unary_operator(Token token) {
    TokenType t = token.type;
    return (t == Token_address || t == Token_bitwise_not
        || t == Token_logical_not || t == Token_minus
        || t == Token_reference);
}

bool is_right_unary_operator(Token token) {
    TokenType t = token.type;
    return (t == Token_increment || t == Token_decrement);
}

bool is_binary_operator(Token token) {
    TokenType t = token.type;
    return (t == Token_bitwise_and || t == Token_bitwise_or
        || t == Token_bitwise_xor || t == Token_different || t == Token_div
        || t == Token_mod || t == Token_greater || t == Token_greater_equal
        || t == Token_is_equal || t == Token_minus || t == Token_plus
        || t == Token_reference || t == Token_smaller || t == Token_smaller_equal
        || t == Token_times || t == Token_times || t == Token_logical_and
        || t == Token_logical_or);
}

bool is_comparison_operator(Token token) {
    TokenType t = token.type;
    return (t == Token_smaller || t == Token_smaller_equal
        || t == Token_greater || t == Token_greater_equal
        || t == Token_is_equal ||t == Token_different);
}

bool is_single_char_token(char c) {
    char single_char_tokens[] = {
        '(', ')', '{', '}', '[', ']',
        ':', ';', ',', '.',
        '+', '-', '*', '/', '&', '@',
        '<', '>', '=', '!', '~', '^',
        '|', '%',
    };
    for(uint32_t i = 0; i < sizeof(single_char_tokens); i++) {
        if(c == single_char_tokens[i]) return true;
    }
    return false;
}

char escape_char(char escaped) {
    switch (escaped) {
        case '\\': return '\\';
        case '\'': return '\'';
        case 'n':  return '\n';
        case 't':  return '\t';
        case 'r':  return '\r';
        case '"':  return  '"';
        default:
            debug_crash("special char not recognized '\\%c'\n", escaped);
    }
}

bool is_whitespace(char c) {
    return (c == ' ' || c == '\n' || c == '\t');
}

bool is_valid_name(uint8_t* str) {
    // note: this function assumes that all the single char tokens
    // are already not present in the string passed in.
    // because of this we only need to check if the name starts with
    // a number or not
    return !(*str >= 48 && *str <= 57);
}

uint32_t find_next_of(char c, Buffer buf, uint32_t start) {
    while(start < buf.size && buf.data[start] != c) start++;
    return start;
}

uint32_t find_next_nonspace(Buffer buf, uint32_t start) {
    while(start < buf.size && !is_whitespace(buf.data[start])) {
        start++;
    }
    return start;
}

uint32_t find_end_with_nest(Buffer buf, uint32_t start) {
    char open_char = buf.data[start];
    char close_char;
    if(open_char == '(') close_char = ')';
    if(open_char == '{') close_char = '}';
    if(open_char == '[') close_char = ']';

    uint32_t nesting = 1;
    uint32_t i = start;
    while(i < buf.size && nesting != 0) {
        i++;
        if(buf.data[i] == open_char) nesting++;
        if(buf.data[i] == close_char) nesting--;
    }
    return i;
}

Token add_identifier(uint8_t** str_table, uint8_t** saved_str) {
    **str_table = '\0';
    *str_table += 1;
    Token token = { .name = NULL };

    if(*saved_str[0] == '#') {
        // note: the order on this array must be the same as the one in the enum (and import
        // must be the first)
        char* directives[] = {
            "import", "macro", "alias", "comprun", "if", "else", "elif",
        };
        uint32_t n_directives = 7;

        bool found_match = false;
        for(uint32_t i = 0; i < n_directives; i++) {
            if(strcmp((char*) (*saved_str + 1), directives[i]) == 0) {
                token.type = (TokenType) (((uint32_t) Token_import) + i);
                found_match = true;
            }
        }
        debug_assert(found_match, "'%s' is not a valid compiler directive\n", *saved_str)
    }
    else {
        // note: the order on this array must be the same as the one in the enum
        char* keywords[] = {
            "goto", "if", "else", "for", "while", "do", "break", "switch", "fall",
            "struct", "enum", "inherits", "defer", "return", "self", "const", "public",
            "static", "as", "extern", "nonamespace", "fnptr", "true", "false",
        };
        uint32_t n_keywords = sizeof(keywords) / sizeof(char*);
        bool found_keyword = false;
        for(uint32_t i = 0; i < n_keywords; i++) {
            if(strcmp((char*) *saved_str, keywords[i]) == 0) {
                token.type = (TokenType) (((uint32_t) Token_goto) + i);
                found_keyword = true;
            }
        }

        if(!found_keyword) {
            // TODO: check for floats

            token.name = *saved_str;
            if(is_valid_name(token.name)) {
                token.type = Token_identifier;
            }
            else {
                token.type = Token_integer;
            }
        }
    }

    *saved_str = *str_table;
    return token;
}

// in case `token` is a Token_integer and the one immediatly before it was Token_minus
// change the token list so we have a Token_plus and a signed Token_integer.
// (essentially doing this: "(...) - 1" -> "(...) + -1")
// returns the new token that should be append to the list 
Token check_for_negative_integer(Token token, List* tokens, uint8_t** str_table, uint8_t** saved_str) {
    // note: disabled for now. not sure if this will be usefull in the future
    return token;

    if (tokens->length == 0) return token;

    Token* last_tk = &at_index(tokens, tokens->length - 1, Token);
    if (!(token.type == Token_integer && last_tk->type == Token_minus)) return token;

    // in the token list "Token_minus, Token_integer" becomes "Token_plus, Token_integer"
    last_tk->type = Token_plus; //                                        ^ signed

    // move all chars one forward, insert the minus sign
    uint8_t* cursor = *str_table;
    while (cursor > token.name) { *cursor = *(cursor - 1); cursor--; }
    *cursor = '-';

    // fix str_table bookkeping
    *str_table += 1;
    **str_table = '\0';
    *saved_str = *str_table;

    return token;
}

List tokenize(Buffer file, uint8_t** str_table, size_t file_n, uint8_t* filename) {
    List tokens = create_list(Token);

    size_t cur_line = 1, cur_column = 1;
    size_t saved_line = 1, saved_column = 1; // for keeping track of identifier start while reading it
    bool just_read_token = true;
    bool reading_str = false;
    uint8_t* saved_str = *str_table;
    for(size_t i = 0; i < file.size; i++) {
        char c = file.data[i];

        cur_column++;
        if (c == '\n') { cur_line++; cur_column = 1; }

        // check for comments before anything else because '/' is actually a token
        // ignore single-line comments
        if(c == '/' && (file.size > i + 1 && file.data[i + 1] == '/')) {
            // jump ahead by changing the current loop index (yes, I know this
            // is taboo, but it is also, probably, the simplest and least
            // convoluted way to do this)
            i = find_next_of('\n', file, i);
            // now 'i' is the position of the newline. we want to move to the
            // next character. 'continue' will still increment i so all is good
            cur_line++;
            cur_column = 1;
            continue;
        }

        // strings
        if (c == '"') {
            Token token = {
                .type = Token_string,
                .src_loc = {
                    .file = file_n, .line = cur_line, .column = cur_column - 1, .filename = filename
                },
            };
            // TODO: deal with escaping charaters
            c = file.data[++i];
            cur_column++;
            while (c != '"') {
                char actual_c = c;
                if (c == '\\') { actual_c = escape_char(file.data[++i]); cur_column++; }

                **str_table = actual_c;
                *str_table += 1;
                
                if (c == '\n') { cur_line++; cur_column = 1; }

                c = file.data[++i];
                cur_column++;
            }
            **str_table = '\0';
            *str_table += 1;
            token.name = saved_str;
            append(&tokens, token);

            saved_str = *str_table;
            saved_line = cur_line;
            saved_column = cur_column - 1;
            continue;
        }

        // single char literals
        if (c == '\'') {
            Token token = {
                .type = Token_char,
                .src_loc = {
                    .file = file_n, .line = cur_line, .column = cur_column - 1, .filename = filename
                },
            };

            char char_lit = file.data[++i];
            if (char_lit == '\\') char_lit = escape_char(file.data[++i]);

            debug_assert(file.data[++i] == '\'', "invalid character literal at %s:%lu:%lu\n",
                expand_src(token));

            token.name = *str_table; 
            **str_table = char_lit; *str_table += 1;
            **str_table = '\0'; *str_table += 1;

            append(&tokens, token);

            saved_str = *str_table;
            saved_line = cur_line;
            saved_column = cur_column - 1;
            continue;
        }

        // if we were reading an identifier and found whitespace -> identifier ended
        if(is_whitespace(c) && !reading_str) {
            if(*str_table != saved_str) {
                Token token = add_identifier(str_table, &saved_str);
                token.src_loc = (SourceLocation) {
                    .file = file_n, .line = saved_line, .column = saved_column, .filename = filename
                };
                token = check_for_negative_integer(token, &tokens, str_table, &saved_str);
                append(&tokens, token);
                just_read_token = true;
            }
            continue;
        }

        // check if is single char token
        if(is_single_char_token(c)) {
            if(*str_table != saved_str) { // we were reading an identifier
                Token token = add_identifier(str_table, &saved_str);
                token.src_loc = (SourceLocation) {
                    .file = file_n, .line = saved_line, .column = saved_column, .filename = filename
                };
                token = check_for_negative_integer(token, &tokens, str_table, &saved_str);
                append(&tokens, token);
                just_read_token = true;
            }

            // TODO: right now we can't distinguish between a logical 'and' and
            // a type declaration that has 2 or more levels of references (e.g &&u8)
            // we could try and check the token immediatly before it, but this might
            // not be possible (at least not in a simple way) because if the type
            // appears in a variable declaration the token before, i.e the last token
            // of the previous line, could be almost anything.
            // either way this is probably going to always be very hacky and fragile.
            // right now we always assume logical 'and'

            Token token = { .src_loc = {
                .file = file_n, .line = cur_line, .column = cur_column - 1, .filename = filename
            } };

            char next_char = file.data[i + 1];
            debug_assert(is_single_char_token(c) && c != 'c', "not supporting this combo %c%c\n", c, next_char);

            switch(c) {
                case '(': token.type = Token_open_parentheses; break;
                case ')': token.type = Token_close_parentheses; break;
                case '{': token.type = Token_start_scope; break;
                case '}': token.type = Token_end_scope; break;
                case '[': token.type = Token_open_brackets; break;
                case ']': token.type = Token_close_brackets; break;
                case ':': token.type = Token_colon; break;
                case ';': token.type = Token_semicolon; break;
                case ',': token.type = Token_comma; break;
                case '.': token.type = Token_dot; break;
                case '+':
                    if (next_char == '+') { token.type = Token_increment; i++; }
                    else if (next_char == '=') { token.type = Token_plus_equal; i++; }
                    else token.type = Token_plus;
                    break;
                case '-':
                    if (next_char == '-') { token.type = Token_decrement; i++; }
                    else if (next_char == '=') { token.type = Token_minus_equal; i++; }
                    else token.type = Token_minus;
                    break;
                case '*': 
                    if (next_char == '=') { token.type = Token_times_equal; i++; }
                    else token.type = Token_times;
                    break;
                case '/':
                    if (next_char == '=') { token.type = Token_div_equal; i++; }
                    else token.type = Token_div;
                    break;
                case '&':
                    if (next_char == '&') { token.type = Token_logical_and; i++; }
                    else if (next_char == ' ') { token.type = Token_bitwise_and; i++; }
                    else token.type = Token_reference;
                    break;
                case '@': token.type = Token_address; break;
                case '<':
                    if (next_char == '=') { token.type = Token_smaller_equal; i++; }
                    else token.type = Token_smaller;
                    break;
                case '>':
                    if (next_char == '=') { token.type = Token_greater_equal; i++; }
                    else token.type = Token_greater;
                    break;
                case '=':
                    if (next_char == '=') { token.type = Token_is_equal; i++; }
                    else token.type = Token_equal;
                    break;
                case '!':
                    if (next_char == '=') { token.type = Token_different; i++; }
                    else token.type = Token_logical_not;
                    break;
                case '~': token.type = Token_bitwise_not; break;
                case '^': token.type = Token_bitwise_xor; break;
                case '|':
                    if (next_char == '|') { token.type = Token_logical_or; i++; }
                    else token.type = Token_bitwise_or;
                    break;
                case '%':
                    if (next_char == '=') { token.type = Token_mod_equal; i++; }
                    else token.type = Token_mod;
                    break;

                default: debug_crash("unrecognized single char token: '%c'\n", c);
            }
            append(&tokens, token);
            just_read_token = true;
            continue;
        }

        // if we reached this part of the loop and this is true that means we just
        // started reading the next identifier (whitespace is ignored above)
        if (just_read_token) {
            saved_line = cur_line;
            saved_column = cur_column - 1;
            just_read_token = false;
        }

        **str_table = c;
        *str_table += 1;
    }

    return tokens;
}

bool src_loc_eql(SourceLocation a, SourceLocation b) {
    return (a.file == b.file && a.line == b.line && a.column == b.column);
}

void print_token(Token token) {
    printf("%s:%lu:%lu ", expand_src(token));
    switch(token.type) {
        case Token_identifier:
        case Token_integer:
        case Token_float:
        case Token_string:
            printf("%s (%s)\n", token_type2str[token.type], token.name);
            break;

        default:
            printf("%s\n", token_type2str[token.type]);
    }
}
