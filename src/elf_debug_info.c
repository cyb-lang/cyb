#include "elf_debug_info.h"
#include "parser.h"
#include "common.h"

#include <elf.h>
#include <dwarf.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

void encode_uleb128(Buffer* buf, uint64_t value) {
    do {
        uint8_t byte = value & 0x7f;
        value >>= 7;
        if (value != 0) byte |= 0x80;
        buf->data[buf->pos++] = byte;
    } while (value != 0);
}

void encode_sleb128(Buffer* buf, int64_t value) {
    bool more = true;
    bool negative = (value < 0);
    uint8_t size = 64;
    while (more) {
        uint8_t byte = value & 0x7f;
        value >>= 7;

        if (negative) value |= -(1 << (size - 7));

        if ((value == 0 && !(byte & 0x40)) || (value == -1 && (byte & 0x40))) more = false;
        else byte |= 0x80;

        buf->data[buf->pos++] = byte;
    }
}

Buffer write_debug_line_section(DebugInfo dbg_info) {
    Buffer buf = new_buffer(0x1000);
    // .debug_line header (most of these where copied from the cyb binary)
    buf.pos += 4; // leave space for compilation unit line info length later
    write_u16(&buf, 3); // version
    buf.pos += 4; // leave space header length later
    write_u8(&buf, 1); // min instruction length
    write_u8(&buf, 1); // is_stmt default value
    const int8_t line_base = -5; 
    write_u8(&buf, line_base);
    const uint8_t line_range = 14; 
    write_u8(&buf, line_range);
    const uint8_t opcode_base = 13; 
    write_u8(&buf, opcode_base);
    uint8_t std_opcodes_nargs[12] = {0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1};
    memcpy(buf.data + buf.pos, std_opcodes_nargs, 12);
    buf.pos += 12;
    // include_directories entry
    str_table_add(&buf, dbg_info.compile_dir);
    write_u8(&buf, 0); // end include_directories list
    // file_names entry
    for (size_t i = 0; i < dbg_info.filenames.length; i++) {
        str_table_add(&buf, at_index(&dbg_info.filenames, i, uint8_t*));
        encode_uleb128(&buf, 1); // directory from include_directories
        encode_uleb128(&buf, 0); // modif. time
        encode_uleb128(&buf, 0); // length
    }
    write_u8(&buf, 0); // end of file_names list
    *((uint32_t*) &buf.data[6]) = buf.pos - 10; // fix header length

    // debug line program instructions
    size_t cur_file = 0;
    size_t cur_line = 1;
    size_t cur_addr = dbg_info.virtual_addr + dbg_info.code_addr;
    write_u8(&buf, 0); // because extended opcode
    encode_uleb128(&buf, 9); // opcode length: opcode byte + address
    write_u8(&buf, DW_LNE_set_address);
    write_u64(&buf, cur_addr);
    write_u8(&buf, DW_LNS_set_file); // set file to 1
    encode_uleb128(&buf, 1);
    write_u8(&buf, DW_LNS_set_column); // set column to 1
    encode_uleb128(&buf, 1);
    // make a new row with this info
    write_u8(&buf, DW_LNS_copy);
    // TODO: make sure these are sorted by increasing address
    for (size_t i = 0; i < dbg_info.line_addrs.length; i++) {
        LineAddr line_addr = at_index(&dbg_info.line_addrs, i, LineAddr);
        size_t real_addr = line_addr.addr + dbg_info.virtual_addr + dbg_info.code_addr;
        size_t line_advance = line_addr.src_loc.line - cur_line;
        size_t addr_advance = real_addr - cur_addr;

        uint16_t opcode = (line_advance - line_base) + (line_range * addr_advance) + opcode_base;

        if (line_addr.src_loc.file > cur_file) {
            cur_file = line_addr.src_loc.file;
            write_u8(&buf, DW_LNS_set_file);
            encode_uleb128(&buf, cur_file + 1);
        }

        // if we can, use "special opcode" because it only uses 1 byte 
        if (line_advance <= (size_t) (line_base + line_range - 1) && opcode < 255) {
            write_u8(&buf, opcode);
        }
        else {
            // set address
            write_u8(&buf, 0); // because extended opcode
            encode_uleb128(&buf, 9); // opcode length: opcode byte + address
            write_u8(&buf, DW_LNE_set_address);
            write_u64(&buf, real_addr);
            // advance line
            write_u8(&buf, DW_LNS_advance_line);
            encode_uleb128(&buf, line_addr.src_loc.line - cur_line);

            // make a new row with this info
            write_u8(&buf, DW_LNS_copy);
        }

        cur_line = line_addr.src_loc.line;
        cur_addr = real_addr;
    }
    write_u8(&buf, 0);
    encode_uleb128(&buf, 1);
    write_u8(&buf, DW_LNE_end_sequence);
    *((uint32_t*) &buf.data[0]) = buf.pos - 4; // fix header length field

    return buf;
}

Buffer write_debug_abbrev_section(DebugInfo* dbg_info) {
    Buffer buf = new_buffer(0x1000);

    uint32_t abbrev_code = 1;

    // compilation unit abbrev
    dbg_info->tag_abbrev_codes[DW_TAG_compile_unit] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_compile_unit); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_yes; // children are functions
    encode_uleb128(&buf, DW_AT_producer); // compiler name
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_name); // attrib will be the filename
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_comp_dir); // attrib will be the compile directory
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_stmt_list); // which part of line information to use
    encode_uleb128(&buf, DW_FORM_data4); // offset into .debug_line
    encode_uleb128(&buf, DW_AT_low_pc); // start of address
    encode_uleb128(&buf, DW_FORM_addr);
    encode_uleb128(&buf, DW_AT_high_pc); // size of address range
    encode_uleb128(&buf, DW_FORM_addr);
    encode_uleb128(&buf, DW_AT_language); // language? pretent it's C99?
    encode_uleb128(&buf, DW_FORM_data2);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // subprogram abbrev (function, basically)
    dbg_info->tag_abbrev_codes[DW_TAG_subprogram] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_subprogram); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_yes; // children: types and variables
    encode_uleb128(&buf, DW_AT_name); // attrib will be the filename
    encode_uleb128(&buf, DW_FORM_string); // attrib form will be string
    encode_uleb128(&buf, DW_AT_decl_file);
    encode_uleb128(&buf, DW_FORM_data1);
    encode_uleb128(&buf, DW_AT_decl_line);
    encode_uleb128(&buf, DW_FORM_data1);
    encode_uleb128(&buf, DW_AT_decl_column);
    encode_uleb128(&buf, DW_FORM_data1);
    encode_uleb128(&buf, DW_AT_low_pc); // start of function address
    encode_uleb128(&buf, DW_FORM_addr);
    encode_uleb128(&buf, DW_AT_high_pc); // size of function
    encode_uleb128(&buf, DW_FORM_addr);
    encode_uleb128(&buf, DW_AT_external); // symbols is external (yes/no)
    encode_uleb128(&buf, DW_FORM_flag);
    encode_uleb128(&buf, DW_AT_frame_base); // out to calc. base frame
    encode_uleb128(&buf, DW_FORM_block); // a DWARF expression
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // base type info
    dbg_info->tag_abbrev_codes[DW_TAG_base_type] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_base_type); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_no; // no children
    encode_uleb128(&buf, DW_AT_name);
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_encoding);
    encode_uleb128(&buf, DW_FORM_data1);
    encode_uleb128(&buf, DW_AT_byte_size);
    encode_uleb128(&buf, DW_FORM_data1);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // pointer type info
    dbg_info->tag_abbrev_codes[DW_TAG_pointer_type] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_pointer_type); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_no; // no children
    encode_uleb128(&buf, DW_AT_name); // name of type
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_type); // offset to type entry
    encode_uleb128(&buf, DW_FORM_ref4);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // structure type info
    dbg_info->tag_abbrev_codes[DW_TAG_structure_type] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_structure_type); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_yes;
    encode_uleb128(&buf, DW_AT_name);
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_byte_size);
    encode_uleb128(&buf, DW_FORM_data4);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // structure member type info
    dbg_info->tag_abbrev_codes[DW_TAG_member] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_member); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_no;
    encode_uleb128(&buf, DW_AT_name);
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_type); // offset to type entry
    encode_uleb128(&buf, DW_FORM_ref4);
    encode_uleb128(&buf, DW_AT_data_member_location); // byte offset within struct
    encode_uleb128(&buf, DW_FORM_data4);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // array type info
    dbg_info->tag_abbrev_codes[DW_TAG_array_type] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_array_type); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_yes;
    encode_uleb128(&buf, DW_AT_name);
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_type); // offset to type entry
    encode_uleb128(&buf, DW_FORM_ref4);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // subrange type info (needed for array)
    dbg_info->tag_abbrev_codes[DW_TAG_subrange_type] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_subrange_type); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_no;
    encode_uleb128(&buf, DW_AT_type); // offset to type entry
    encode_uleb128(&buf, DW_FORM_ref4);
    encode_uleb128(&buf, DW_AT_count); // number of elements
    encode_uleb128(&buf, DW_FORM_data4);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // variable info
    dbg_info->tag_abbrev_codes[DW_TAG_variable] = abbrev_code;
    encode_uleb128(&buf, abbrev_code++);
    encode_uleb128(&buf, DW_TAG_variable); // abbrev tag
    buf.data[buf.pos++] = DW_CHILDREN_no; // no children
    encode_uleb128(&buf, DW_AT_name); // name of variable
    encode_uleb128(&buf, DW_FORM_string);
    encode_uleb128(&buf, DW_AT_type); // offset to type entry
    encode_uleb128(&buf, DW_FORM_ref4);
    encode_uleb128(&buf, DW_AT_location); // location of variable in memory
    encode_uleb128(&buf, DW_FORM_block1);
    encode_uleb128(&buf, 0); // terminating attribute (name part)
    encode_uleb128(&buf, 0); // terminating attribute (form part)

    // end of abbrev list
    encode_uleb128(&buf, 0);

    return buf;
}

void write_dbg_var_list(Buffer* buf, DebugInfo dbg_info, List var_list) {
    for (size_t i = 0; i < var_list.length; i++) {
        VarInfo var = at_index(&var_list, i, VarInfo);
        TypeInfo type = at_index(&dbg_info.types, var.type_idx, TypeInfo);

        encode_uleb128(buf, dbg_info.tag_abbrev_codes[DW_TAG_variable]);

        str_table_add(buf, var.name);
        write_u32(buf, type.bufpos);

        if (var.loc.type == Loc_Memory) {
            encode_uleb128(buf, 9);
            write_u8(buf, DW_OP_addr);
            write_u64(buf, var.loc.addr);
        }
        else if (var.loc.type == Loc_Imm) {
            encode_uleb128(buf, 9);
            write_u8(buf, DW_OP_const8u);
            write_u64(buf, var.loc.imm);
        }
        else if (var.loc.type == Loc_Stack) {
            size_t encode_start = buf->pos;
            encode_sleb128(buf, var.loc.stack_pos);
            size_t encoded_size = buf->pos - encode_start;
            buf->pos = encode_start;

            encode_uleb128(buf, 1 + encoded_size);
            write_u8(buf, DW_OP_fbreg);
            encode_sleb128(buf, var.loc.stack_pos);
        }
        else if (var.loc.type == Loc_Register) {
            encode_uleb128(buf, 1);
            write_u8(buf, DW_OP_reg0 + var.loc.reg);
        }
        else {
            debug_crash("'%s' loc.type = %u\n", var.name, var.loc.type);
        }
    }
}

TypeInfo write_debug_simple_type_info(DebugInfo dbg_info, Buffer* buf, TypeInfo type) {
    uint8_t encoding = DW_ATE_unsigned;
    if (type.is_signed) encoding = DW_ATE_signed;
    else if (type.type == Type_Bool) encoding = DW_ATE_boolean;

    encode_uleb128(buf, dbg_info.tag_abbrev_codes[DW_TAG_base_type]);

    str_table_add(buf, type.name);
    write_u8(buf, encoding);
    write_u8(buf, type.n_bits / 8);

    if (type.ptr_level != 0) {
        debug_assert(type.ptr_level <= 1, "multiple levels of pointer not implement\n");

        size_t base_type_bufpos = type.bufpos;
        type.bufpos = buf->pos;

        encode_uleb128(buf, dbg_info.tag_abbrev_codes[DW_TAG_pointer_type]);

        char tmpbuf[1000]; snprintf(tmpbuf, 1000, "&%s", type.name);
        str_table_add(buf, tmpbuf);
        write_u32(buf, base_type_bufpos);
    }

    return type;
}

Buffer write_debug_info_section(DebugInfo dbg_info) {
    Buffer buf = new_buffer(0x1000);
    // .debug_info header
    buf.pos += 4; // space for unit length, fixed later
    write_u16(&buf, 3); // version
    write_u32(&buf, 0); // offset into .debug_abbrev
    write_u8(&buf, 8); // address size in bytes

    // compilation unit entry 
    encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_compile_unit]);
    str_table_add(&buf, "Cyb compiler");
    str_table_add(&buf, at_index(&dbg_info.filenames, 0, uint8_t*)); // root filename
    str_table_add(&buf, dbg_info.compile_dir); // the second attribute value (comp_dir)
    write_u32(&buf, 0); // 3rd attrib: offset info .debug_line
    write_u64(&buf, dbg_info.virtual_addr + dbg_info.code_addr);
    write_u64(&buf, dbg_info.virtual_addr + dbg_info.code_addr + 0x1000); // TODO: what if bigger than one page
    write_u16(&buf, DW_LANG_C99); // pretend we're C99?

    // type info entries
    for (size_t i = 0; i < dbg_info.types.length; i++) {
        TypeInfo type = at_index(&dbg_info.types, i, TypeInfo);
        type.bufpos = buf.pos;

        if (type.type == Type_Struct) {
            debug_assert(type.ptr_level == 0, "pointer to struct not done\n");
            StructType stype = at_index(&dbg_info.struct_types, type.struct_idx, StructType);

            encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_structure_type]);

            str_table_add(&buf, stype.name);
            write_u32(&buf, stype.bit_size / 8);

            for (size_t m = 0; m < stype.members.length; m++) {
                StructMember member = at_index(&stype.members, m, StructMember);

                size_t bufpos = buf.pos;
                write_debug_simple_type_info(dbg_info, &buf, member.type_info);

                encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_member]);
                str_table_add(&buf, member.name);
                write_u32(&buf, bufpos);
                write_u32(&buf, member.byte_offset);
            }

            encode_uleb128(&buf, 0); // mark end of children for structure_type entry
        }
        else if (type.is_array) {
            debug_assert(type.type != Type_Struct, "array of structs debug info not done\n");

            size_t elem_type_bufpos = buf.pos;
            write_debug_simple_type_info(dbg_info, &buf, type);

            type.bufpos = buf.pos;
            encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_array_type]);
            char tmpbuf[1000]; snprintf(tmpbuf, 1000, "%s[%lu]", type.name, type.array_size);
            str_table_add(&buf, tmpbuf);
            write_u32(&buf, elem_type_bufpos);

            // need this subrange_type entry even for 1-dim array, not sure why
            encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_subrange_type]);
            write_u32(&buf, elem_type_bufpos);
            write_u32(&buf, type.array_size);

            encode_uleb128(&buf, 0); // mark end of children for array_type entry
        }
        else {
            type = write_debug_simple_type_info(dbg_info, &buf, type);
        }

        at_index(&dbg_info.types, i, TypeInfo) = type;
    }

    // global variables info entries
    write_dbg_var_list(&buf, dbg_info, dbg_info.global_vars);

    // entries for each function
    for (size_t i = 0; i < dbg_info.funcs.length; i++) {
        FuncInfo fn_dbg = at_index(&dbg_info.funcs, i, FuncInfo);
        encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_subprogram]);
        str_table_add(&buf, fn_dbg.name);

        write_u8(&buf, 1); // file
        write_u8(&buf, fn_dbg.line); // line
        write_u8(&buf, 1); // column

        write_u64(&buf, fn_dbg.start_addr + dbg_info.virtual_addr + dbg_info.code_addr);
        write_u64(&buf, fn_dbg.start_addr + dbg_info.virtual_addr + dbg_info.code_addr + fn_dbg.size);

        write_u8(&buf, 1); // yes, it is external

        encode_uleb128(&buf, 1); // block size
        write_u8(&buf, 0x9c); // expr: DW_OP_call_frame_cfa

        // local variables
        for (size_t j = 0; j < fn_dbg.vars.length; j++) {
            VarInfo var = at_index(&fn_dbg.vars, j, VarInfo);

            encode_uleb128(&buf, dbg_info.tag_abbrev_codes[DW_TAG_variable]);

            str_table_add(&buf, var.name);
            write_u32(&buf, at_index(&dbg_info.types, var.type_idx, TypeInfo).bufpos);

            if (var.loc.type == Loc_Memory || var.loc.type == Loc_Imm) {
                encode_uleb128(&buf, 9);
                write_u8(&buf, DW_OP_addr);
                write_u64(&buf, var.loc.addr);
            }
            else if (var.loc.type == Loc_Stack) {
                size_t encode_start = buf.pos;
                encode_sleb128(&buf, var.loc.stack_pos);
                size_t encoded_size = buf.pos - encode_start;
                buf.pos = encode_start;

                encode_uleb128(&buf, 1 + encoded_size);
                write_u8(&buf, DW_OP_fbreg);
                encode_sleb128(&buf, var.loc.stack_pos);
            }
            else if (var.loc.type == Loc_Register) {
                encode_uleb128(&buf, 1);
                write_u8(&buf, DW_OP_reg0 + var.loc.reg);
            }
            else {
                debug_crash("'%s' loc.type = %u\n", var.name, var.loc.type);
            }
        }

        encode_uleb128(&buf, 0); // mark end of children for function entry
    }

    encode_uleb128(&buf, 0); // mark end of children for compilation unit entry

    *((uint32_t*) &buf.data[0]) = buf.pos - 4; // fix header length field

    return buf;
}

// order used for register codes in DWARF
enum {
    rax = 0, rdx, rcx, rbx, rsi, rdi, rbp, rsp,
    r8, r9, r10, r11, r12, r13, r14, r15,
    rip, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6,
    xmm7, xmm8, xmm9, xmm10, xmm11, xmm12, xmm13, xmm14
};

Buffer write_debug_frame_section(DebugInfo dbg_info) {
    Buffer buf = new_buffer(0x1000);
    // CIE
    buf.pos += 4; // space for length, fixed later
    write_u32(&buf, DW_CIE_ID_32);
    write_u8(&buf, 3); // version
    write_u8(&buf, 0); // no aug
    //str_table_add(&buf, "zR");
    encode_uleb128(&buf, 1); // code alignment
    encode_sleb128(&buf, -1); // data alignment??
    buf.data[buf.pos++] = 16; // return address register
    //encode_uleb128(&buf, 1); // size of augmentation dataa
    //write_u8(&buf, 0x1b); // specifies encoding of address in FDE's
    // things that are the same for all functions:
    // - CFA (i.e start of function frame address) is just the rbp register
    write_u8(&buf, DW_CFA_def_cfa);
    encode_uleb128(&buf, rbp);
    encode_uleb128(&buf, 0);
    // - rip gets saved on the stack, always last thing on stack before the function frame
    write_u8(&buf, DW_CFA_offset | 16); // rip = 16
    encode_uleb128(&buf, 8); // gets multiplied by data_alignment, so really, its -8
    buf.pos = align_to_next(buf.pos, 8);
    *((uint32_t*) &buf.data[0]) = buf.pos - 4; // fix CIE length field
    // FDE for each function
    for (size_t i = 0; i < dbg_info.funcs.length; i++) {
        FuncInfo fn_addr = at_index(&dbg_info.funcs, i, FuncInfo);
        size_t len_bufpos = buf.pos;
        buf.pos += 4; // space for FDE length, fix at the end FDE
        write_u32(&buf, 0); // offset to CIE in section
        uint64_t encoded_pc = dbg_info.virtual_addr + dbg_info.code_addr + fn_addr.start_addr;
        write_u64(&buf, encoded_pc);
        write_u64(&buf, fn_addr.size);
        encode_uleb128(&buf, 0); // no aug data in our FDE's

        buf.pos = align_to_next(buf.pos, 8);

        // fix FDE length
        *((uint32_t*) (buf.data + len_bufpos)) = buf.pos - len_bufpos - 4;
    }

    return buf;
}

void write_debug_sections(FILE* elf_file, List* section_headers, Buffer* section_name_strtab, DebugInfo dbg_info) {
    Buffer debug_line_buffer = write_debug_line_section(dbg_info);
    Elf64_Shdr debug_line_section = {
        .sh_name = str_table_add(section_name_strtab, ".debug_line"),
        .sh_type = SHT_PROGBITS,
        .sh_offset = ftell(elf_file),
        .sh_size = debug_line_buffer.pos,
    };
    fwrite(debug_line_buffer.data, 1, debug_line_buffer.pos, elf_file);
    free(debug_line_buffer.data);

    Buffer debug_abbrev_buffer = write_debug_abbrev_section(&dbg_info);
    Elf64_Shdr debug_abbrev_section = {
        .sh_name = str_table_add(section_name_strtab, ".debug_abbrev"),
        .sh_type = SHT_PROGBITS,
        .sh_offset = ftell(elf_file),
        .sh_size = debug_abbrev_buffer.pos,
    };
    fwrite(debug_abbrev_buffer.data, 1, debug_abbrev_buffer.pos, elf_file);

    Buffer debug_info_buffer = write_debug_info_section(dbg_info);
    Elf64_Shdr debug_info_section = {
        .sh_name = str_table_add(section_name_strtab, ".debug_info"),
        .sh_type = SHT_PROGBITS,
        .sh_offset = ftell(elf_file),
        .sh_size = debug_info_buffer.pos,
    };
    fwrite(debug_info_buffer.data, 1, debug_info_buffer.pos, elf_file);

    Buffer debug_frame_buffer = write_debug_frame_section(dbg_info);
    Elf64_Shdr debug_frame_section = {
        .sh_name = str_table_add(section_name_strtab, ".debug_frame"),
        .sh_type = SHT_PROGBITS,
        .sh_offset = ftell(elf_file),
        .sh_size = debug_frame_buffer.pos,
    };
    fwrite(debug_frame_buffer.data, 1, debug_frame_buffer.pos, elf_file);

    append(section_headers, debug_line_section);
    append(section_headers, debug_abbrev_section);
    append(section_headers, debug_info_section);
    append(section_headers, debug_frame_section);
}
