#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#include "cmd_options.h"
#include "tokenizer.h"
#include "parser.h"
#include "write_elf_x64.h"
#include "cyb_ir.h"
#include "emit_ir.h"

Buffer read_file_relative(uint8_t* comp_dir, uint8_t* first_file, uint8_t* import) {
    // remove the filename from the first_file path
    char tmpbuf_dir[500]; snprintf(tmpbuf_dir, 500, "%s", first_file);
    size_t last_slash = 0;
    size_t idx = 0;
    while (tmpbuf_dir[idx] != '\0') {
        if (tmpbuf_dir[idx] == '/') last_slash = idx;
        idx += 1;
    }
    tmpbuf_dir[last_slash] = '\0';

    char tmpbuf[3500]; snprintf(tmpbuf, 3500, "%.1000s/%.1000s/%.1000s", comp_dir, tmpbuf_dir, import);

    return read_file(tmpbuf);
}

int main(int argc, char** argv) {
    CmdOptions args = parse_cmdline_args(argc, argv);
    if (args.invalid) { return 1; }

    if (strcmp(args.filename, args.output_filename) == 0) {
        printf("outfile is the same as input file. exiting\n");
        return 1;
    }

    Buffer file = read_file(args.filename);
    if (!file.size || !file.data) { return 2; }

    uint8_t* str_table = (uint8_t*) malloc(0x1000);

    size_t files = 0;

    uint8_t* str_ptr = str_table;
    List tokens = tokenize(file, &str_ptr, files, args.filename);
    if (args.print_debug) {
        printf("Tokens:\n");
        for (size_t i = 0; i < tokens.length; i++) {
            printf("[%lu] ", i);
            print_token(at_index(&tokens, i, Token));
        }
    }

    ParseInfo info = parse_file(tokens);

    append(&info.dbg_info.filenames, args.filename);
    char compile_dir_buf[1000];
    info.dbg_info.compile_dir = getcwd(compile_dir_buf, 1000);

    char _tmpbuf[0x1000]; char* tmpbuf = _tmpbuf;
    while (info.imports.length > 0) {
        // @leak: this code here leaks the previous tokens, file, and
        // everything it uses, basically
        Import import = pop_back(&info.imports, Import);
        file = read_file_relative(info.dbg_info.compile_dir, args.filename, import.filename);
        debug_assert(file.size && file.data, "couldn't open '%s' from import @ %s:%lu:%lu\n",
            import.filename, expand_src(import));
        files += 1;
        append(&info.dbg_info.filenames, tmpbuf);
        tmpbuf += snprintf(tmpbuf, 500, "tests/%s", import.filename); // @hack: sry, am tired
        tokens = tokenize(file, &str_ptr, files, import.filename);
        if (args.print_debug) {
            printf("Tokens:\n");
            for (size_t i = 0; i < tokens.length; i++) {
                printf("[%lu] ", i);
                print_token(at_index(&tokens, i, Token));
            }
        }
        ParseInfo new_info = parse_file(tokens);
        merge_infos(&info, new_info);
    }

    do_type_checking(&info);

    if (args.print_debug) { print_parse_info(info, stdout); }

    if (args.dump_tree_dot) {
        dump_tree_as_dot(info, "tree_dump.dot");
        return 0;
    }

    if (args.print_tree) {
        print_tree(info);
        return 0;
    }

    IrInfo ir_info = emit_ir(&info);
    if (args.print_ir) print_ir_info(ir_info);
    write_executable(&info, args, &ir_info);
    if (args.print_ir) print_ir_info(ir_info);

    free(file.data);
    free(str_table);

    return 0;
}
