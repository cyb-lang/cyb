#include "cmd_options.h"

#include <string.h>

void print_usage(char *argv0) {
    printf("usage: %s <filename> [-o <outfile_filename>] [flags]\n", argv0);
    printf("-h, --help       print this message\n");
    printf("-d               print raw ParseInfo struct\n");
    printf("--tree           print ParseInfo in tree form\n");
    printf("--print-ir       print CybIR\n");
    printf("--dump-tree-dot  export AST as GraphViz to 'tree_dump.dot'\n");
}

CmdOptions parse_cmdline_args(int _argc, char** argv) {
    size_t argc = (size_t) _argc;

    // set default options
    CmdOptions options = {0};
    options.link_libs = create_list(uint8_t*);
    options.output_filename = "executable";

    if (argc < 2) { goto parse_error; }

    for(size_t i = 1; i < argc; i++) {
        char* arg = argv[i];
        if (strcmp(arg, "-h") == 0 || strcmp(arg, "--help") == 0) {
            goto parse_error;
        }
        else if (strcmp(arg, "-o") == 0) {
            if(argc <= i + 1) { goto parse_error; }
            options.output_filename = argv[i + 1];
            i++;
        }
        else if (strcmp(arg, "-l") == 0) {
            if(argc <= i + 1) { goto parse_error; }
            uint8_t* lib_name = (uint8_t*) argv[i + 1];
            append(&options.link_libs, lib_name);
            i++;
        }
        else if (strcmp(arg, "-d") == 0) {
            options.print_debug = true;
        }
        else if (strcmp(arg, "--tree") == 0) {
            options.print_tree = true;
        }
        else if (strcmp(arg, "--print-ir") == 0) {
            options.print_ir = true;
        }
        else if (strcmp(arg, "--dump-tree-dot") == 0) {
            options.dump_tree_dot = true;
        }
        else if (arg[0] == '-' && arg[1] == '-') {
            printf("option '%s' not recognized\n", arg);
            goto parse_error;
        }
        else {
            options.filename = argv[i];
        }
    }

    if (options.filename == NULL) goto parse_error;

    return options;

parse_error:
    print_usage(argv[0]);
    options.invalid = true;
    return options;
}

