#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <execinfo.h>
#include <elfutils/libdwfl.h>
#include <unistd.h>

void print_backtrace(FILE* file) {
    // get return address for stack trace
    #define addr_buf_size 100
    void* addr_buf[addr_buf_size];
    int32_t addrs_got = backtrace(addr_buf, addr_buf_size);
    char** symbols = backtrace_symbols(addr_buf, addrs_got);

    // backtrace_symbols(3) can only has the function name and a hex offset it.
    // instead we use libdwfl which gives us line, source file information,
    // and the function name as well

    // init libdwfl session
    char* debuginfo_path = NULL;
    Dwfl_Callbacks callbacks = {
        .find_elf = dwfl_linux_proc_find_elf,
        .find_debuginfo = dwfl_standard_find_debuginfo,
        .debuginfo_path = &debuginfo_path,
    };
    Dwfl* dwfl = dwfl_begin(&callbacks);
    dwfl_linux_proc_report(dwfl, getpid());
    dwfl_report_end(dwfl, NULL, NULL);

    // the first function in the backtrace is this one we're in
    // right now, so start printing after that first symbol
    for (uint32_t i = 1; i < (uint32_t) addrs_got; i++) {
        uint64_t addr = (uint64_t) addr_buf[i];

        Dwfl_Module* module = dwfl_addrmodule(dwfl, addr);
        // function name
        const char* fn_name = dwfl_module_addrname(module, addr); // NULL if unavailable
        // source filename and line number
        int line = -1, column = -1;
        const char* filename = NULL;
        Dwfl_Line* module_line = dwfl_module_getsrc(module, addr);
        if (line) {
            filename = dwfl_lineinfo(module_line, &addr, &line, &column, NULL, NULL); 
        }

        fprintf(file, "called from: ");
        if (filename && line != -1 && column != -1) {
            fprintf(file, "%s @ %s:%d:%d\n", fn_name, filename, line, column);
        }
        else {
            fprintf(file, "%s\n", fn_name);
        }
    }

    #undef addr_buf_size
    free(symbols);
    dwfl_end(dwfl);
}

size_t align_to_next(size_t pos, size_t alignment) {
    if (pos % alignment == 0) return pos;
    else return pos + alignment - (pos % alignment);
}

Buffer new_buffer(size_t size) {
    Buffer buffer = {0};
    buffer.data = calloc(1, size);
    if(!buffer.data) { debug_print("malloc error\n") }
    else { buffer.size = size; }
    return buffer;
}

void free_buffer(Buffer* buf) {
    free(buf->data);
    buf->data = NULL;
    buf->size = 0;
    buf->pos = 0;
}

Buffer read_file(char* filepath) {
    Buffer buffer = {0};
    FILE* f = fopen(filepath, "r");
    if(!f) {
        debug_print("error opening file '%s'\n", filepath)
        return buffer;
    }

    fseek(f, 0, SEEK_END);
    buffer.size = ftell(f);
    buffer.data = malloc(buffer.size);
    if(!buffer.data) {
        debug_print("out of memory (file '%s')\n", filepath)
        return buffer;
    }

    fseek(f, 0, SEEK_SET);
    debug_assert(fread(buffer.data, 1, buffer.size, f) == buffer.size,
        "read less bytes from file '%s' than supposted to\n", filepath);

    fclose(f);
    return buffer;
}

void buffer_write(Buffer* buf, void* src, size_t n_bytes) {
    memcpy(buf->data + buf->pos, src, n_bytes);
    buf->pos += n_bytes;
}

void buffer_write_at(Buffer* buf, size_t pos, void* src, size_t n_bytes) {
    memcpy(buf->data + pos, src, n_bytes);
}


void buffer_insert_gap(Buffer* buf, size_t gap_start, size_t gap_len) {
    memmove(
        buf->data + gap_start + gap_len,
        buf->data + gap_start,
        buf->pos - gap_start
    );
    memset(buf->data + gap_start, 0, gap_len);
    buf->pos += gap_len;
}

void write_str(Buffer* buf, char* str) {
    size_t bytes_left = buf->size - buf->pos;
    size_t str_len = strlen(str);
    debug_assert(str_len + 1 <= bytes_left,
        "str_len + '\\0'=%lu vs bytes_left=%lu\n", str_len + 1, bytes_left);
    strncpy(buf->data + buf->pos, str, str_len);
    buf->pos += str_len + 1;
}

void write_u8(Buffer* buf, uint8_t val) {
    buf->data[buf->pos++] = val;
}

void write_u16(Buffer* buf, uint16_t val) {
    *((uint16_t*) (buf->data + buf->pos)) = val; buf->pos += 2;
}

void write_u32(Buffer* buf, uint32_t val) {
    *((uint32_t*) (buf->data + buf->pos)) = val; buf->pos += 4;
}

void write_u64(Buffer* buf, uint64_t val) {
    *((uint64_t*) (buf->data + buf->pos)) = val; buf->pos += 8;
}

void write_i32_at(Buffer* buf, size_t pos, int32_t val) {
    *((int32_t*) (buf->data + pos)) = val;
}

void write_u64_at(Buffer* buf, size_t pos, uint64_t val) {
    *((uint64_t*) (buf->data + pos)) = val;
}

void print_colored(int color, const char *str) {
    //printf("\e[3%cm%s\e[0m", '0' + color, str);
    printf("\x1b[3%cm%s\x1b[0m", '0' + color, str);
}

char* bool_str(bool b) {
    return b ? "true":"false";
}

size_t str_table_add(Buffer* str_table, char* str) {
    size_t str_offset = str_table->pos;
    write_str(str_table, str);
    str_table->data[str_table->pos] = '\0'; // there must be a \0 at the end of the table
    return str_offset;
}
