#pragma once

#include "common.h"
#include "list.h"

#include <stdbool.h>

extern const char* token_type2str[74];
// @note: when adding to this enum don't forget to update token_type2str string list
typedef enum {
    // note: the part of the tokenizer that checks which keyword it should use for
    // a token relies on the order of these enums and on goto being the first
    Token_goto,
    Token_if,
    Token_else,
    Token_for,
    Token_while,
    Token_do,
    Token_break,
    Token_switch,
    Token_fall,
    Token_struct,
    Token_enum,
    Token_inherits,
    Token_defer,
    Token_return,
    Token_self,
    Token_const,
    Token_public,
    Token_static,
    Token_as,
    Token_extern,
    Token_nonamespace,
    Token_fnptr,
    Token_true,
    Token_false,
    // same for compiler directives
    Token_import,
    Token_macro,
    Token_alias,
    Token_comprun,
    Token_comp_if,
    Token_comp_else,
    Token_comp_elif,

    Token_identifier,

    Token_open_parentheses,
    Token_close_parentheses,
    Token_start_scope,
    Token_end_scope,
    Token_open_brackets,
    Token_close_brackets,
    Token_colon,
    Token_semicolon,
    Token_comma,
    Token_dot,
    Token_plus,
    Token_plus_equal,
    Token_increment,
    Token_decrement,
    Token_minus,
    Token_minus_equal,
    Token_times,
    Token_times_equal,
    Token_div,
    Token_div_equal,
    Token_mod,
    Token_mod_equal,
    Token_reference,
    Token_address,
    Token_smaller,
    Token_smaller_equal,
    Token_greater,
    Token_greater_equal,
    Token_equal,
    Token_is_equal,
    Token_different,
    Token_logical_and,
    Token_logical_or,
    Token_logical_not,
    Token_bitwise_and,
    Token_bitwise_or,
    Token_bitwise_not,
    Token_bitwise_xor,

    Token_integer,
    Token_float,
    Token_string,
    Token_char,
} TokenType;

typedef struct {
    size_t file, line, column;
    uint8_t* filename;
} SourceLocation;

// usefull for constantly printing line/column information
#define expand_src(thing) thing.src_loc.filename, thing.src_loc.line, thing.src_loc.column

bool src_loc_eql(SourceLocation a, SourceLocation b);

typedef struct {
    TokenType type;
    uint8_t* name; // for identifiers

    // for error messages
    SourceLocation src_loc;
} Token;

bool is_compound_assign(Token token);
bool is_assign_operator(Token token);
TokenType simple_op_from_compound(Token token);
bool is_left_unary_operator(Token token);
bool is_right_unary_operator(Token token);
bool is_binary_operator(Token token);
bool is_comparison_operator(Token token);

List tokenize(Buffer file, uint8_t** str_table, size_t file_n, uint8_t* filename);
void print_token(Token token);
