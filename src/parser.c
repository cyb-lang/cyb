// TODO: write a assert_end_of_line function that warns against
// writing statements on the same line without using a semicolon

// TODO: maybe change all the parse function to do the restore & Stmt_None instead of failing
// (might help with better error messages, and it would be more consistent)

// TODO: maybe rewrite parse_type to use parse_literal?

#include "common.h"
#include "parser.h"

#include <errno.h>

#define assert_token(token, token_type) { \
    debug_assert(token.type == token_type, "wrong token at %s:%lu:%lu. expected %s found %s\n", \
        expand_src(token), token_type2str[token_type], token_type2str[token.type]); \
}

Token consume_token(List* tokens) {
    Token tk = at_index(tokens, tokens->pos, Token);
    tokens->pos += 1;
    //debug_print("consumed a %s\n", token_type2str[tk.type]);
    return tk;
}

Token peek_token(List* tokens, size_t off) {
    return at_index(tokens, tokens->pos + off, Token);
}

// usefull if we need to rewind parsing for some special situations
typedef struct {
    size_t tokens_pos;
    size_t info_stmts_len, info_variables_len;
} ParseState;
ParseState save_parse_state(ParseInfo* info, List* tokens) {
    return (ParseState) {
        .tokens_pos = tokens->pos,
        .info_stmts_len = info->stmts.length,
        .info_variables_len = info->variables.length,
    };
}
void restore_parse_state(ParseState state, ParseInfo* info, List* tokens) {
    // TODO: go through the new things in info (new stmts, mostly) to look
    // for things that need to be dealloc'd (like the stmt.list)
    tokens->pos = state.tokens_pos;
    info->stmts.length = state.info_stmts_len;
    info->variables.length = state.info_variables_len;
}

#define error_out(saved, info, tokens) { \
    restore_parse_state(saved, info, tokens); \
    /*debug_print("bailing out (tokens->pos = %lu)\n", tokens->pos); \ */\
    return (Stmt) { .type = Stmt_None }; \
}

// some functions below can call each other, so we need to forward declare some stuff
Stmt parse_literal(ParseInfo* info, List* tokens);
Stmt parse_expression(ParseInfo* info, List* tokens);
Stmt parse_statement(ParseInfo* info, List* tokens);
List parse_block(ParseInfo* info, List* tokens);
Stmt parse_var_decl(ParseInfo* info, List* tokens);

// StringLit <- "\"" string_char* "\"" skip
Variable parse_string(List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_string);

    return (Variable) {
        .type_info = { .type = Type_String, .n_bits = strlen(token.name) * 8 },
        .name = token.name,
        .src_loc = token.src_loc,
    };
}

// CharLit <- "'" (char_escape / [A-Za-z0-9]) "'" skip
Variable parse_char(List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_char);

    return (Variable) {
        .type_info = { .type = Type_Integer, .n_bits = 8 },
        .name = token.name,
        .src_loc = token.src_loc,
        .is_value = true, .value = token.name[0],
    };
}

// IntegerLit <- MINUS? (bin_int / oct_int / dec_int / hex_int) skip
Variable parse_integer(List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_integer);

    char* parsing = token.name;
    size_t slen = strlen((char*) parsing);

    bool is_signed = false;
    if (slen > 1 && parsing[0] == '-') {
        is_signed = true;
        parsing++; slen--;
    }

    int32_t base = 10;
    if (slen > 1 && parsing[0] == '0') {
        if (parsing[1] == 'b') { base = 2; parsing += 2; slen -= 2; }
        else if (parsing[1] == 'o') { base = 8; parsing += 2; slen -= 2; }
        else if (parsing[1] == 'x') { base = 16; parsing += 2; slen -= 2; }
    }

    errno = 0;
    char* end_ptr = NULL;
    uint64_t integer = strtoul(parsing, &end_ptr, base);
    debug_assert(errno != EINVAL, "couldn't convert integer literal (base=%d)\n", base);
    debug_assert(errno != ERANGE, "integer literal too big\n");

    // choose the smallest type that can fit this constant
    uint16_t bits = 64;
    if (is_signed) {
        if (integer <= 0x100) bits = 8;
        else if (integer <= 0x10000) bits = 16;
        else if (integer <= 0x100000000) bits = 32;
    }
    else {
        if (integer < 0x100) bits = 8;
        else if (integer < 0x10000) bits = 16;
        else if (integer < 0x100000000) bits = 32;
    }

    return (Variable) {
        .type_info = {
            .type = Type_Integer,
            .is_signed = is_signed, .n_bits = bits,
            .is_comptime_const = true,
            .comptime_value = is_signed ? -integer : integer,
        },
        .name = token.name,
        .src_loc = token.src_loc,
        .is_value = true, .value = is_signed ? -integer : integer,
    };
}

// BooleanLit <- (KEYWORD_true / KEYWORD_false) skip
Variable parse_boolean(List* tokens) {
    Token token = consume_token(tokens);

    return (Variable) {
        .type_info = { .type = Type_Bool, .n_bits = 8 },
        .name = token.name,
        .src_loc = token.src_loc,
        .is_value = true, .value = (token.type == Token_true),
    };
}

// ArrayLit <- LBRACKET (Expr (COMMA Literal)*)? RBRACKET
Stmt parse_array(ParseInfo* info, List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_open_brackets);

    Stmt stmt = { .type = Stmt_ArrayInit, .src_loc = token.src_loc };
    stmt.list = create_list(size_t);

    Token next = peek_token(tokens, 0);
    while (next.type != Token_close_brackets) {
        Stmt elem = parse_expression(info, tokens);
        append(&stmt.list, info->stmts.length);
        append(&info->stmts, elem);

        next = peek_token(tokens, 0);
        if (next.type == Token_comma) {
            consume_token(tokens);
            next = peek_token(tokens, 0);
        }
    }
    consume_token(tokens);

    return stmt;
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// Literal <- StringLit / CharLit / IntegerLit / FloatLit / BooleanLit / ArrayLit / StructLit
Stmt parse_literal(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Variable var = {0};

    Token next = peek_token(tokens, 0);

    if (next.type == Token_string) {
        var = parse_string(tokens);
    }
    else if (next.type == Token_char) {
        var = parse_char(tokens);
    }
    else if (next.type == Token_integer) {
        var = parse_integer(tokens);
    }
    else if (next.type == Token_float) {
        debug_crash("TODO\n");
    }
    else if (next.type == Token_true || next.type == Token_false) {
        var = parse_boolean(tokens);
    }
    else if (next.type == Token_open_brackets) {
        return parse_array(info, tokens);
    }
    else if (next.type == Token_struct) {
        debug_crash("TODO\n");
    }
    else {
        error_out(saved, info, tokens);
    }

    Stmt stmt = {
        .type = Stmt_Variable,
        .variable = info->variables.length,
        .src_loc = next.src_loc,
    };
    append(&info->variables, var);
    return stmt;
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// Var
//     <- VarName
//      / VarName DOT VarName
//      / Literal
Stmt parse_variable(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt literal = parse_literal(info, tokens);
    if (literal.type != Stmt_None) return literal;

    Token token = consume_token(tokens);
    Token next = peek_token(tokens, 0);

    Stmt stmt = { .type = Stmt_Variable, .src_loc = token.src_loc };

    if (token.type == Token_identifier) {
        Variable var = {0};
        var.name = token.name;
        var.src_loc = token.src_loc;

        stmt.variable = info->variables.length;
        append(&info->variables, var);

        if (next.type == Token_dot) {
            stmt.type = Stmt_DotOp;

            token = consume_token(tokens); // dot token

            token = consume_token(tokens);
            assert_token(token, Token_identifier);

            stmt.name = token.name;
        }

        return stmt;
    }

    error_out(saved, info, tokens);
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// FnCall <- Identifier LPAREN (Expr (COMMA Expr)*)? RPAREN
Stmt parse_fn_call(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Token token = consume_token(tokens);

    Stmt stmt = { .type = Stmt_FunctionCall, .src_loc = token.src_loc };
    stmt.list = create_list(size_t);

    if (token.type != Token_identifier) error_out(saved, info, tokens);
    stmt.name = token.name;

    token = consume_token(tokens);
    if (token.type != Token_open_parentheses) error_out(saved, info, tokens);

    Token next = peek_token(tokens, 0);
    if (next.type != Token_close_parentheses) {
        Stmt expr = parse_expression(info, tokens);
        append(&stmt.list, info->stmts.length);
        append(&info->stmts, expr);

        next = peek_token(tokens, 0);
        while (next.type != Token_close_parentheses) {
            token = consume_token(tokens);
            assert_token(token, Token_comma);

            expr = parse_expression(info, tokens);
            append(&stmt.list, info->stmts.length);
            append(&info->stmts, expr);


            next = peek_token(tokens, 0);
        }
    }

    token = consume_token(tokens);
    assert_token(token, Token_close_parentheses);

    return stmt;
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// Indexing <- Var LBRACKET Var RBRACKET
Stmt parse_indexing(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Token token = peek_token(tokens, 0);
    Stmt stmt = { .type = Stmt_Indexing, .src_loc = token.src_loc };

    Stmt array_var = parse_variable(info, tokens);
    if (array_var.type == Stmt_None) error_out(saved, info, tokens);

    token = consume_token(tokens);
    if (token.type != Token_open_brackets) error_out(saved, info, tokens);

    Stmt index_var = parse_variable(info, tokens);
    if (index_var.type == Stmt_None) error_out(saved, info, tokens);

    token = consume_token(tokens);
    if (token.type != Token_close_brackets) error_out(saved, info, tokens);

    stmt.indexed_stmt = info->stmts.length;
    append(&info->stmts, array_var);
    stmt.index_stmt = info->stmts.length;
    append(&info->stmts, index_var);

    return stmt;
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// EnclosedExpr <- LPAREN Expr RPAREN
Stmt parse_enclosed_expression(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Token token = consume_token(tokens);
    if (token.type != Token_open_parentheses) error_out(saved, info, tokens);

    Stmt expr = parse_expression(info, tokens);
    expr.src_loc = token.src_loc;
    if (expr.type == Stmt_None) error_out(saved, info, tokens);

    token = consume_token(tokens);
    if (token.type != Token_close_parentheses) error_out(saved, info, tokens);

    return expr;
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// InnerOpExpr <- EnclosedExpr / Indexing / Var
Stmt parse_inner_op_expression(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt expr = parse_enclosed_expression(info, tokens);
    if (expr.type != Stmt_None) return expr;

    Stmt index = parse_indexing(info, tokens);
    if (index.type != Stmt_None) return index;

    Stmt var = parse_variable(info, tokens);
    if (var.type != Stmt_None) return var;

    error_out(saved, info, tokens);
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// UnaryOp <- (LeftUnaryOperator Expr) / (Var RightUnaryOperator)
Stmt parse_unary_op(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Token next = peek_token(tokens, 0);

    if (is_left_unary_operator(next)) {
        Token token = consume_token(tokens);
        Stmt unary_stmt = { .type = Stmt_UnaryOp, .op_token = token.type };
        unary_stmt.src_loc = token.src_loc;
        Stmt operand = parse_inner_op_expression(info, tokens);
        unary_stmt.op_left = info->stmts.length;
        append(&info->stmts, operand);
        return unary_stmt;
    }
    else {
        Stmt unary_stmt = { .type = Stmt_UnaryOp };
        Stmt operand = parse_inner_op_expression(info, tokens);
        unary_stmt.src_loc = operand.src_loc;

        Token token = consume_token(tokens);
        if (is_right_unary_operator(token)) {
            unary_stmt.op_token = token.type; 
            unary_stmt.op_left = info->stmts.length;
            append(&info->stmts, operand);
        }
        else {
            // couldn't parse unary expression; restore state and error out
            // note: this might leak memory, needs further investigation (check restore function)
            error_out(saved, info, tokens);
        }

        return unary_stmt;
    }
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// InnerBinOpExpr <- UnaryOp / InnerOpExpr
Stmt parse_inner_bin_op_expression(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt unary_op = parse_unary_op(info, tokens);
    if (unary_op.type != Stmt_None) return unary_op;

    Stmt expr = parse_inner_op_expression(info, tokens);
    if (expr.type != Stmt_None) return expr;

    error_out(saved, info, tokens);
}

// returns Stmt_None instead of failing. see parse_expression for explanation
// BinaryOp <- InnerOpExpr BinaryOperator InnerOpExpr
Stmt parse_binary_op(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt left = parse_inner_bin_op_expression(info, tokens);

    Token token = consume_token(tokens);
    if (!is_binary_operator(token)) error_out(saved, info, tokens);

    Stmt right = parse_inner_bin_op_expression(info, tokens);

    Stmt bin_op = {
        .type = Stmt_BinaryOp, .op_token = token.type,
        .op_left = info->stmts.length,
        .op_right = info->stmts.length + 1,
        .src_loc = token.src_loc,
    };
    append(&info->stmts, left);
    append(&info->stmts, right);

    return bin_op;
}

// Expr
//     <- UnaryOp
//      / BinaryOp
//      / FnCall
//      / Indexing
//      / Var
Stmt parse_expression(ParseInfo* info, List* tokens) {
    //ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Token next = peek_token(tokens, 0);

    Token next2 = peek_token(tokens, 1);
    if (next.type == Token_identifier && next2.type == Token_open_parentheses) {
        return parse_fn_call(info, tokens);
    }

    // at the moment I can't figure out how to decide which path the parser
    // should go from this function, because stuff like UnaryOp could include
    // a whole other expression inside it, so here's what I'm going to do instead:
    // - parse_unary_op, parse_binary_op, parse_fn_call, parse_var are going to
    // try and parse their respective parts of the tree and, if they can't, 
    // return a Stmt_None and rollback any tokens they consumed.
    // then here we just have to try all of them to see if one sticks
    // (p.s. this can cause us to have to reparse an expression which, in theory,
    // could be big, but not big enough to matter at least in any reasonable program,
    // written by humans)
    Stmt stmt = { .type = Stmt_None };

    // the order in which we call these is important. variables must be last because
    // it match the left side of a binary op, and then we wouldn't parse the binary op

    stmt = parse_binary_op(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    stmt = parse_unary_op(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    stmt = parse_indexing(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    stmt = parse_variable(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    debug_assert(stmt.type != Stmt_None,
        "couldn't parse expression at %s:%lu:%lu\n", expand_src(next));

    return stmt;
}

// helper functions for if/else, for, while statement parsing functions
List parse_stmt_or_block(ParseInfo* info, List* tokens) {
    List list = create_list(size_t);

    Token next = peek_token(tokens, 0);
    if (next.type == Token_start_scope) {
        list = parse_block(info, tokens); 
    }
    else {
        Stmt stmt = parse_statement(info, tokens);
        append(&list, info->stmts.length);
        append(&info->stmts, stmt);
    }

    return list;
}
Stmt parse_expr_in_parentheses(ParseInfo* info, List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_open_parentheses);

    Stmt expr = parse_expression(info, tokens);
    expr.src_loc = token.src_loc;

    token = consume_token(tokens);
    assert_token(token, Token_close_parentheses);

    return expr;
}

// WhileStatement
//     <- KEYWORD_while LPAREN Expr RPAREN StmtBlock
Stmt parse_while_statement(ParseInfo* info, List* tokens) {
    Stmt stmt = { .type = Stmt_While };

    Token token = consume_token(tokens);
    assert_token(token, Token_while);
    stmt.src_loc = token.src_loc;

    // the conditional
    Stmt cond_stmt = parse_expr_in_parentheses(info, tokens);
    stmt.cond_stmt = info->stmts.length;
    append(&info->stmts, cond_stmt);

    stmt.list = parse_stmt_or_block(info, tokens);

    return stmt;
}

// ForStatement
//     <- KEYWORD_for LPAREN VarDecl SEMICOLON Expr SEMICOLON Expr RPAREN StmtBlock
Stmt parse_for_statement(ParseInfo* info, List* tokens) {
    Stmt stmt = { .type = Stmt_For };

    Token token = consume_token(tokens);
    assert_token(token, Token_for);
    stmt.src_loc = token.src_loc;

    token = consume_token(tokens);
    assert_token(token, Token_open_parentheses);

    Stmt init_stmt = parse_var_decl(info, tokens);
    stmt.init_stmt = info->stmts.length;
    append(&info->stmts, init_stmt);

    token = consume_token(tokens);
    assert_token(token, Token_semicolon);

    Stmt cond_stmt = parse_expression(info, tokens);
    stmt.cond_stmt = info->stmts.length;
    append(&info->stmts, cond_stmt);

    token = consume_token(tokens);
    assert_token(token, Token_semicolon);

    Stmt step_stmt = parse_expression(info, tokens);
    stmt.step_stmt = info->stmts.length;
    append(&info->stmts, step_stmt);

    token = consume_token(tokens);
    assert_token(token, Token_close_parentheses);

    stmt.list = parse_stmt_or_block(info, tokens);

    return stmt;
}

// IfStatement
//     <- KEYWORD_if LPAREN Expr RPAREN StmtBlock (KEYWORD_else (StmtBlock / IfStatement))?
Stmt parse_if_statement(ParseInfo* info, List* tokens) {
    Stmt stmt = { .type = Stmt_If };

    Token token = consume_token(tokens);
    assert_token(token, Token_if);
    stmt.src_loc = token.src_loc;

    // the conditional
    Stmt cond_stmt = parse_expr_in_parentheses(info, tokens);
    stmt.cond_stmt = info->stmts.length;
    append(&info->stmts, cond_stmt);

    // the block
    stmt.list = parse_stmt_or_block(info, tokens);

    // the else clause
    Token next = peek_token(tokens, 0);
    if (next.type == Token_else) {
        consume_token(tokens);
        next = peek_token(tokens, 0);

        Stmt else_stmt = { .type = Stmt_Else };

        if (next.type == Token_if) {
            else_stmt = parse_if_statement(info, tokens);
        }
        else {
            else_stmt.list = parse_stmt_or_block(info, tokens);
        }

        stmt.else_stmt = info->stmts.length;
        append(&info->stmts, else_stmt);
    }

    return stmt;
}

// VarAssign <- Expr AssignOperator Expr
Stmt parse_var_assign(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt stmt = { .type = Stmt_Assignment };
    stmt.src_loc = peek_token(tokens, 0).src_loc;

    Stmt dst = parse_expression(info, tokens);
    if (dst.type == Stmt_None) error_out(saved, info, tokens);

    Token token = consume_token(tokens);
    if (!is_assign_operator(token)) error_out(saved, info, tokens);

    Stmt src = parse_expression(info, tokens);
    if (src.type == Stmt_None) error_out(saved, info, tokens);

    if (is_compound_assign(token)) {
        // for compound assignments (+=, |=, etc) we replace the right expr with
        // a binary op where the left operand is the left side of the assignment
        Stmt bin_op = { .type = Stmt_BinaryOp };
        bin_op.op_left = info->stmts.length;
        append(&info->stmts, dst);
        bin_op.op_right = info->stmts.length;
        append(&info->stmts, src);
        bin_op.op_token = simple_op_from_compound(token);

        src = bin_op;
    }

    stmt.op_left = info->stmts.length;
    append(&info->stmts, dst);

    stmt.op_right = info->stmts.length;
    append(&info->stmts, src);

    return stmt;
}

// TypeName <- AMPERSAND* Identifier (LBRACKET IntegerLit RBRACKET)?
TypeInfo parse_type(ParseInfo* info, List* tokens) {
    TypeInfo type = {0};

    Token token = consume_token(tokens);
    while (token.type == Token_reference) {
        type.ptr_level++;
        token = consume_token(tokens);
    }

    if (token.name && strcmp(token.name, "bool") == 0) {
        return (TypeInfo) { .type = Type_Bool, .name = "bool", .n_bits = 8 };
    }

    assert_token(token, Token_identifier);
    type.name = token.name;

    errno = 0;
    char* end_ptr = NULL;
    type.n_bits = strtoul(type.name + 1, &end_ptr, 10);
    debug_assert(errno != ERANGE, "type '%s' too big @ %s:%lu:%lu\n", type.name, expand_src(token));
    if (errno != EINVAL && end_ptr != (char*) (type.name + 1)) {
        type.type = Type_Integer;
        debug_assert(type.n_bits % 8 == 0,
            "only supporting type sizes that are an integer number of bytes\n");
        debug_assert(type.name[0] != 'f', "floating point types not supported yet\n");
        type.is_signed = (type.name[0] == 'i');
    }
    else {
        type.type = Type_Struct;
    }


    Token next = peek_token(tokens, 0);
    if (next.type == Token_open_brackets) {
        token = consume_token(tokens);
        type.is_array = true;

        token = consume_token(tokens);
        if (token.type == Token_integer) {
            // @note: we may need to check errno here to make sure it's not valid
            // I think the string '0xzzz' would make s_end point to the first z
            // but errno would still be set to EINVAL
            char* s_end = NULL;
            uint64_t size = strtoul(token.name, &s_end, 0);
            debug_assert(size > 0, "invalid size for static array\n");
            debug_assert((uint8_t*) s_end != token.name, "invalid number\n");
            type.array_size = size;

            token = consume_token(tokens);
        }

        assert_token(token, Token_close_brackets);
    }

    return type;
}

// VarDecl <- TypeName VarName (EQUAL Expr)?
Stmt parse_var_decl(ParseInfo* info, List* tokens) {
    ParseState saved = save_parse_state(info, tokens); // used to rollback if we error out

    Stmt stmt = { .type = Stmt_Decl };
    Variable var = {0};

    var.type_info = parse_type(info, tokens);

    Token token = consume_token(tokens);
    if (token.type != Token_identifier) error_out(saved, info, tokens);
    var.name = token.name;
    var.src_loc = token.src_loc;
    stmt.src_loc = token.src_loc;

    stmt.variable = info->variables.length;
    append(&info->variables, var);

    Token next = peek_token(tokens, 0);
    if (next.type == Token_equal) {
        token = consume_token(tokens);

        Stmt expr = parse_expression(info, tokens);
        if (expr.type == Stmt_None) error_out(saved, info, tokens);

        stmt.assign_stmt = info->stmts.length;
        append(&info->stmts, expr);
    }

    return stmt;
}

// Statement
//     <- VarDecl
//      / VarAssign
//      / IfStatement
//      / ForStatement
//      / WhileStatement
//      / KEYWORD_return Expr?
//      / KEYWORD_break
//      / Expr
Stmt parse_statement(ParseInfo* info, List* tokens) {
    Token token = peek_token(tokens, 0);
    Token next = peek_token(tokens, 1);

    if (token.type == Token_if) {
        return parse_if_statement(info, tokens);
    }
    else if (token.type == Token_for) {
        return parse_for_statement(info, tokens);
    }
    else if (token.type == Token_while) {
        return parse_while_statement(info, tokens);
    }
    else if (token.type == Token_return) {
        Stmt stmt = { .type = Stmt_Return, .src_loc = token.src_loc };
        consume_token(tokens); // consume 'return' keyword
        if (next.src_loc.line == token.src_loc.line && next.type != Token_end_scope) {
            Stmt return_stmt = parse_expression(info, tokens);
            stmt.return_stmt = info->stmts.length;
            append(&info->stmts, return_stmt);
        }
        return stmt;
    }
    else if (token.type == Token_defer) {
        // TODO
        debug_crash("'defer' not implemented yet\n");
    }
    else if (token.type == Token_break) {
        consume_token(tokens); // consume 'break' keyword
        Stmt break_stmt = { .type = Stmt_Break, .src_loc = token.src_loc };
        return break_stmt;
    }

    // now all the possible statements that start with an identifier
    Stmt stmt = { .type = Stmt_None };

    stmt = parse_var_assign(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    stmt = parse_var_decl(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    stmt = parse_expression(info, tokens);
    if (stmt.type != Stmt_None) return stmt;

    debug_crash("invalid statement at %s:%lu:%lu\n", expand_src(token));
}

// Block <- LBRACE Statement* RBRACE
List parse_block(ParseInfo* info, List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_start_scope);

    List block = create_list(size_t);

    Token next = peek_token(tokens, 0);
    while (next.type != Token_end_scope) {
        Stmt stmt = parse_statement(info, tokens);
        append(&block, info->stmts.length);
        append(&info->stmts, stmt);

        next = peek_token(tokens, 0);
    }
    consume_token(tokens); // consume '}'

    return block;
}

// StructDef _FnPtrORD_struct Identifier LBRACE VarDecl* RBRACE
Stmt parse_struct_def(ParseInfo* info, List* tokens) {
    Stmt struct_stmt = { .type = Stmt_StructDef };

    StructType struct_type = {0};
    struct_type.members = create_list(StructMember);

    Token token = consume_token(tokens);
    assert_token(token, Token_struct);
    struct_type.src_loc = token.src_loc;
    struct_stmt.src_loc = token.src_loc;

    token = consume_token(tokens);
    assert_token(token, Token_identifier);
    struct_type.name = token.name;
    struct_stmt.name = token.name;

    token = consume_token(tokens);
    assert_token(token, Token_start_scope);

    Token next = peek_token(tokens, 0);
    while (next.type != Token_end_scope) {
        Stmt member_decl = parse_var_decl(info, tokens);
        Variable var = at_index(&info->variables, member_decl.variable, Variable);
        StructMember struct_member = {
            .name = var.name, .type_info = var.type_info, 
            .byte_offset = struct_type.bit_size / 8,
        };
        append(&struct_type.members, struct_member);

        uint32_t member_bits = var.type_info.n_bits;
        if (var.type_info.ptr_level > 0) member_bits = 64;
        if (var.type_info.is_array) member_bits *= var.type_info.array_size;
        struct_type.bit_size += member_bits;

        next = peek_token(tokens, 0);
    }
    consume_token(tokens); // consume '}'

    struct_stmt.struct_type_idx = info->struct_types.length;
    append(&info->struct_types, struct_type);

    return struct_stmt;
}

// FnType <- LPAREN FnArgList? RPAREN (COLON TypeName)?
// FnArgList <- TypeName VarName (COMMA TypeName VarName)*
Function parse_function_type(ParseInfo* info, List* tokens) {
    Function func = {0};
    func.args = create_list(size_t);

    // argument list
    Token token = consume_token(tokens);
    assert_token(token, Token_open_parentheses);

    Token next = peek_token(tokens, 0);
    while (next.type != Token_close_parentheses) {
        // checking for a comma here in the loop will allow something like "fn_name(, u8 var)"
        // which, while not correct, doesn't bother me that much
        if (next.type == Token_comma) token = consume_token(tokens);

        TypeInfo type = parse_type(info, tokens);

        token = consume_token(tokens);
        assert_token(token, Token_identifier);

        Variable var = {
            .type_info = type, .name = token.name, .src_loc = token.src_loc,
        };
        append(&func.args, info->variables.length);
        append(&info->variables, var);

        next = peek_token(tokens, 0);
    }
    consume_token(tokens); // consume ')'

    // return type
    next = peek_token(tokens, 0);
    if (next.type == Token_colon) {
        consume_token(tokens); // consume ':'
        func.return_type = parse_type(info, tokens);
    }

    return func;
}


// TopLevelDecl
//     <- StructDef
//      / KEYWORD_fnptr Identifier FnType
//      / TypeName VarName (EQUAL Literal)?
Stmt parse_top_level_decl(ParseInfo* info, List* tokens) {
    Token token = peek_token(tokens, 0);

    Stmt stmt = { .src_loc = token.src_loc };

    if (token.type == Token_struct) {
        stmt = parse_struct_def(info, tokens);
    }
    else if (token.type == Token_fnptr) {
        consume_token(tokens); // consume 'fnptr'

        stmt.type = Stmt_Decl;
        stmt.is_global = true;
        Variable var = { .is_global = true };

        token = consume_token(tokens);
        assert_token(token, Token_identifier);
        var.name = token.name;

        Function dummy_fn = parse_function_type(info, tokens);
        FnPtrType fnptr_type = { .return_type = dummy_fn.return_type };
        fnptr_type.arg_types = create_list(TypeInfo);
        for (size_t i = 0; i < dummy_fn.args.length; i++) {
            TypeInfo arg_type = indirect_index(&info->variables, &dummy_fn.args, i, Variable).type_info;
            append(&fnptr_type.arg_types, arg_type);
        }
        var.type_info = (TypeInfo) {
            .type = Type_FnPtr,
            .fnptr_idx = info->fnptr_types.length,
        };
        append(&info->fnptr_types, fnptr_type);

        stmt.variable = info->variables.length;
        append(&info->variables, var);

        // because we don't allow global decls without assignment lets make one
        Stmt assign_null_stmt = { .type = Stmt_Variable,
            .variable = info->variables.length,
        };
        Variable null_variable = {
            .type_info = { .type = Type_Integer,
                .n_bits = 8, .is_comptime_const = true, .comptime_value = 0,
            },
            .is_value = true, .value = 0,
        };
        append(&info->variables, null_variable);
        stmt.assign_stmt = info->stmts.length;
        append(&info->stmts, assign_null_stmt);
    }
    else {
        stmt.type = Stmt_Decl;
        stmt.is_global = true;
        Variable var = { .is_global = true };

        var.type_info = parse_type(info, tokens);

        token = consume_token(tokens);
        assert_token(token, Token_identifier);
        var.name = token.name;
        var.src_loc = token.src_loc;

        token = consume_token(tokens);
        assert_token(token, Token_equal);

        Stmt lit_stmt = parse_literal(info, tokens);
        debug_assert(lit_stmt.type == Stmt_Variable || lit_stmt.type == Stmt_ArrayInit,
            "top level decl must be simple variables, or arrays\n");

        stmt.variable = info->variables.length;
        append(&info->variables, var);

        stmt.assign_stmt = info->stmts.length;
        append(&info->stmts, lit_stmt);
    }

    return stmt;
}

// Function <- Identifier FnType Block
Function parse_function(ParseInfo* info, List* tokens) {
    Token token = consume_token(tokens);

    Function func = {0};

    assert_token(token, Token_identifier);
    func.name = token.name;
    func.src_loc = token.src_loc;

    Function fn_type = parse_function_type(info, tokens);
    func.args = fn_type.args;
    func.return_type = fn_type.return_type;

    func.start_stmts = info->stmts.length;
    func.stmts = parse_block(info, tokens);
    func.end_stmts = info->stmts.length;

    return func;
}

// ExternFn <- KEYWORD_extern Identifier FnType
Function parse_extern_function(ParseInfo* info, List* tokens) {
    Token token = consume_token(tokens);
    assert_token(token, Token_extern);
    SourceLocation start_loc = token.src_loc;

    token = consume_token(tokens);
    assert_token(token, Token_identifier);

    Function func = parse_function_type(info, tokens);
    func.name = token.name;
    func.stmts = create_list(size_t);
    func.is_extern = true;
    func.src_loc = start_loc;

    return func;
}

// Import <- KEYWORD_import StringLit 
Import parse_import(ParseInfo* info, List* tokens) {
    Import import = {0};

    Token token = consume_token(tokens);
    assert_token(token, Token_import);
    import.src_loc = token.src_loc;

    token = consume_token(tokens);
    assert_token(token, Token_string);
    import.filename = token.name;

    return import;
}

// Root <- skip (Import / TopLevelDecl / StructDef / Function)* eof
ParseInfo parse_file(List tokens) {
    ParseInfo info = {0};
    info.struct_types = create_list(StructType);
    info.fnptr_types = create_list(FnPtrType);
    info.functions = create_list(Function);
    info.stmts = create_list(Stmt);
    info.variables = create_list(Variable);
    info.top_level_decls = create_list(size_t);
    info.imports = create_list(Import);
    // debug info list that need initializing
    info.dbg_info.filenames = create_list(uint8_t*);
    info.dbg_info.line_addrs = create_list(LineAddr);
    info.dbg_info.funcs = create_list(FuncInfo);
    info.dbg_info.types = create_list(TypeInfo);
    info.dbg_info.global_vars = create_list(VarInfo);

    // index 0 of stmts must be Stmt_None
    Stmt stmt_none = { .type = Stmt_None };
    append(&info.stmts, stmt_none);

    while (tokens.pos < tokens.length) {
        Token token = peek_token(&tokens, 0);
        Token next = peek_token(&tokens, 1);

        if (token.type == Token_import) {
            Import import = parse_import(&info, &tokens);
            append(&info.imports, import);
        }
        else if (token.type == Token_extern) {
            Function func = parse_extern_function(&info, &tokens);
            append(&info.functions, func);
        }
        else if (token.type == Token_identifier && next.type == Token_open_parentheses) {
            Function func = parse_function(&info, &tokens);
            append(&info.functions, func);
        }
        else {
            Stmt stmt = parse_top_level_decl(&info, &tokens);
            append(&info.top_level_decls, info.stmts.length);
            append(&info.stmts, stmt);
        }
    }

    return info;
}

void merge_infos(ParseInfo* old_info, ParseInfo new_info) {
    // before copying the new info from the new list we have to bump up
    // all the stmts/variables that reference each other through indexes
    // into list to account for the ones already present in old_info

    size_t base_functions = old_info->functions.length;
    size_t base_stmts = old_info->stmts.length;
    size_t base_variables = old_info->variables.length;
    size_t base_struct_types = old_info->struct_types.length;

    append_list(&old_info->struct_types, &new_info.struct_types);

    for (size_t i = 0; i < new_info.functions.length; i++) {
        Function fn = at_index(&new_info.functions, i, Function);

        for (size_t s = 0; s < fn.stmts.length; s++) {
            at_index(&fn.stmts, s, size_t) += base_stmts;
        }
        for (size_t v = 0; v < fn.args.length; v++) {
            at_index(&fn.args, v, size_t) += base_variables;
        }
        fn.start_stmts += base_stmts;
        fn.end_stmts += base_stmts;

        at_index(&new_info.functions, i, Function) = fn;
    }
    append_list(&old_info->functions, &new_info.functions);

    for (size_t i = 0; i < new_info.stmts.length; i++) {
        Stmt stmt = at_index(&new_info.stmts, i, Stmt);

        for (size_t s = 0; s < stmt.list.length; s++) {
            at_index(&stmt.list, s, size_t) += base_stmts;
        }

        // because Stmt struct uses unions we only need to take care of special cases
        if (stmt.op_left == 0) ;
        else if (stmt.type == Stmt_FunctionCall) stmt.function_idx += base_functions;
        else if (stmt.type == Stmt_StructDef) stmt.struct_type_idx += base_struct_types;
        else if (stmt.type == Stmt_DotOp) ;
        else stmt.op_left += base_stmts;

        stmt.step_stmt += base_stmts;

        if (stmt.type == Stmt_Variable || stmt.type == Stmt_Decl || stmt.type == Stmt_DotOp) {
            stmt.variable += base_variables;
        }
        else {
            stmt.op_right += base_stmts;
        }

        at_index(&new_info.stmts, i, Stmt) = stmt;
    }
    append_list(&old_info->stmts, &new_info.stmts);

    for (size_t i = 0; i < new_info.variables.length; i++) {
        Variable var = at_index(&new_info.variables, i, Variable);

        var.first += base_variables;
        if (var.type_info.type == Type_Struct) var.type_info.struct_idx += base_struct_types;

        at_index(&new_info.variables, i, Variable) = var;
    }
    append_list(&old_info->variables, &new_info.variables);

    for (size_t i = 0; i < new_info.top_level_decls.length; i++) {
        at_index(&new_info.top_level_decls, i, size_t) += base_stmts;
    }
    append_list(&old_info->top_level_decls, &new_info.top_level_decls);

    append_list(&old_info->imports, &new_info.imports);

    // no need to merge the debug info as this is only filled in the write_x64
    // functions, i.e. after all the imports have been parsed and merged
}
