const char* token_type2str[] = {
    "Token_goto",
    "Token_if",
    "Token_else",
    "Token_for",
    "Token_while",
    "Token_do",
    "Token_break",
    "Token_switch",
    "Token_fall",
    "Token_struct",
    "Token_enum",
    "Token_inherits",
    "Token_defer",
    "Token_return",
    "Token_self",
    "Token_const",
    "Token_public",
    "Token_static",
    "Token_as",
    "Token_extern",
    "Token_nonamespace",
    "Token_fnptr",
    "Token_true",
    "Token_false",

    "Token_import",
    "Token_macro",
    "Token_alias",
    "Token_comprun",
    "Token_comp_if",
    "Token_comp_else",
    "Token_comp_elif",

    "Token_identifier",

    "Token_open_parentheses",
    "Token_close_parentheses",
    "Token_start_scope",
    "Token_end_scope",
    "Token_open_brackets",
    "Token_close_brackets",
    "Token_colon",
    "Token_semicolon",
    "Token_comma",
    "Token_dot",
    "Token_plus",
    "Token_plus_equal",
    "Token_increment",
    "Token_decrement",
    "Token_minus",
    "Token_minus_equal",
    "Token_times",
    "Token_times_equal",
    "Token_div",
    "Token_div_equal",
    "Token_mod",
    "Token_mod_equal",
    "Token_reference",
    "Token_address",
    "Token_smaller",
    "Token_smaller_equal",
    "Token_greater",
    "Token_greater_equal",
    "Token_equal",
    "Token_is_equal",
    "Token_different",
    "Token_logical_and",
    "Token_logical_or",
    "Token_logical_not",
    "Token_bitwise_and",
    "Token_bitwise_or",
    "Token_bitwise_not",
    "Token_bitwise_xor",

    "Token_integer",
    "Token_float",
    "Token_string",
    "Token_char",
};

const char* stmt_type2str[] = {
    "Stmt_None",
    "Stmt_FunctionCall",
    "Stmt_If",
    "Stmt_Else",
    "Stmt_While",
    "Stmt_For",
    "Stmt_Break",
    "Stmt_Return",
    "Stmt_Variable",
    "Stmt_Decl",
    "Stmt_StructDef",
    "Stmt_Assignment",
    "Stmt_ArrayInit",
    "Stmt_UnaryOp",
    "Stmt_BinaryOp",
    "Stmt_Indexing",
    "Stmt_DotOp",
};

const char* type_type2str[] = {
    "Type_None",
    "Type_Integer",
    "Type_Float",
    "Type_String",
    "Type_Bool",
    "Type_Struct",
    "Type_FnPtr",
};

const char* loc_type2str[] = {
    "Loc_None",
    "Loc_Memory",
    "Loc_Register",
    "Loc_Stack",
    "Loc_Imm",
};

const char* reg_code2str[] = {
    "rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi",
    "r8",  "r9",  "r10", "r11", "r12", "r13", "r14", "r15",
};

const char* op_mnemonic2str[] = {
    "OpAdd",
    "OpSub",
    "OpAnd",
    "OpXor",
    "OpMov",
    "OpMovEql",
    "OpMovNEql",
    "OpLea",
    "OpMul",
    "OpDiv",
    "OpInc",
    "OpDec",
    "OpNeg",
    "OpCmp",
    "OpSetOverflow",
    "OpSetNOverflow",
    "OpSetBlw",
    "OpSetCarry",
    "OpSetEql",
    "OpSetNEql",
    "OpSetBlwEql",
    "OpSetAbv",
    "OpSetSign",
    "OpSetNSign",
    "OpSetParity",
    "OpSetNParity",
    "OpSetLess",
    "OpSetNLess",
    "OpSetLessEql",
    "OpSetGreater",
    "OpPush",
    "OpPop",
    "OpJmp",
    "OpRet",
    "OpSyscall",
};

const char* operand_type2str[] = {
    "NullLoc", "Register", "Memory", "Imm",
};

const char* ir_literal_type2str[] = {
    "IrLiteralInt",
    "IrLiteralString",
};

const char* ir_obj_ref_type2str[] = {
    "IrRefData",
    "IrRefGlobal",
    "IrRefLocal",
};

const char* ir_obj_loc2str[] = {
    "IrLocNone",
    "IrLocReg",
    "IrLocStack",
    "IrLocMem",
};

const char* ir_obj_type2str[] = {
    "IrLiteral",
    "IrAddrStub",
    "IrVar",
    "IrVarRef",
    "IrIndexOf",
    "IrMemberOf",
};

const char* call_conv2str[] = {
    "CallConvSyscall",
    "CallConvCyb",
    "CallConvCDecl",
};

const char* ir_instr_type2str[] = {
    "IrAlloc",
    "IrCall",
    "IrReturn",
    "IrAssign",
    "IrBranch",
    "IrCondBranch",
    "IrUnOp",
    "IrBinOp",
    "IrCast",
};

const char* patch_type2str[] = {
    "PatchAddressOf",
    "PatchJmp2Instr",
    "PatchJmp2Epilogue",
    "PatchCall",
};

const char* un_op_type2str[] = {
    "UnOpInc",
    "UnOpNeg",
    "UnOpLogicalNot",
    "UnOpAddr",
};

const char* bin_op_type2str[] = {
    "BinOpPlus",
    "BinOpMinus",
    "BinOpLess",
    "BinOpLessEql",
    "BinOpGreater",
    "BinOpGreaterEql",
    "BinOpEql",
    "BinOpNotEql",
    "BinOpMod",
    "BinOpMul",
    "BinOpDiv",
    "BinOpLogicalOr",
    "BinOpLogicalAnd",
};
