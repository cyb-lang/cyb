#pragma once

#include "tokenizer.h"
#include "common.h"

#include <stdint.h>
#include <stdbool.h>

// codenames for the x64 registers to make code more understandable
enum {
    Reg_rax = 0, Reg_rcx, Reg_rdx, Reg_rbx, Reg_rsp, Reg_rbp, Reg_rsi, Reg_rdi,
    Reg_r8,      Reg_r9,  Reg_r10, Reg_r11, Reg_r12, Reg_r13, Reg_r14, Reg_r15,
};
extern const char* reg_code2str[16];

void write_x64_movabs2reg(Buffer* buffer, uint64_t imm, uint8_t reg);
void write_x64_jmp_rel32(Buffer* buffer, int32_t offset);
void patch_jmp(Buffer* buffer, size_t patch_pos, size_t jmp_target);
void patch_call(Buffer* buffer, size_t patch_pos, int32_t offset);
void write_jmp_conditional(Buffer* buffer, TokenType op, int32_t offset);
void patch_jmp_conditional(Buffer* buffer, size_t patch_pos, TokenType op, size_t jmp_target);

typedef struct {
    bool has_0f_prefix;

    bool has_op_size_prefix;
    uint8_t op_size_prefix;

    bool has_rex;
    bool w, r, x, b;

    uint8_t opcode;

    bool has_modrm;
    uint8_t mod, reg, rm;

    bool has_sib;
    uint8_t scale, index, base;

    bool has_disp;
    uint8_t disp_size; // in bytes
    int64_t disp;

    bool has_imm;
    uint8_t imm_size; // in bytes
    uint64_t imm;
} x64_Instr;

void write_x64_Instr(Buffer* buf, x64_Instr ins);


typedef enum {
    OpAdd = 0,
    OpSub,

    OpAnd,
    OpXor,

    OpMov,
    OpMovEql,
    OpMovNEql,
    OpLea,

    OpMul,
    OpDiv,

    OpInc,
    OpDec,
    OpNeg,

    OpCmp,

    OpSetOverflow,
    OpSetNOverflow,
    OpSetBlw,
    OpSetCarry,
    OpSetEql,
    OpSetNEql,
    OpSetBlwEql,
    OpSetAbv,
    OpSetSign,
    OpSetNSign,
    OpSetParity,
    OpSetNParity,
    OpSetLess,
    OpSetNLess,
    OpSetLessEql,
    OpSetGreater,

    OpPush,
    OpPop,

    OpJmp,

    OpRet,
    OpSyscall,
} OpMnemonic;
extern const char* op_mnemonic2str[35];

typedef enum { NullLoc, Register, Memory, Imm } OperandLoc;
extern const char* operand_type2str[4];
typedef struct {
    OperandLoc type;
    uint8_t reg;
    uint8_t imm_size; // in bytes
    uint64_t imm;
} OperandType;

typedef struct {
    bool use_simple, use_complex, use_rip_relative; // only one can be active
    uint8_t scale, index; // for base use OperandType reg field
    int32_t disp;
    uint8_t disp_size;
} IndexingInfo;


// operand size must be passed in bits, not bytes
void write_x64_op0(Buffer* buf, OpMnemonic op);
void write_x64_op1(Buffer* buf, OpMnemonic op, OperandType src, uint8_t operand_size, IndexingInfo indexing);
void write_x64_op2(Buffer* buf, OpMnemonic op, OperandType src, OperandType dst, uint8_t operand_size, IndexingInfo indexing);

void write_x64_movzx(Buffer* buf, OperandType src, uint8_t src_bits, uint8_t dst_reg, uint8_t dst_bits, IndexingInfo idx);
void write_x64_movsx(Buffer* buf, OperandType src, uint8_t src_bits, uint8_t dst_reg, uint8_t dst_bits, IndexingInfo idx);

void write_x64_mov_from_ds_32(Buffer* buf, uint32_t addr, uint8_t reg, uint8_t bit_size);
void write_x64_mov_to_ds_32(Buffer* buf, uint32_t addr, uint8_t reg, uint8_t bit_size);
