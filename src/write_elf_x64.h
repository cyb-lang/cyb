#pragma once

#include "cmd_options.h"
#include "parser.h"
#include "cyb_ir.h"

void write_executable(ParseInfo* info, CmdOptions cmd_options, IrInfo* ir_info);
