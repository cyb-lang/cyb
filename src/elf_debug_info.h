#pragma once

#include "list.h"
#include "tokenizer.h"

#include <stdbool.h>

typedef struct {
    SourceLocation src_loc;
    size_t addr;
} LineAddr;

typedef enum {
    Loc_None, Loc_Memory, Loc_Register, Loc_Stack, Loc_Imm,
} VarLocationType;
extern const char* loc_type2str[5];
typedef struct {
    VarLocationType type;

    union {
        size_t addr; // offset into data buffer
        size_t reg;
        int64_t stack_pos; // relative to frame base pointer (usually RBP)
        uint64_t imm;
    };
    bool indirect; // only usefull when Loc_Register is used

    // type information
    bool is_ptr, is_array, is_signed;
    uint32_t num_elems;
    uint32_t bit_size;
    uint32_t imm_min_bit_size; // TODO: <- when is this necessary???
} VarLocation;
// examples of VarLocation for various variable types:
//
// string literals are always Loc_Memory, even if they are used in a fn_call or
//   top_level_decl or whatever else
// "blah"
// - loc = { .type=Loc_Memory, .addr=0x1000, .is_array=true, .num_elems=5, .bit_size=8, }
//
// top_level_decl integer: i8 nl = 10
// global variable -> allocated on binary (note: it's not const)
// - loc = { .type=Loc_Memory, .addr=0x1000, .is_signed=true, .bit_size=8 }
//
// decl inside function: func() { i8 nl = 10 }
// locals variable -> allocated on stack, even though it starts as a constant (10)
// - loc = { .type=Loc_Stack, .stack_pos=-0x8, .is_signed=true, .bit_size=8 }
//
// static array inside function: func() { u8 arr[] = {1, 2, 3} }
// static arrays get allocated in binary (same goes for global static arrays variables)
// - loc = { .type=Loc_Memory, .addr=0x1000, .is_array=true, .num_elems=3,
//       .bit_size=8 }
//
// dynamic arrays inside functions: func() { i32 things[5] }
// - loc = { .type=Loc_Stack, .stack_pos=-0x10, .is_array=true, .num_elems=5,
//       .is_signed=true, .bit_size=32 }
//
// dynamic arrays as globals: func() { u32 things[5] }
// - loc = { .type=Loc_Memory, .addr=0x1000, .is_array=true, .num_elems=5,
//       .bit_size=32 }
//
// string passed as fn argument: func(&u8 str) {}
// - loc = { .type=Loc_Stack, .stack_pos=0x10, .is_ptr=true, .bit_size=8 }

typedef struct {
    uint8_t* name;
    size_t type_idx; // index into types list in DebugInfo
    VarLocation loc;
} VarInfo;

typedef struct {
    char* name;
    uint64_t start_addr;
    size_t line;
    uint32_t size;

    List vars;
} FuncInfo;

typedef struct {
    List filenames; // list of strings
    List line_addrs; // list of LineAddr's
    char* compile_dir;
    List funcs; // list of FuncInfo's
    List types; // list of TypeInfo's
    List struct_types; // copy of the list from ParseInfo
    List global_vars; // list of VarInfo

    uint8_t tag_abbrev_codes[256]; // using DW_TAG as index to get corresponding abbrev code

    size_t virtual_addr, code_addr;
} DebugInfo;

void write_debug_sections(FILE* elf_file, List* section_headers, Buffer* section_name_strtab, DebugInfo dbg_info);
