#include "emit_ir.h"

uint32_t next_size_in_bits(uint32_t start) {
    uint32_t power_of_two = 8;
    while (start > power_of_two) power_of_two *= 2;
    return power_of_two;
}

typedef struct {
    ParseInfo* info;
    IrInfo* ir_info;
    IrFunction* ir_fn;
    List break_indices; // list of size_t
} EmitContext;

typedef struct {
    bool pure_obj; // if true, only the `obj` field is valid 
    IrObj obj;
    IrObjRef ref;
} InstrResult;

IrTypeInfo convert_to_ir_type(ParseInfo* info, TypeInfo type) {
    if (type.type == Type_None) return (IrTypeInfo) { .bit_size = 0 };

    debug_assert(type.type != Type_String, "stub\n");
    debug_assert(type.type != Type_Float, "stub\n");

    IrTypeInfo ir_type = {
        .is_ptr = (type.ptr_level > 0),
        .is_array = type.is_array, .array_size = type.array_size,
        .is_signed = type.is_signed,
        .bit_size = next_size_in_bits(type.n_bits),
    };

    if (type.type == Type_Struct) {
        ir_type.is_struct = true;
        ir_type.struct_name = type.name;
        ir_type.members = create_list(IrTypeInfo);
        StructType s_type = at_index(&info->struct_types, type.struct_idx, StructType);
        for (size_t i = 0; i < s_type.members.length; i++) {
            StructMember s_member = at_index(&s_type.members, i, StructMember);
            IrTypeInfo member_ir_type = convert_to_ir_type(info, s_member.type_info);
            debug_assert(!member_ir_type.is_array, "\n");
            append(&ir_type.members, member_ir_type);
        }
    }

    return ir_type;
}

IrObj get_obj_from_result(InstrResult result) {
    if (result.pure_obj) return result.obj;
    return (IrObj) {
        .type = IrVarRef,
        .type_info = result.obj.type_info,
        .ref = result.ref,
    };
}

static IrObj obj_from_ref(EmitContext* context, IrObjRef ref) {
    List* obj_list = NULL;
    switch (ref.type) {
        case IrRefData: obj_list = &context->ir_info->data_objs; break;
        case IrRefGlobal: obj_list = &context->ir_info->global_objs; break;
        case IrRefLocal: obj_list = &context->ir_fn->local_objs; break;
    }
    return at_index(obj_list, ref.idx, IrObj);
}

InstrResult make_result_from_ref(EmitContext* context, IrObjRef ref) {
    IrObj obj = obj_from_ref(context, ref);
    return (InstrResult) {
        .obj = {
            .type = IrVarRef,
            .type_info = obj.type_info,
            .ref = ref,
        },
        .ref = ref,
    };
}

IrObj var_ref_from_result(EmitContext* context, InstrResult result) {
    return (IrObj) {
        .type = IrVarRef,
        .type_info = result.obj.type_info,
        .ref = result.ref,
    };
}

InstrResult add_local_var(EmitContext* context, IrTypeInfo type_info) {
    IrObj local = {
        .type = IrVar,
        .type_info = type_info,
    };
    append(&context->ir_fn->local_objs, local);
    return (InstrResult) {
        .obj = local,
        .ref = {
            .type = IrRefLocal,
            .idx = context->ir_fn->local_objs.length - 1,
        },
    };
}

bool stmt_makes_new_local(Stmt stmt) {
    switch (stmt.type) {
        case Stmt_FunctionCall:
        case Stmt_BinaryOp:
        case Stmt_ArrayInit:
        case Stmt_DotOp:
            return true;

        case Stmt_If:
        case Stmt_Else:
        case Stmt_While:
        case Stmt_For:
        case Stmt_Break:
        case Stmt_Return:
        case Stmt_Variable:
        case Stmt_StructDef:
        case Stmt_Assignment:
        case Stmt_Indexing:
            return false;

        default: debug_crash("%s\n", stmt_type2str[stmt.type]);
    }
}

InstrResult emit_ir_stmt(EmitContext* context, Stmt stmt);

void insert_cast_if_needed(EmitContext* context, IrObj* src, IrTypeInfo dst_type) {
    bool needs_casting = false;
    IrTypeInfo src_type = src->type_info;
    if (dst_type.is_ptr && type_info_bits(src_type) != 64) needs_casting = true;
    else if (type_info_bits(src_type) != type_info_bits(dst_type)) needs_casting = true;
    else if (src_type.is_signed != dst_type.is_signed) needs_casting = true;
    if (!needs_casting) return;

    if (src->type == IrLiteral) {
        src->type_info.bit_size = type_info_bits(dst_type);
        return;
    }

    if (src->type != IrVarRef) {
        InstrResult result = add_local_var(context, src_type);
        IrInstr instr = {
            .type = IrAssign,
            .assign_dst_obj = get_obj_from_result(result),
            .assign_src_obj = *src,
        };
        append(&context->ir_fn->instrs, instr);
        *src = get_obj_from_result(result);
    }

    IrInstr instr = {
        .type = IrCast,
        .cast_src_ref = src->ref,
        .cast_dst_type = dst_type,
    };
    InstrResult result = add_local_var(context, dst_type);
    instr.local_idx = result.ref.idx;
    append(&context->ir_fn->instrs, instr);

    *src = get_obj_from_result(result);
}

InstrResult emit_ir_stmt_fn_call(EmitContext* context, Stmt stmt) {
    IrInstr instr = {
        .type = IrCall, .src_loc = stmt.src_loc,
        .call_conv = CallConvCyb,
    };
    instr.args = create_list(IrObj);
    instr.callee_arg_types = create_list(IrTypeInfo);

    Function callee = at_index(&context->info->functions, stmt.function_idx, Function);
    instr.callee_name = callee.name;

    List callee_arg_types = callee.arg_types;

    if (stmt.using_fnptr) {
        instr.call_conv = CallConvCDecl;
        instr.using_fnptr = true;
        TypeInfo fp_info = at_index(&context->info->variables, stmt.variable, Variable).type_info;
        FnPtrType fp_type = at_index(&context->info->fnptr_types, fp_info.fnptr_idx, FnPtrType);
        callee_arg_types = fp_type.arg_types;
    }
    else if (callee.is_extern) {
        instr.call_conv = CallConvCDecl;
    }
    else if (callee.is_syscall) {
        instr.call_conv = CallConvSyscall;
        instr.syscall_code = callee.code;
    }

    for (size_t i = 0; i < stmt.list.length; i++) {
        Stmt arg_stmt = indirect_index(&context->info->stmts, &stmt.list, i, Stmt);
        InstrResult emit_result = emit_ir_stmt(context, arg_stmt);

        TypeInfo arg_type = at_index(&callee_arg_types, i, TypeInfo);
        IrTypeInfo obj_type = convert_to_ir_type(context->info, arg_type);
        append(&instr.callee_arg_types, obj_type);

        IrObj arg_obj = get_obj_from_result(emit_result);
        insert_cast_if_needed(context, &arg_obj, obj_type);
        append(&instr.args, arg_obj);
    }

    InstrResult result = add_local_var(context, convert_to_ir_type(context->info, callee.return_type));
    instr.local_idx = result.ref.idx;
    append(&context->ir_fn->instrs, instr);

    return result;
}

InstrResult emit_ir_stmt_if(EmitContext* context, Stmt stmt) {
    Stmt cond_stmt = at_index(&context->info->stmts, stmt.cond_stmt, Stmt);
    InstrResult cond_result = emit_ir_stmt(context, cond_stmt);

    IrInstr cond_br_instr = {
        .type = IrCondBranch, .src_loc = stmt.src_loc,
        .cond_obj = get_obj_from_result(cond_result),
    };
    size_t cond_br_instr_idx = context->ir_fn->instrs.length;
    append(&context->ir_fn->instrs, cond_br_instr);

    for (size_t i = 0; i < stmt.list.length; i++) {
        Stmt s = indirect_index(&context->info->stmts, &stmt.list, i, Stmt);
        emit_ir_stmt(context, s);
    }

    IrInstr skip_else_instr = { .type = IrBranch, };
    size_t skip_else_idx = context->ir_fn->instrs.length;

    Stmt else_stmt = at_index(&context->info->stmts, stmt.else_stmt, Stmt);
    if (else_stmt.type != Stmt_None) append(&context->ir_fn->instrs, skip_else_instr);

    // patch jump if condition failed
    IrInstr* cond_br_instr_ptr = &at_index(&context->ir_fn->instrs, cond_br_instr_idx, IrInstr);
    cond_br_instr_ptr->target_instr_idx = context->ir_fn->instrs.length;

    if (else_stmt.type == Stmt_If) {
        emit_ir_stmt_if(context, else_stmt);
    }
    else if (else_stmt.type == Stmt_Else) {
        for (size_t i = 0; i < else_stmt.list.length; i++) {
            Stmt s = indirect_index(&context->info->stmts, &else_stmt.list, i, Stmt);
            emit_ir_stmt(context, s);
        }
    }
    
    // patch the jump to the end of all if/else blocks
    if (else_stmt.type != Stmt_None) {
        IrInstr* ins_ptr = &at_index(&context->ir_fn->instrs, skip_else_idx, IrInstr);
        ins_ptr->target_instr_idx = context->ir_fn->instrs.length;
    }

    return (InstrResult) {0};
}

InstrResult emit_ir_stmt_while(EmitContext* context, Stmt stmt) {
    Stmt cond_stmt = at_index(&context->info->stmts, stmt.cond_stmt, Stmt);
    size_t cond_instr_idx = context->ir_fn->instrs.length;
    InstrResult cond_result = emit_ir_stmt(context, cond_stmt);

    IrInstr cond_br_instr = {
        .type = IrCondBranch, .src_loc = stmt.src_loc,
        .cond_obj = get_obj_from_result(cond_result),
    };
    size_t cond_br_instr_idx = context->ir_fn->instrs.length;
    append(&context->ir_fn->instrs, cond_br_instr);

    for (size_t i = 0; i < stmt.list.length; i++) {
        Stmt s = indirect_index(&context->info->stmts, &stmt.list, i, Stmt);
        emit_ir_stmt(context, s);
    }

    IrInstr loopback_instr = {
        .type = IrBranch,
        .target_instr_idx = cond_instr_idx,
    };
    append(&context->ir_fn->instrs, loopback_instr);

    // patch the `cond_instr` to point to whatever comes after the block
    IrInstr* cond_br_instr_ptr = &at_index(&context->ir_fn->instrs, cond_br_instr_idx, IrInstr);
    cond_br_instr_ptr->target_instr_idx = context->ir_fn->instrs.length;

    // fix any `Stmt_Break` statements that might have been inside the loop
    for (size_t i = 0; i < context->break_indices.length; i++) {
        IrInstr* ins = &indirect_index(&context->ir_fn->instrs, &context->break_indices, i, IrInstr);
        ins->target_instr_idx = context->ir_fn->instrs.length; 
    }
    clear_list(&context->break_indices);

    return (InstrResult) {0};
}

// the only difference between this and emit_ir_stmt_while is that here
// have the extra init_stmt and the extra step_stmt before jumping back
// to the cond part of the loop
InstrResult emit_ir_stmt_for(EmitContext* context, Stmt stmt) {
    Stmt init_stmt = at_index(&context->info->stmts, stmt.init_stmt, Stmt);
    emit_ir_stmt(context, init_stmt);

    Stmt cond_stmt = at_index(&context->info->stmts, stmt.cond_stmt, Stmt);
    size_t cond_instr_idx = context->ir_fn->instrs.length;
    InstrResult cond_result = emit_ir_stmt(context, cond_stmt);

    IrInstr cond_br_instr = {
        .type = IrCondBranch, .src_loc = stmt.src_loc,
        .cond_obj = get_obj_from_result(cond_result),
    };
    size_t cond_br_instr_idx = context->ir_fn->instrs.length;
    append(&context->ir_fn->instrs, cond_br_instr);

    for (size_t i = 0; i < stmt.list.length; i++) {
        Stmt s = indirect_index(&context->info->stmts, &stmt.list, i, Stmt);
        emit_ir_stmt(context, s);
    }

    Stmt step_stmt = at_index(&context->info->stmts, stmt.step_stmt, Stmt);
    emit_ir_stmt(context, step_stmt);

    IrInstr loopback_instr = {
        .type = IrBranch,
        .target_instr_idx = cond_instr_idx,
    };
    append(&context->ir_fn->instrs, loopback_instr);

    // patch the `cond_instr` to point to whatever comes after the block
    IrInstr* cond_br_instr_ptr = &at_index(&context->ir_fn->instrs, cond_br_instr_idx, IrInstr);
    cond_br_instr_ptr->target_instr_idx = context->ir_fn->instrs.length;

    // fix any `Stmt_Break` statements that might have been inside the loop
    for (size_t i = 0; i < context->break_indices.length; i++) {
        IrInstr* ins = &indirect_index(&context->ir_fn->instrs, &context->break_indices, i, IrInstr);
        ins->target_instr_idx = context->ir_fn->instrs.length; 
    }
    clear_list(&context->break_indices);

    return (InstrResult) {0};
}

InstrResult emit_ir_stmt_break(EmitContext* context, Stmt stmt) {
    IrInstr instr = {
        .type = IrBranch, .src_loc = stmt.src_loc,
    };
    append(&context->break_indices, context->ir_fn->instrs.length);
    append(&context->ir_fn->instrs, instr);

    return (InstrResult) {0};
}

InstrResult emit_ir_stmt_return(EmitContext* context, Stmt stmt) {
    Stmt ret_stmt = at_index(&context->info->stmts, stmt.return_stmt, Stmt);
    InstrResult emit_result = emit_ir_stmt(context, ret_stmt); 
    IrObj ret_obj = get_obj_from_result(emit_result);

    // TODO: type casting

    IrInstr instr = {
        .type = IrReturn, .src_loc = stmt.src_loc,
        .ret_obj = ret_obj,
    };
    append(&context->ir_fn->instrs, instr);

    return (InstrResult) {0};
}

InstrResult emit_ir_stmt_decl(EmitContext* context, Stmt stmt) {
    Variable* var = &at_index(&context->info->variables, stmt.variable, Variable);

    InstrResult result = {0};
    if (stmt.assign_stmt != 0) {
        Stmt assign_stmt = at_index(&context->info->stmts, stmt.assign_stmt, Stmt);
        result = emit_ir_stmt(context, assign_stmt);

        // in case the right hand side of the declaration does not already produce
        // a new local variable, we have to create one, and assign it the result
        if (result.pure_obj || !stmt_makes_new_local(assign_stmt)) {
            IrInstr instr = {
                .type = IrAssign, .src_loc = stmt.src_loc,
                .assign_src_obj = result.obj,
            };
            result = add_local_var(context, convert_to_ir_type(context->info, var->type_info));
            instr.assign_dst_obj = var_ref_from_result(context, result);
            append(&context->ir_fn->instrs, instr);
        }
    }
    else {
        result = add_local_var(context, convert_to_ir_type(context->info, var->type_info));
        IrInstr instr = {
            .type = IrAlloc, .local_idx = result.ref.idx, .src_loc = stmt.src_loc,
            .alloc_type_info = result.obj.type_info,
        };
        append(&context->ir_fn->instrs, instr);
    }

    var->obj_ref = result.ref;

    return result;
}

InstrResult emit_ir_stmt_assignment(EmitContext* context, Stmt stmt) {
    Stmt left_stmt = at_index(&context->info->stmts, stmt.op_left, Stmt);
    InstrResult left_result = emit_ir_stmt(context, left_stmt);

    Stmt right_stmt = at_index(&context->info->stmts, stmt.op_right, Stmt);
    InstrResult right_result = emit_ir_stmt(context, right_stmt);

    IrInstr instr = {
        .type = IrAssign, .src_loc = stmt.src_loc,
        .assign_dst_obj = get_obj_from_result(left_result),
        .assign_src_obj = get_obj_from_result(right_result),
    };
    append(&context->ir_fn->instrs, instr);

    return left_result;
}

InstrResult emit_ir_stmt_array_init(EmitContext* context, Stmt stmt) {
    IrTypeInfo array_type = convert_to_ir_type(context->info, stmt.array_type);
    InstrResult array_result = add_local_var(context, array_type);

    IrInstr alloc_instr = {
        .type = IrAlloc, .src_loc = stmt.src_loc,
        .local_idx = array_result.ref.idx,
        .alloc_type_info = array_type,
    };
    append(&context->ir_fn->instrs, alloc_instr);

    for (size_t i = 0; i < stmt.list.length; i++) {
        Stmt elem_stmt = indirect_index(&context->info->stmts, &stmt.list, i, Stmt);
        InstrResult elem_result = emit_ir_stmt(context, elem_stmt);
        IrObj elem_obj = get_obj_from_result(elem_result);

        // assign the value to the array element
        IrInstr assign_instr = {
            .type = IrAssign, .src_loc = stmt.src_loc,
            .assign_dst_obj = {
                .type = IrIndexOf,
                .type_info = base_type_of(array_type),
                .ref = var_ref_from_result(context, array_result).ref,
                .index_is_const = true, .const_index = i,
            },
            .assign_src_obj = elem_obj,
        };
        append(&context->ir_fn->instrs, assign_instr);
    }

    return array_result;
}

InstrResult emit_ir_stmt_variable(EmitContext* context, Stmt stmt) {
    Variable var = at_index(&context->info->variables, stmt.variable, Variable);
    bool was_first = !var.not_first;
    if (var.not_first) var = at_index(&context->info->variables, var.first, Variable);

    if (var.type_info.type == Type_String) {
        IrObj str_obj = {
            .type = IrLiteral, .literal_type = IrLiteralString,
            .str_literal = var.name,
        };
        size_t data_idx = context->ir_info->data_objs.length;
        append(&context->ir_info->data_objs, str_obj);
        return (InstrResult) {
            .pure_obj = true,
            .obj = {
                .type = IrAddrStub,
                .type_info = { .is_ptr = true, .bit_size = 8 },
                .ref = { .type = IrRefData, .idx = data_idx },
            },
        };
    }

    if (var.is_value) {
        return (InstrResult) {
            .pure_obj = true,
            .obj = {
                .type = IrLiteral, .literal_type = IrLiteralInt,
                .type_info = convert_to_ir_type(context->info, var.type_info),
                .int_literal = var.value,
            },
        };
    }

    IrObj var_obj = obj_from_ref(context, var.obj_ref);
    if (was_first) {
        debug_print("stub: this might not be correct\n");
        return (InstrResult) {
            .pure_obj = true,
            .obj = var_obj,
            .ref = var.obj_ref,
        };
    }
    else {
        return make_result_from_ref(context, var.obj_ref);
    }
}

InstrResult emit_ir_stmt_unary_op(EmitContext* context, Stmt stmt) {
    Stmt operand = at_index(&context->info->stmts, stmt.op_left, Stmt);
    InstrResult emit_result = emit_ir_stmt(context, operand);
    IrObj obj = get_obj_from_result(emit_result);

    if (stmt.op_token == Token_reference) {
        if (obj.type == IrVarRef) {
            obj.type = IrAddrStub;
            obj.type_info.is_ptr = true;
            return (InstrResult) { .pure_obj = true, .obj = obj };
        }
        else {
            IrInstr instr = {
                .type = IrUnOp, .src_loc = stmt.src_loc,
                .un_op_type = UnOpAddr,
                .un_op_obj = obj,
            };
            InstrResult result = add_local_var(context, pointer_to_type(obj.type_info));
            instr.local_idx = result.ref.idx;
            append(&context->ir_fn->instrs, instr);
            return result;
        }
    }
    else if (stmt.op_token == Token_increment) {
        debug_assert(obj.type == IrVarRef, "stub: %s\n", ir_obj_type2str[obj.type]);
        IrInstr instr = {
            .type = IrUnOp, .src_loc = stmt.src_loc,
            .un_op_type = UnOpInc,
            .un_op_obj = obj,
        };
        InstrResult result = add_local_var(context, obj.type_info);
        instr.local_idx = result.ref.idx;
        append(&context->ir_fn->instrs, instr);

        IrInstr assign = {
            .type = IrAssign, .src_loc = stmt.src_loc,
            .assign_dst_obj = obj,
            .assign_src_obj = get_obj_from_result(result),
        };
        append(&context->ir_fn->instrs, assign);

        return result;
    }
    else if (stmt.op_token == Token_minus) {
        if (obj.type == IrLiteral) {
            obj.type_info.is_signed = true;
            obj.int_literal = -obj.int_literal;
            return (InstrResult) { .pure_obj = true, .obj = obj };
        }

        IrInstr instr = {
            .type = IrUnOp, .src_loc = stmt.src_loc,
            .un_op_type = UnOpNeg,
            .un_op_obj = obj,
        };
        InstrResult result = add_local_var(context, obj.type_info);
        instr.local_idx = result.ref.idx;
        append(&context->ir_fn->instrs, instr);
        return result;
    }
    else if (stmt.op_token == Token_logical_not) {
        // TODO: cast into boolean if needed

        IrInstr instr = {
            .type = IrUnOp, .src_loc = stmt.src_loc,
            .un_op_type = UnOpLogicalNot,
            .un_op_obj = obj,
        };
        InstrResult result = add_local_var(context, obj.type_info);
        instr.local_idx = result.ref.idx;
        append(&context->ir_fn->instrs, instr);
        return result;
    }
    else {
        debug_crash("stub: unary_op %s\n", token_type2str[stmt.op_token]);
    }
}

InstrResult emit_ir_stmt_binary_op(EmitContext* context, Stmt stmt) {
    IrInstr instr = {
        .type = IrBinOp, .src_loc = stmt.src_loc,
    };
    TokenType t = stmt.op_token;
    if (t == Token_plus) instr.bin_op_type = BinOpPlus;
    else if (t == Token_minus) instr.bin_op_type = BinOpMinus;
    else if (t == Token_smaller) instr.bin_op_type = BinOpLess;
    else if (t == Token_smaller_equal) instr.bin_op_type = BinOpLessEql;
    else if (t == Token_greater) instr.bin_op_type = BinOpGreater;
    else if (t == Token_greater_equal) instr.bin_op_type = BinOpGreaterEql;
    else if (t == Token_is_equal) instr.bin_op_type = BinOpEql;
    else if (t == Token_different) instr.bin_op_type = BinOpNotEql;
    else if (t == Token_mod) instr.bin_op_type = BinOpMod;
    else if (t == Token_times) instr.bin_op_type = BinOpMul;
    else if (t == Token_div) instr.bin_op_type = BinOpDiv;
    else if (t == Token_logical_or) instr.bin_op_type = BinOpLogicalOr;
    else if (t == Token_logical_and) instr.bin_op_type = BinOpLogicalAnd;
    else debug_crash("stub: %s\n", token_type2str[stmt.op_token]);

    // TODO: cast operands into booleans if needed

    Stmt left_stmt = at_index(&context->info->stmts, stmt.op_left, Stmt);
    InstrResult left_result = emit_ir_stmt(context, left_stmt);
    instr.left_obj = get_obj_from_result(left_result);

    Stmt right_stmt = at_index(&context->info->stmts, stmt.op_right, Stmt);
    InstrResult right_result = emit_ir_stmt(context, right_stmt);
    instr.right_obj = get_obj_from_result(right_result);

    IrTypeInfo result_type = instr.left_obj.type_info;
    // comparision binary ops (like ==, <, >, etc) return a boolean instead of the operand type
    if (t == Token_is_equal || t == Token_different
        || t == Token_smaller || t == Token_smaller_equal
        || t == Token_greater || t == Token_greater_equal) {
        result_type = (IrTypeInfo) { .bit_size = 8 };
    }
    InstrResult result = add_local_var(context, result_type);
    instr.local_idx = result.ref.idx;
    append(&context->ir_fn->instrs, instr);

    return result;
}

InstrResult emit_ir_stmt_indexing(EmitContext* context, Stmt stmt) {
    Stmt indexed_stmt = at_index(&context->info->stmts, stmt.indexed_stmt, Stmt);
    InstrResult indexed_result = emit_ir_stmt(context, indexed_stmt);

    Stmt index_stmt = at_index(&context->info->stmts, stmt.index_stmt, Stmt);
    InstrResult index_result = emit_ir_stmt(context, index_stmt);

    InstrResult result = {
        .pure_obj = true,
        .obj = {
            .type = IrIndexOf,
            .type_info = base_type_of(indexed_result.obj.type_info),
        },
    };

    IrObj index_obj = get_obj_from_result(index_result);

    if (index_result.pure_obj && index_obj.type == IrLiteral) {
        debug_assert(index_obj.literal_type == IrLiteralInt, "\n");
        debug_assert(index_obj.int_literal <= 0xffffffff, "\n");
        result.obj.index_is_const = true;
        result.obj.const_index = index_obj.int_literal;
    }
    else if (index_result.pure_obj) {
        // index has to be referenceable, so put it in a local if needed
        InstrResult new_index_result = add_local_var(context, index_obj.type_info);
        IrInstr instr = {
            .type = IrAssign, .src_loc = stmt.src_loc,
            .assign_dst_obj = get_obj_from_result(new_index_result),
            .assign_src_obj = index_obj,
        };
        append(&context->ir_fn->instrs, instr);
        index_result = new_index_result;
    }

    result.obj.ref = var_ref_from_result(context, indexed_result).ref;
    result.obj.index_ref = var_ref_from_result(context, index_result).ref;

    return result;
}

InstrResult emit_ir_stmt_dot_op(EmitContext* context, Stmt stmt) {
    Variable var = at_index(&context->info->variables, stmt.variable, Variable);
    if (var.not_first) var = at_index(&context->info->variables, var.first, Variable);

    IrTypeInfo struct_type = obj_from_ref(context, var.obj_ref).type_info;
    IrTypeInfo member_type = at_index(&struct_type.members, stmt.struct_member_idx, IrTypeInfo);

    IrObj member_obj = {
        .type = IrMemberOf,
        .type_info = member_type,
        .member_idx = stmt.struct_member_idx,
        .ref = var.obj_ref,
    };

    return (InstrResult) {
        .pure_obj = true,
        .obj = member_obj,
    };
}

InstrResult emit_ir_stmt(EmitContext* context, Stmt stmt) {
    switch (stmt.type) {
        case Stmt_FunctionCall: return emit_ir_stmt_fn_call(context, stmt);
        case Stmt_If: return emit_ir_stmt_if(context, stmt);
        case Stmt_Else: debug_crash("should've been handled with Stmt_If\n");
        case Stmt_While: return emit_ir_stmt_while(context, stmt);
        case Stmt_For: return emit_ir_stmt_for(context, stmt);
        case Stmt_Break: return emit_ir_stmt_break(context, stmt);
        case Stmt_Return: return emit_ir_stmt_return(context, stmt);
        case Stmt_Decl: return emit_ir_stmt_decl(context, stmt);
        case Stmt_Assignment: return emit_ir_stmt_assignment(context, stmt);
        case Stmt_ArrayInit: return emit_ir_stmt_array_init(context, stmt);
        case Stmt_Variable: return emit_ir_stmt_variable(context, stmt);
        case Stmt_UnaryOp: return emit_ir_stmt_unary_op(context, stmt);
        case Stmt_BinaryOp: return emit_ir_stmt_binary_op(context, stmt);
        case Stmt_Indexing: return emit_ir_stmt_indexing(context, stmt);
        case Stmt_DotOp: return emit_ir_stmt_dot_op(context, stmt);

        default: debug_crash("stub: %s @ %s:%lu:%lu\n", stmt_type2str[stmt.type], expand_src(stmt));
    }
}

IrFunction emit_ir_function(Function fn, ParseInfo* info, IrInfo* ir_info) {
    IrFunction ir_fn = {
        .name = fn.name, 
        .call_conv = fn.is_extern ? CallConvCDecl : CallConvCyb,
        .return_type = convert_to_ir_type(info, fn.return_type), 
        .is_extern = fn.is_extern
    };
    ir_fn.args_type_info = create_list(IrTypeInfo);
    ir_fn.instrs = create_list(IrInstr);
    ir_fn.local_objs = create_list(IrObj);

    // setup function arguments as local variables
    for (size_t i = 0; i < fn.args.length; i++) {
        Variable* arg_var = &indirect_index(&info->variables, &fn.args, i, Variable);
        if (arg_var->not_first) {
            arg_var = &indirect_index(&info->variables, &fn.args, arg_var->first, Variable);
        }

        IrTypeInfo ir_type = convert_to_ir_type(info, arg_var->type_info);
        append(&ir_fn.args_type_info, ir_type);
        IrObj ir_arg = {
            .type = IrVar,
            .type_info = ir_type,
        };
        arg_var->obj_ref = (IrObjRef) { .type = IrRefLocal, .idx = ir_fn.local_objs.length };
        append(&ir_fn.local_objs, ir_arg);
    }

    EmitContext context = {
        .info = info,
        .ir_info = ir_info,
        .ir_fn = &ir_fn,
    };
    context.break_indices = create_list(size_t);
    for (size_t i = 0; i < fn.stmts.length; i++) {
        Stmt stmt = indirect_index(&info->stmts, &fn.stmts, i, Stmt);
        emit_ir_stmt(&context, stmt);
    }

    return ir_fn;
}

IrInfo emit_ir(ParseInfo* info) {
    IrInfo ir_info = {0};
    ir_info.data_objs = create_list(IrObj);
    ir_info.global_objs = create_list(IrObj);
    ir_info.functions = create_list(IrFunction);

    // global and data ir objs
    for (size_t i = 0; i < info->top_level_decls.length; i++) {
        Stmt stmt = indirect_index(&info->stmts, &info->top_level_decls, i, Stmt);
        if (!stmt.is_global || stmt.type == Stmt_StructDef) continue;

        debug_assert(stmt.type == Stmt_Decl, "\n");
        debug_assert(stmt.assign_stmt != 0, "stub: global decl w/out initial value, stmt @ %s:%lu:%lu\n", expand_src(stmt));
        Variable stmt_var = at_index(&info->variables, stmt.variable, Variable);

        Stmt assign_stmt = at_index(&info->stmts, stmt.assign_stmt, Stmt);
        Variable assign_var = at_index(&info->variables, assign_stmt.variable, Variable);
        debug_assert(!assign_var.not_first, "only literal are supported at global scope for now\n");

        if (assign_stmt.type == Stmt_ArrayInit) {
            // to whomever has to fix this: sorry
            debug_print("global array hackery if afoot\n");
            IrObj array_obj = {
                .type = IrVar,
                .type_info = convert_to_ir_type(info, stmt_var.type_info),
                .is_global_array_hack = true,
            };
            array_obj.global_array_hack = create_list(IrObj);

            for (size_t i = 0; i < assign_stmt.list.length; i++) {
                Stmt elem_stmt = indirect_index(&info->stmts, &assign_stmt.list, i, Stmt);
                debug_assert(elem_stmt.type == Stmt_Variable, "\n");
                Variable elem_var = at_index(&info->variables, elem_stmt.variable, Variable);
                debug_assert(elem_var.type_info.type == Type_Integer, "\n");
                IrObj elem_obj = {
                    .type = IrLiteral, .literal_type = IrLiteralInt,
                    .type_info = convert_to_ir_type(info, elem_var.type_info),
                    .int_literal = elem_var.value,
                };
                append(&array_obj.global_array_hack, elem_obj);
            }

            at_index(&info->variables, stmt.variable, Variable).obj_ref = (IrObjRef) {
                .type = IrRefGlobal,
                .idx = ir_info.global_objs.length,
            };
            append(&ir_info.global_objs, array_obj);
            continue;
        }

        TypeInfo var_type = stmt_var.type_info;
        IrObj ir_obj = {
            .type_info = {
                .is_ptr = (var_type.ptr_level != 0),
                .is_signed = var_type.is_signed,
                .bit_size = var_type.n_bits,
            },
        };

        TypeInfo assign_type = assign_var.type_info;
        if (assign_type.type == Type_Integer) {
            ir_obj.type = IrLiteral;
            ir_obj.int_literal = assign_var.value;
        }
        else if (assign_type.type == Type_String) {
            IrObj str_obj = {
                .type = IrLiteral,
                .literal_type = IrLiteralString,
                .str_literal = assign_var.name,
            };
            size_t data_idx = ir_info.data_objs.length;
            append(&ir_info.data_objs, str_obj);

            ir_obj.type = IrAddrStub;
            ir_obj.ref = (IrObjRef) { .type = IrRefData, .idx = data_idx };
        }
        else {
            debug_crash("%s\n", type_type2str[assign_type.type]);
        }

        at_index(&info->variables, stmt.variable, Variable).obj_ref = (IrObjRef) {
            .type = IrRefGlobal,
            .idx = ir_info.global_objs.length,
        };
        append(&ir_info.global_objs, ir_obj);
    }

    for (size_t i = 0; i < info->functions.length; i++) {
        Function fn = at_index(&info->functions, i, Function);
        // we have some dummy Function's for syscalls that serve to hold information about
        // the argument types and syscall code. these don't produce any IR
        if (fn.is_syscall) continue;

        if (strcmp(fn.name, "main") == 0) ir_info.main_fn_idx = ir_info.functions.length;

        IrFunction ir_fn = emit_ir_function(fn, info, &ir_info);
        append(&ir_info.functions, ir_fn);
    }

    return ir_info;
}
