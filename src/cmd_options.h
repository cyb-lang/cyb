#pragma once

#include "list.h"
#include "common.h"

typedef struct {
    bool invalid;

    char* filename;
    char* output_filename;

    List link_libs; // list of uint8_t*

    bool print_debug;
    bool print_tree;
    bool print_ir;
    bool dump_tree_dot; 
} CmdOptions;

void print_usage(char *argv0);
CmdOptions parse_cmdline_args(int _argc, char** argv);
