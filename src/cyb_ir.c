#include "cyb_ir.h"
#include "x64_codegen.h"
#include "elf_debug_info.h"

#include <string.h>

static const size_t code_addr    = 0x1000;
static const size_t data_addr    = 0x2000;
static const size_t plt_addr     = 0x3000;
static const size_t got_plt_addr = 0x4000;
static const size_t virtual_addr = 0x400000;
//static const size_t code_vaddr    =    code_addr + virtual_addr;
static const size_t data_vaddr    =    data_addr + virtual_addr;
//static const size_t plt_vaddr     =     plt_addr + virtual_addr;
//static const size_t got_plt_vaddr = got_plt_addr + virtual_addr;

uint32_t type_info_bits(IrTypeInfo type_info) {
    if (type_info.is_ptr) return 64;

    uint32_t bit_size = 0;
    if (type_info.is_struct) {
        for (size_t i = 0; i < type_info.members.length; i++) {
            IrTypeInfo member_type = at_index(&type_info.members, i, IrTypeInfo);
            bit_size += type_info_bits(member_type);
        }
    }
    else {
        bit_size = type_info.bit_size;
    }

    if (type_info.is_array) bit_size *= type_info.array_size;

    return bit_size;
}

uint32_t runtime_type_info_bits(IrTypeInfo type_info) {
    if (type_info.is_struct) return type_info_bits(type_info);
    if (type_info.is_ptr || type_info.is_array) return 64;
    else return type_info.bit_size;
}

IrTypeInfo base_type_of(IrTypeInfo type) {
    type.is_ptr = false;
    type.is_array = false;
    return type;
}

IrTypeInfo pointer_to_type(IrTypeInfo type) {
    type.is_ptr = true;
    return type;
}

IrObj dummy_var(IrTypeInfo type, IrObjLoc loc, uint64_t loc_value) {
    IrObj obj = { .type = IrVar, .type_info = type, .loc = loc };
    switch (obj.loc) {
        case IrLocNone: debug_crash("\n");
        case IrLocReg: { obj.reg = (uint8_t) loc_value; break; }
        case IrLocStack: { obj.stack_offset = (int32_t ) loc_value; break; }
        case IrLocMem: { obj.reg = loc_value; break; }
    }
    return obj;
}

bool instr_stores_local_result(IrInstr instr) {
    switch (instr.type) {
        case IrAlloc: case IrCall: case IrUnOp: case IrBinOp: case IrCast:
            return true;
        // these instructions don't have a result stored in locals 
        case IrAssign: case IrBranch: case IrCondBranch: case IrReturn:
            return false;
    }
    return false;
}

size_t next_align_to(size_t start_addr, uint32_t alignment) {
    size_t extra = start_addr % alignment;
    return start_addr + (alignment - extra);
}

typedef enum {
    PatchAddressOf,
    PatchJmp2Instr,
    PatchJmp2Epilogue,
    PatchCall,
} PatchType;
extern const char* patch_type2str[4];
typedef struct {
    PatchType type;

    // note: for the PatchJmp2*/PatchCall types of patch the `offset` field
    // is not the start of the jmp/call instruction, but the start of the
    // imm32 that represent the relative offset.
    size_t offset; // where in the buffer to patch
    bool write_in_data_buffer; 

    uint8_t* fn_name; // for PatchCall

    size_t instr_idx; // for Jmp2Instr

    IrObjRef ref;
    size_t fn_idx; // which scope, in case ref.type is IrRefLocal
} PatchInfo;

typedef struct {
    IrInfo* info;
    IrFunction* fn;
    size_t instr_idx; // index in fn->instrs 

    Buffer* buf;
    List* patches;

    DebugInfo* dbg_info;
} PassContext;

void call_for_every_instr(void (*callback)(PassContext*, IrInstr*), IrInfo* info) {
    PassContext context = { .info = info, };
    for (size_t i = 0; i < info->functions.length; i++) {
        IrFunction* fn = &at_index(&info->functions, i, IrFunction);
        context.fn = fn;
        for (size_t j = 0; j < fn->instrs.length; j++) {
            IrInstr* instr = &at_index(&fn->instrs, j, IrInstr);
            context.instr_idx = j;
            callback(&context, instr);
        }
    }
}

bool needs_cast(IrTypeInfo src, IrTypeInfo dst) {
    debug_assert(dst.is_struct == false, "stub\n");
    if (src.is_ptr && dst.is_ptr) return false;
    return (runtime_type_info_bits(src) != runtime_type_info_bits(dst));
}

void change_obj_type_if_needed(IrObj* obj, IrTypeInfo target_type) {
    if (obj->type != IrLiteral) return;
    if (!needs_cast(obj->type_info, target_type)) return;
    obj->type_info.bit_size = target_type.bit_size;
}

void write_x64_push_reg(Buffer* buf, uint8_t reg, uint8_t reg_bits) {
    OperandType src = { .type = Register, .reg = reg };
    IndexingInfo idx = {0};
    write_x64_op1(buf, OpPush, src, reg_bits, idx);
}

void write_x64_pop_reg(Buffer* buf, uint8_t reg, uint8_t reg_bits) {
    OperandType dst = { .type = Register, .reg = reg };
    IndexingInfo idx = {0};
    write_x64_op1(buf, OpPop, dst, reg_bits, idx);
}

void write_x64_int_lit_into_reg(Buffer* buf, uint64_t lit, uint8_t reg, uint8_t reg_bits) {
    // TODO: change imm size according to smallest value if could be and if there is 
    // an instruction that could do that mov

    if (reg_bits == 64) {
        write_x64_movabs2reg(buf, lit, reg);
    }
    else {
        OperandType src = { .type = Imm, .imm_size = reg_bits / 8, .imm = lit };
        OperandType dst = { .type = Register, .reg = reg };
        IndexingInfo idx = {0};
        write_x64_op2(buf, OpMov, src, dst, reg_bits, idx);
    }
}

void write_x64_reg_into_reg(Buffer* buf, uint8_t src_reg, uint8_t dst_reg, uint8_t reg_bits) {
    OperandType src = { .type = Register, .reg = src_reg };
    OperandType dst = { .type = Register, .reg = dst_reg };
    IndexingInfo idx = {0};
    write_x64_op2(buf, OpMov, src, dst, reg_bits, idx);
}

// return the address where we need to patch later
size_t write_x64_int_lit_into_reg_stub(Buffer* buf, uint8_t reg, uint8_t reg_bits) {
    // note: we're not calling write_x64_int_lit_into_reg because that might change
    // later, for optimizing instruction size given an int literal that could fit
    // in less bytes than the register passed in

    if (reg_bits == 64) {
        write_x64_movabs2reg(buf, 0, reg);
        return buf->pos - 8;
    }
    else {
        OperandType src = { .type = Imm, .imm_size = reg_bits / 8, .imm = 0 };
        OperandType dst = { .type = Register, .reg = reg };
        IndexingInfo idx = {0};
        write_x64_op2(buf, OpMov, src, dst, reg_bits, idx);
        return buf->pos - (reg_bits / 8);
    }
}

PatchInfo ir_call_stub(Buffer* buf, uint8_t* name) {
    write_u8(buf, 0xe8); // call rel32 opcode
    size_t stub_offset = buf->pos;
    buf->pos += 4;
    return (PatchInfo) {
        .type = PatchCall,
        .fn_name = name,
        .offset = stub_offset,
    };
}

PatchInfo ir_ret_stub(Buffer* buf) {
    write_u8(buf, 0xe9);
    size_t stub_offset = buf->pos;
    buf->pos += 4;
    return (PatchInfo) {
        .type = PatchJmp2Epilogue,
        .offset = stub_offset,
    };
}

PatchInfo ir_jmp_stub(Buffer* buf, size_t instr_idx) {
    PatchInfo patch = ir_ret_stub(buf);
    patch.type = PatchJmp2Instr;
    patch.instr_idx = instr_idx;
    return patch;
}

PatchInfo ir_jmp_cond_stub(Buffer* buf, size_t instr_idx) {
    write_u8(buf, 0x0f);
    write_u8(buf, 0x84);
    size_t stub_offset = buf->pos;
    buf->pos += 4;
    return (PatchInfo) {
        .type = PatchJmp2Instr,
        .offset = stub_offset,
        .instr_idx = instr_idx,
    };
}

typedef struct {
    uint8_t reg;
    bool was_in_use;
} RegAllocInfo;
typedef struct {
    List allocations; // list of RegAllocInfo
} RegAllocState;

void init_reg_alloc_state(RegAllocState* reg_alloc_state) {
    reg_alloc_state->allocations = create_list(RegAllocInfo);
}

void reg_alloc_state_cleanup(PassContext* context, RegAllocState* reg_alloc_state);
uint8_t alloc_tmp_reg(PassContext* context, RegAllocState* reg_alloc_state);
void alloc_force_reg(PassContext* context, RegAllocState* reg_alloc_state, uint8_t reg);

OperandType op_from_ir_obj(PassContext* context, RegAllocState* reg_alloc_state, IrObj obj, IndexingInfo* idx);
void ir_mov_objs(PassContext* context, IrObj src, IrObj dst);
void ir_mov_obj_to_reg(PassContext* context, IrObj src, uint8_t reg, uint8_t bit_size);
void ir_mov_reg_to_obj(PassContext* context, uint8_t reg, uint8_t bit_size, IrObj dst);

// some instructions always use the same the registers (e.g. rax/rdx for div/mul, syscall args, etc)
// so we would like to allocate all other registers first before using these.
const uint8_t preferred_reg_alloc_order[14] = {
    // first we use registers that aren't really used for anything in particular
    Reg_rbx, Reg_r12, Reg_r13, Reg_r14,
    // calling a syscall with 6 arguments is a lot less likely then just 2 or 3 so the
    // order of these registers is the inverse of the argument order
    Reg_r9, Reg_r8, Reg_r10, Reg_rdx, Reg_rsi, Reg_rdi,
    Reg_rcx, Reg_r11, // gets clobbered by syscalls
    Reg_rax, // used for syscall number and return value, and div/mul
    Reg_r15, // return value of Cyb functions
};

void write_x64_clean_reg(Buffer* buf, uint8_t reg) {
    // xor is the best way to zero out any register. see: https://stackoverflow.com/a/33668295
    IndexingInfo dummy_idx = {0};
    OperandType reg_op = { .type = Register, .reg = reg };
    write_x64_op2(buf, OpXor, reg_op, reg_op, 32, dummy_idx);
}

uint8_t allocate_reg(PassContext* context, bool* success) {
    IrFunction* fn = context->fn;
    for (size_t i = 0; i < 14; i++) {
        uint8_t reg = preferred_reg_alloc_order[i];
        if (!fn->reg_in_use[reg]) {
            if (fn->reg_dirty[reg]) write_x64_clean_reg(context->buf, reg);
            fn->reg_in_use[reg] = true;
            fn->reg_dirty[reg] = true;
            *success = true;
            return reg;
        }
    }
    *success = false;
    return 0;
}

void allocate_stack(PassContext* context, IrObj* obj) {
    context->fn->stack_space += type_info_bits(obj->type_info) / 8;
    obj->loc = IrLocStack;
    obj->stack_offset = -context->fn->stack_space;

    RegAllocState reg_alloc_state = {0};

    // array objects actually hold a pointer to the start of the array
    if (obj->type_info.is_array) {
        context->fn->stack_space += 8;
        int32_t array_stack_offset = obj->stack_offset;
        obj->stack_offset = -context->fn->stack_space;

        ir_mov_reg_to_obj(context, Reg_rbp, 64, *obj);
        // correct for stack offset of the array itself
        OperandType op_offset = { .type = Imm, .imm = array_stack_offset, .imm_size = 4 };
        IndexingInfo idx = {0};
        OperandType op = op_from_ir_obj(context, &reg_alloc_state, *obj, &idx);
        write_x64_op2(context->buf, OpAdd, op_offset, op, 64, idx);
    }

    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void allocate_any(PassContext* context, IrObj* obj) {
    if (obj->type_info.is_struct) allocate_stack(context, obj);

    bool reg_alloc_success = false;
    uint8_t reg = allocate_reg(context, &reg_alloc_success);
    if (reg_alloc_success) {
        obj->loc = IrLocReg;
        obj->reg = reg;
    }
    else {
        allocate_stack(context, obj);
    }
}

void deallocate_reg(PassContext* context, uint8_t reg) {
    context->fn->reg_in_use[reg] = false;
}

void deallocate_any(PassContext* context, IrObj* obj) {
    if (obj->loc == IrLocReg) deallocate_reg(context, obj->reg);
    obj->loc = IrLocNone;
}

void reg_alloc_state_cleanup(PassContext* context, RegAllocState* reg_alloc_state) {
    // if allocations list wasn't even initialized, no need to do anything
    if (reg_alloc_state->allocations.size_of_elem == 0) return;

    for (size_t i = reg_alloc_state->allocations.length; i > 0; i--) {
        RegAllocInfo reg_info = at_index(&reg_alloc_state->allocations, i - 1, RegAllocInfo);
        if (reg_info.was_in_use) write_x64_pop_reg(context->buf, reg_info.reg, 64);
        deallocate_reg(context, reg_info.reg);
    }
}

uint8_t alloc_tmp_reg(PassContext* context, RegAllocState* reg_alloc_state) {
    bool alloc_success = false;
    uint8_t reg = allocate_reg(context, &alloc_success);

    // no registers free, use r14 (but save it first)
    if (!alloc_success) {
        reg = Reg_r14;
        write_x64_push_reg(context->buf, reg, 64);
    }

    // make sure list has been initialized
    if (reg_alloc_state->allocations.size_of_elem == 0) init_reg_alloc_state(reg_alloc_state);

    RegAllocInfo alloc_info = { .reg = reg, .was_in_use = !alloc_success, };
    append(&reg_alloc_state->allocations, alloc_info);

    return reg;
}

void alloc_force_reg(PassContext* context, RegAllocState* reg_alloc_state, uint8_t reg) {
    bool in_use = context->fn->reg_in_use[reg];
    if (in_use) write_x64_push_reg(context->buf, reg, 64);

    // make sure list has been initialized
    if (reg_alloc_state->allocations.size_of_elem == 0) init_reg_alloc_state(reg_alloc_state);

    RegAllocInfo alloc_info = { .reg = reg, .was_in_use = in_use };
    append(&reg_alloc_state->allocations, alloc_info);
}

IrObj* obj_ptr_from_ref(PassContext* context, IrObjRef ref) {
    List* obj_list = NULL;
    switch (ref.type) {
        case IrRefData: obj_list = &context->info->data_objs; break;
        case IrRefGlobal: obj_list = &context->info->global_objs; break;
        case IrRefLocal: obj_list = &context->fn->local_objs; break;
    }
    return &at_index(obj_list, ref.idx, IrObj);
}

static IrObj obj_from_ref(PassContext* context, IrObjRef ref) {
    return *obj_ptr_from_ref(context, ref);
}

OperandType op_from_index_of(PassContext* context, RegAllocState* reg_alloc_state, IrObj obj, IndexingInfo* idx) {
    debug_assert(obj.type == IrIndexOf, "\n");

    uint32_t bit_size = type_info_bits(obj.type_info);
    debug_assert(bit_size <= 64, "need to premultiply the index registers\n");
    uint32_t byte_size = bit_size / 8;

    IrObj array_obj = obj_from_ref(context, obj.ref);
    uint8_t ptr_reg = alloc_tmp_reg(context, reg_alloc_state);

    // move ptr to a new register
    if (array_obj.loc == IrLocMem || array_obj.loc == IrLocStack) {
        ir_mov_obj_to_reg(context, array_obj, ptr_reg, 64);
    }
    else {
        debug_print("stub: %s\n", ir_obj_loc2str[array_obj.loc]);
    }
    OperandType op = { .type = Memory, .reg = ptr_reg, };

    if (obj.index_is_const) {
        idx->use_simple = true;
        idx->disp = obj.const_index * byte_size;
        idx->disp_size = (idx->disp <= 0xff) ? 1 : 4;
        return op;
    }

    // make sure index is in a registers
    IrObj index_obj = obj_from_ref(context, obj.index_ref);
    uint8_t index_reg = 0;
    if (index_obj.loc == IrLocReg) { index_reg = index_obj.reg; }
    else {
        index_reg = alloc_tmp_reg(context, reg_alloc_state);
        ir_mov_obj_to_reg(context, index_obj, index_reg, type_info_bits(index_obj.type_info));
    }

    idx->use_complex = true;
    idx->index = index_reg;
    idx->disp_size = 0;
    if (bit_size == 64) idx->scale = 3;
    else if (bit_size == 32) idx->scale = 2;
    else if (bit_size == 16) idx->scale = 1;
    else if (bit_size == 8) idx->scale = 0;

    return op;
}

OperandType op_from_member_of(PassContext* context, RegAllocState* reg_alloc_state, IrObj obj, IndexingInfo* idx) {
    IrObj struct_obj = obj_from_ref(context, obj.ref);
    IrTypeInfo struct_type = struct_obj.type_info;
    debug_assert(struct_type.is_struct, "\n");

    debug_assert(struct_obj.loc == IrLocStack, "\n");
    int32_t byte_disp = struct_obj.stack_offset;
    for (size_t i = 0; i < obj.member_idx; i++) {
        IrTypeInfo member_type = at_index(&struct_type.members, i, IrTypeInfo);
        byte_disp += runtime_type_info_bits(member_type) / 8;
    }

    OperandType op = { .type = Memory, .reg = Reg_rbp };
    idx->use_simple = true;
    idx->disp = byte_disp;
    idx->disp_size = (idx->disp <= 0xff) ? 1 : 4;

    return op;
}

OperandType op_from_ir_obj(PassContext* context, RegAllocState* reg_alloc_state, IrObj obj, IndexingInfo* idx) {
    if (obj.type == IrIndexOf) {
        return op_from_index_of(context, reg_alloc_state, obj, idx);
    }
    if (obj.type == IrMemberOf) {
        return op_from_member_of(context, reg_alloc_state, obj, idx);
    }

    if (obj.type == IrLiteral) {
        return (OperandType) {
            .type = Imm, .imm = obj.int_literal,
            .imm_size = obj.type_info.bit_size / 8,
        };
    }

    if (obj.loc == IrLocReg) {
        return (OperandType) { .type = Register, .reg = obj.reg };
    }

    if (obj.loc == IrLocStack) {
        idx->use_simple = true;
        idx->disp = obj.stack_offset;
        idx->disp_size = (obj.stack_offset > 127 || obj.stack_offset < -128) ? 4 : 1;
        return (OperandType) { .type = Memory, .reg = Reg_rbp };
    }

    debug_crash("obj={.type=%s, .loc=%s}\n", ir_obj_type2str[obj.type], ir_obj_loc2str[obj.loc]);
}

void ir_mov_objs(PassContext* context, IrObj src, IrObj dst) {
    // An IrObj can be 1 of 4 'places': a register (Reg), in the stack (Stack),
    //   in memory (Mem) or an immediate value (Imm).
    // Thus to support doing an arbitrary move we have to implement 4x3=12 different
    //   scenarios, presented in table form below (having the destination be an
    //   immediate value doesn't really make sense).
    // Some of these cannot be done in a single instruction (despite the x64
    //   `mov` instruction being almost turing complete), and so require using
    //    additional temporary registers to hold addresses/values.
    //
    //             |    destination    |
    //     ^-^     +-------------------+
    //             | Reg | Stack | Mem |
    // ----+-------+-----+-------+-----+    T: requires temporary register
    //  s  |   Reg |  X  |   X   |  X  |
    //  o  |-------+-----+-------+-----+    X: already implemented
    //  u  | Stack |  X  |  T,X  | T,X |
    //  r  |-------+-----+-------+-----+    O: not yet implemented
    //  c  |   Mem |  X  |  T,X  | T,X |
    //  e  |-------+-----+-------+-----+
    //     |   Imm |  X  |   X   | T,X |
    // ----+-------+-----+-------+-----+
    // (if the source Imm is 64 bits we also need a temporary register, if the destination is
    // Stack or Mem, because the `movabs` instruction only works on registers)
    // (movs Mem->Reg or Reg->Mem can be done using the DS segment register, and so don't need
    // to use any temporary registers)
    debug_assert(dst.type != IrLiteral, "\n");

    // TODO: IrAddrStub should work here, but I have to think about it
    debug_assert(dst.type != IrAddrStub, "\n");

    if (src.type == IrVarRef) src = obj_from_ref(context, src.ref);
    if (dst.type == IrVarRef) dst = obj_from_ref(context, dst.ref);

    if(runtime_type_info_bits(src.type_info) != runtime_type_info_bits(dst.type_info)) {
        debug_crash("stub: need to introduce IrCast instruction\n");
    }
    uint32_t bit_size = runtime_type_info_bits(dst.type_info);

    // for things that don't have the neat registers size we want (like structs)
    // we need to do multiple smaller movs
    if (bit_size != 64 && bit_size != 32 && bit_size != 16 && bit_size != 8) {
        debug_assert(src.loc == IrLocStack && dst.loc == IrLocStack, "\n");
        uint32_t bits_left = bit_size;
        while (bits_left > 0) {
            uint32_t fake_bit_size = 8;
            if (bits_left > 64) fake_bit_size = 64;  
            else if (bits_left > 32) fake_bit_size = 32;  

            IrTypeInfo fake_type = { .bit_size = fake_bit_size };
            uint32_t bytes_done = (bit_size - bits_left) / 8; 
            IrObj fake_src = dummy_var(fake_type, IrLocStack, src.stack_offset + bytes_done);
            IrObj fake_dst = dummy_var(fake_type, IrLocStack, dst.stack_offset + bytes_done);
            ir_mov_objs(context, fake_src, fake_dst);

            bits_left -= fake_bit_size;
        }
        return;
    }

    RegAllocState reg_alloc_state = {0};

    if (src.type == IrAddrStub) {
        uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
        PatchInfo addr_stub_patch = {
            .type = PatchAddressOf,
            .offset = write_x64_int_lit_into_reg_stub(context->buf, tmp_reg, 64),
            .ref = src.ref,
        };
        append(context->patches, addr_stub_patch);
        src = dummy_var(pointer_to_type(src.type_info), IrLocReg, tmp_reg);
    }

    // TODO: if src and dst are identical the mov is a no-op (?? might still want to do the mov
    // for other reasons, like flags???, is this a valid use case???)

    // take care of all the ones that tmp reg before. i.e. do the extra move now, and then
    // continue with the reduced case
    bool tmp_register_needed =
        (src.loc == IrLocStack && dst.loc != IrLocReg)
        || (src.loc == IrLocMem && dst.loc != IrLocReg)
        || (src.type == IrIndexOf && dst.loc != IrLocReg)
        || (src.type == IrLiteral && dst.loc == IrLocMem)
        || (dst.type == IrIndexOf && !(src.loc == IrLocReg || src.type == IrLiteral))
        || (src.type == IrIndexOf && dst.type == IrIndexOf);
    if (tmp_register_needed) {
        uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
        IrObj new_src = dummy_var(src.type_info, IrLocReg, tmp_reg);
        ir_mov_objs(context, src, new_src);
        src = new_src;
    }
    if (src.loc == IrLocMem) {
        debug_assert(dst.loc == IrLocReg, "stub\n");
        debug_assert(src.buf_offset + data_vaddr < 0x100000000, "stub\n");
        write_x64_mov_from_ds_32(context->buf, src.buf_offset + data_vaddr, dst.reg, bit_size);
        return;
    }
    if (dst.loc == IrLocMem) {
        debug_assert(src.loc == IrLocReg, "stub\n");
        debug_assert(dst.buf_offset + data_vaddr < 0x100000000, "stub\n");
        write_x64_mov_to_ds_32(context->buf, dst.buf_offset + data_vaddr, src.reg, bit_size);
        return;
    }
    if (src.type == IrLiteral && src.literal_type == IrLiteralInt && src.type_info.bit_size == 64
        && dst.loc == IrLocStack) {
        // TODO: if we actually need movabs then we need to use a tmp register
        debug_assert(src.int_literal < 0x100000000, "stub\n");
        debug_assert(!src.type_info.is_signed, "stub: maybe do a movzx\n");
        src.type_info.bit_size = 32;
    }

    OperandType src_op = {0};
    OperandType dst_op = {0};
    IndexingInfo idx = {0};

    src_op = op_from_ir_obj(context, &reg_alloc_state, src, &idx);
    dst_op = op_from_ir_obj(context, &reg_alloc_state, dst, &idx);

    if (src_op.type == Imm && src_op.imm_size == 8) {
        debug_assert(dst_op.type == Register, "\n");
        write_x64_movabs2reg(context->buf, src_op.imm, dst.reg);
    }
    else {
        write_x64_op2(context->buf, OpMov, src_op, dst_op, bit_size, idx);
    }

    // restore any temporary registers that were used
    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_mov_obj_to_reg(PassContext* context, IrObj src, uint8_t reg, uint8_t bit_size) {
    IrTypeInfo dummy_type = { .bit_size = bit_size };
    IrObj dst_obj = dummy_var(dummy_type, IrLocReg, reg);
    ir_mov_objs(context, src, dst_obj);
}

void ir_mov_reg_to_obj(PassContext* context, uint8_t reg, uint8_t bit_size, IrObj dst) {
    IrTypeInfo dummy_type = { .bit_size = bit_size };
    IrObj src_obj = dummy_var(dummy_type, IrLocReg, reg);
    ir_mov_objs(context, src_obj, dst);
}

void ir_load_effective_addr(PassContext* context, IrObj src, IrObj dst) {
    debug_assert(src.type == IrIndexOf, "stub: %s\n", ir_obj_type2str[src.type]);

    RegAllocState reg_alloc_state = {0};

    IrObj lea_dst_obj = dst;
    if(dst.loc != IrLocReg) {
        uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
        lea_dst_obj = dummy_var(dst.type_info, IrLocReg, tmp_reg);
    }

    IndexingInfo idx = {0};
    OperandType src_op = op_from_index_of(context, &reg_alloc_state, src, &idx);
    OperandType dst_op = op_from_ir_obj(context, &reg_alloc_state, lea_dst_obj, &idx);

    write_x64_op2(context->buf, OpLea, src_op, dst_op, 64, idx);

    if (dst.loc != IrLocReg) ir_mov_objs(context, lea_dst_obj, dst); 

    // restore any temporary registers that were used
    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_instr_alloc(PassContext* context, IrInstr* instr) {
    IrObj* obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_stack(context, obj);
}

void ir_write_call_syscall(PassContext* context, IrInstr* instr) {
    IrFunction* fn = context->fn;
    Buffer* buf = context->buf;
    List* patches = context->patches;
    uint8_t syscall_arg_registers[] = {
        Reg_rdi, Reg_rsi, Reg_rdx, Reg_r10, Reg_r9, Reg_r8
    };

    for (size_t i = 0; i < instr->args.length; i++) {
        IrObj arg = at_index(&instr->args, i, IrObj);
        IrTypeInfo type = at_index(&instr->callee_arg_types, i, IrTypeInfo);
        uint32_t type_bits = runtime_type_info_bits(type);
        uint8_t reg = syscall_arg_registers[i];

        switch (arg.type) {
            case IrLiteral: {
                debug_assert(arg.literal_type == IrLiteralInt, "stub\n");
                ir_mov_obj_to_reg(context, arg, reg, type_bits);
                break;
            }
            case IrAddrStub: {
                PatchInfo stub_patch = {
                    .offset = write_x64_int_lit_into_reg_stub(buf, reg, type_bits),
                    .type = PatchAddressOf,
                    .ref = arg.ref,
                };
                append(patches, stub_patch);
                break;
            }

            case IrVar: case IrVarRef: {
                ir_mov_obj_to_reg(context, arg, reg, type_bits);
                break;
            }

            default:
                debug_crash("stub: %s\n", ir_obj_type2str[arg.type]);
        }

        fn->reg_in_use[reg] = true;
        fn->reg_dirty[reg] = true;
    }

    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_any(context, result_obj);

    // rcx and r11 get clobbered inside syscalls
    if (fn->reg_in_use[Reg_rcx]) write_x64_push_reg(buf, Reg_rcx, 64);
    if (fn->reg_in_use[Reg_r11]) write_x64_push_reg(buf, Reg_r11, 64);

    if (fn->reg_in_use[Reg_rax]) write_x64_push_reg(buf, Reg_rax, 64);
    write_x64_int_lit_into_reg(buf, instr->syscall_code, Reg_rax, 32);

    write_x64_op0(buf, OpSyscall);

    ir_mov_reg_to_obj(context, Reg_rax, result_obj->type_info.bit_size, *result_obj);
    if (fn->reg_in_use[Reg_rax]) write_x64_pop_reg(buf, Reg_rax, 64);

    if (fn->reg_in_use[Reg_r11]) write_x64_pop_reg(buf, Reg_r11, 64);
    if (fn->reg_in_use[Reg_rcx]) write_x64_pop_reg(buf, Reg_rcx, 64);

    fn->reg_dirty[Reg_rax] = true;
    fn->reg_dirty[Reg_r11] = true;
    fn->reg_dirty[Reg_rcx] = true;

    for (size_t i = 0; i < instr->args.length; i++) {
        uint8_t reg = syscall_arg_registers[i];
        fn->reg_in_use[reg] = false;
    }
}

void write_push_struct(PassContext* context, IrObj obj) {
    if (obj.type == IrVarRef) obj = obj_from_ref(context, obj.ref);

    OperandType rsp_op = { .type = Register, .reg = Reg_rsp };
    IndexingInfo dummy_idx = {0};

    RegAllocState reg_alloc_state = {0};
    uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);

    uint32_t obj_bit_size = runtime_type_info_bits(obj.type_info);

    OperandType imm_op = { .type = Imm, .imm = obj_bit_size / 8, .imm_size = 4 };
    write_x64_op2(context->buf, OpSub, imm_op, rsp_op, 64, dummy_idx);

    uint32_t bits_left = obj_bit_size;
    while (bits_left > 0) {
        uint32_t fake_bit_size = 8;
        if (bits_left >= 64) fake_bit_size = 64;
        else if (bits_left >= 32) fake_bit_size = 32;

        uint32_t bytes_done = (obj_bit_size - bits_left) / 8;
        IrTypeInfo fake_type = { .bit_size = fake_bit_size };

        IrObj fake_obj = obj;
        debug_assert(fake_obj.loc == IrLocStack, "\n");
        fake_obj.stack_offset += bytes_done;
        fake_obj.type_info = fake_type;

        ir_mov_obj_to_reg(context, fake_obj, tmp_reg, fake_bit_size);
        fake_obj = dummy_var(fake_type, IrLocReg, tmp_reg);

        IndexingInfo idx = {0};
        OperandType obj_op = op_from_ir_obj(context, &reg_alloc_state, fake_obj, &idx);
        OperandType rsp_op_mem = { .type = Memory, .reg = Reg_rsp };
        idx.use_simple = true;
        idx.disp = bytes_done;
        idx.disp_size = (idx.disp <= 0xff) ? 1 : 4;
        write_x64_op2(context->buf, OpMov, obj_op, rsp_op_mem, fake_bit_size, idx);

        bits_left -= fake_bit_size;
    }

    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_call_cyb(PassContext* context, IrInstr* instr) {
    // here's how the stack looks inside a Cyb function with 64 bit arguments,
    // immediatly after its prologue:
    //           0x........: <local variables>
    //           0x........: <other saved registers>
    // rsp,rbp-> 0x7fff0000: <8 bytes> saved rbp
    //           0x7fff0008: <8 bytes> saved return address from call instruction
    //           0x7fff0010: <8 bytes> 1st argument
    //           0x7fff0018: <8 bytes> 2nd argument
    //           0x........: <other arguments>
    //           0x........: <previous function local variables>
    // (the addresses used are made up)

    OperandType rsp_op = { .type = Register, .reg = Reg_rsp };
    IndexingInfo dummy_idx = {0};

    RegAllocState reg_alloc_state = {0};

    uint32_t pushed_bytes = 0;
    for (size_t rev_i = instr->args.length; rev_i > 0; rev_i--) {
        size_t i = rev_i - 1;
        IrObj obj = at_index(&instr->args, i, IrObj);
        uint32_t bit_size = runtime_type_info_bits(obj.type_info);
        uint32_t byte_size = bit_size / 8;

        if (obj.type_info.is_struct) {
            write_push_struct(context, obj);
            pushed_bytes += byte_size;
            continue;
        }

        // some things can't be pushed directly on the stack with the `push` x64
        // instruction, so we have to do the mov and decrement the stack pointer ourselves
        bool can_push = (bit_size == 64 || bit_size == 16)
            && (obj.type == IrLiteral || obj.loc == IrLocReg || obj.loc == IrLocStack);
        // `push` works with 64 bit int literals but they get pushed as a 32 bit
        // integer that gets sign extended to 64 bits
        if (obj.type == IrLiteral && bit_size == 64 && (obj.int_literal & 0xffffffff80000000)) {
            can_push = false;
        }

        if (can_push) {
            IndexingInfo idx = {0};
            OperandType obj_op = op_from_ir_obj(context, &reg_alloc_state, obj, &idx);

            if (obj.type == IrLiteral && bit_size == 64) {
                obj_op.imm_size = 4;
                bit_size = 32;
            }

            write_x64_op1(context->buf, OpPush, obj_op, bit_size, dummy_idx);
        }
        else {
            OperandType imm_op = { .type = Imm, .imm = byte_size, .imm_size = 4 };
            write_x64_op2(context->buf, OpSub, imm_op, rsp_op, 64, dummy_idx);

            if (obj.loc != IrLocReg) {
                uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
                ir_mov_obj_to_reg(context, obj, tmp_reg, bit_size);
                obj = dummy_var(obj.type_info, IrLocReg, tmp_reg);
            }

            IndexingInfo idx = {0};
            OperandType obj_op = op_from_ir_obj(context, &reg_alloc_state, obj, &idx);
            OperandType rsp_op_mem = { .type = Memory, .reg = Reg_rsp };
            write_x64_op2(context->buf, OpMov, obj_op, rsp_op_mem, bit_size, idx);
        }

        pushed_bytes += byte_size;
    }

    PatchInfo call_patch = ir_call_stub(context->buf, instr->callee_name);
    append(context->patches, call_patch);

    OperandType imm_op = { .type = Imm, .imm = pushed_bytes, .imm_size = 4 };
    write_x64_op2(context->buf, OpAdd, imm_op, rsp_op, 64, dummy_idx);

    // return value from CallConvCyb function is always in r15
    debug_assert(!context->fn->reg_in_use[Reg_r15], "have to do alloc for result_obj and a mov\n");
    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    result_obj->loc = IrLocReg;
    result_obj->reg = Reg_r15;

    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_call_cdecl(PassContext* context, IrInstr* instr) {
    IrFunction* fn = context->fn;
    Buffer* buf = context->buf;

    if (fn->reg_in_use[Reg_rax]) write_x64_push_reg(buf, Reg_rax, 64);

    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_stack(context, result_obj);

    PatchInfo call_patch = ir_call_stub(context->buf, instr->callee_name);
    append(context->patches, call_patch);

    ir_mov_reg_to_obj(context,
        Reg_rax, runtime_type_info_bits(result_obj->type_info),
        *result_obj);

    if (fn->reg_in_use[Reg_rax]) write_x64_pop_reg(buf, Reg_rax, 64);

    debug_print("TODO: CDecl arguments\n");
}

void ir_write_instr_call(PassContext* context, IrInstr* instr) {
    switch(instr->call_conv) {
        case CallConvSyscall: ir_write_call_syscall(context, instr); break;
        case CallConvCyb: ir_write_call_cyb(context, instr); break;
        case CallConvCDecl: ir_write_call_cdecl(context, instr); break;
    }
}

void ir_write_instr_return(PassContext* context, IrInstr* instr) {
    debug_assert(context->fn->call_conv == CallConvCyb, "\n");

    IrTypeInfo return_type = context->fn->return_type;
    IrObj ret_obj = instr->ret_obj;
    ir_mov_obj_to_reg(context, ret_obj, Reg_r15, return_type.bit_size);

    // do a dummy jmp to the function epilogue to patch later
    PatchInfo jmp2ret_patch = ir_ret_stub(context->buf);
    append(context->patches, jmp2ret_patch);
}

void ir_write_instr_assign(PassContext* context, IrInstr* instr) {
    IrObj* dst_obj = &instr->assign_dst_obj;
    if (dst_obj->type == IrVarRef) dst_obj = obj_ptr_from_ref(context, dst_obj->ref);

    if (dst_obj->loc == IrLocNone && dst_obj->type == IrVar) {
        allocate_stack(context, dst_obj);
    }

    ir_mov_objs(context, instr->assign_src_obj, *dst_obj);
}

void ir_write_instr_branch(PassContext* context, IrInstr* instr) {
    PatchInfo jmp = ir_jmp_stub(context->buf, instr->target_instr_idx);
    append(context->patches, jmp);
}

void ir_write_instr_cond_branch(PassContext* context, IrInstr* instr) {
    IrObj cond_obj = instr->cond_obj;  
    debug_assert(cond_obj.type == IrVarRef, "\n");
    cond_obj = obj_from_ref(context, cond_obj.ref);

    RegAllocState reg_alloc_state = {0};

    IndexingInfo idx = {0};
    OperandType op1 = { .type = Imm, .imm = 0, .imm_size = 1, };
    OperandType op2 = op_from_ir_obj(context, &reg_alloc_state, cond_obj, &idx);
    write_x64_op2(context->buf, OpCmp, op1, op2, cond_obj.type_info.bit_size, idx);

    PatchInfo cond_jmp = ir_jmp_cond_stub(context->buf, instr->target_instr_idx);
    append(context->patches, cond_jmp);

    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_instr_un_op(PassContext* context, IrInstr* instr) {
    IrObj op_obj = instr->un_op_obj;
    if (op_obj.type == IrVarRef) op_obj = obj_from_ref(context, op_obj.ref);

    uint32_t bit_size = op_obj.type_info.bit_size;
    UnOpType op_type = instr->un_op_type;

    RegAllocState reg_alloc_state = {0};

    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_stack(context, result_obj);

    IndexingInfo idx = {0};
    OperandType op = op_from_ir_obj(context, &reg_alloc_state, *result_obj, &idx);

    if (op_type == UnOpLogicalNot) {
        // mov <result>, 0
        // cmp <operand>, 0
        // sete <result>
        OperandType zero_op = { .type = Imm, .imm = 0, .imm_size = 1 };
        IndexingInfo operand_idx = {0};
        OperandType operand_op = op_from_ir_obj(context, &reg_alloc_state, op_obj, &operand_idx);
        write_x64_op2(context->buf, OpMov, zero_op, op, bit_size, idx);
        write_x64_op2(context->buf, OpCmp, zero_op, operand_op, bit_size, operand_idx);
        write_x64_op1(context->buf, OpSetEql, op, bit_size, idx);
        return;
    }

    if (op_type == UnOpAddr) {
        ir_load_effective_addr(context, op_obj, *result_obj);
        return;
    }

    ir_mov_objs(context, instr->un_op_obj, *result_obj);

    OpMnemonic op_name = 0;
    if (op_type == UnOpInc) op_name = OpInc;
    else if (op_type == UnOpNeg) op_name = OpNeg;
    else debug_crash("stub: %s\n", un_op_type2str[op_type]);
    write_x64_op1(context->buf, op_name, op, bit_size, idx);

    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_instr_bin_op(PassContext* context, IrInstr* instr) {
    IrObj left = instr->left_obj;
    IrObj right = instr->right_obj;
    uint32_t bit_size = left.type_info.bit_size;
    BinOpType op_type = instr->bin_op_type;
    bool any_signed = (left.type_info.is_signed || right.type_info.is_signed); 

    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_stack(context, result_obj);

    RegAllocState reg_alloc_state = {0};

    if (op_type == BinOpPlus || op_type == BinOpMinus) {
        ir_mov_objs(context, left, *result_obj);

        if ((right.type != IrLiteral && right.loc != IrLocReg)
            || (right.type == IrLiteral && right.type_info.bit_size == 64)) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            IrObj new_right = dummy_var(right.type_info, IrLocReg, tmp_reg);
            ir_mov_objs(context, right, new_right);
            right = new_right;
        }

        IndexingInfo idx = {0};
        OperandType dst_op = op_from_ir_obj(context, &reg_alloc_state, *result_obj, &idx);
        OperandType right_op = op_from_ir_obj(context, &reg_alloc_state, right, &idx);
        OpMnemonic op_name = 0;
        if (op_type == BinOpPlus) op_name = OpAdd;
        else if (op_type == BinOpMinus) op_name = OpSub;
        else debug_crash("stub: %s\n", bin_op_type2str[op_type]);
        write_x64_op2(context->buf, op_name, right_op, dst_op, bit_size, idx);
    }
    else if (op_type == BinOpMod || op_type == BinOpMul || op_type == BinOpDiv) {
        alloc_force_reg(context, &reg_alloc_state, Reg_rax);
        // even tho we might not need to use it, rdx still gets written to by the mul/div
        // instruction; this ensures it gets saved/restored if it was being used
        alloc_force_reg(context, &reg_alloc_state, Reg_rdx);

        ir_mov_obj_to_reg(context, left, Reg_rax, bit_size);
        write_x64_clean_reg(context->buf, Reg_rdx);

        // mul/div don't support immediate operands
        if (right.type == IrLiteral || right.type_info.bit_size != bit_size) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            ir_mov_obj_to_reg(context, right, tmp_reg, right.type_info.bit_size);
            right = dummy_var(right.type_info, IrLocReg, tmp_reg);
        }

        if (right.type == IrVarRef) right = obj_from_ref(context, right.ref);

        // if the operand size is 8 bit the reminder gets put in AH
        // which is the high byte of the 16 bit AL register. currently
        // our codegen stuff does not support addressing these "high bit"
        // registers, so force a 16 bit operation instead.
        uint32_t op_bit_size = (bit_size == 8) ? 16 : bit_size;

        IndexingInfo idx = {0};
        OperandType right_op = op_from_ir_obj(context, &reg_alloc_state, right, &idx);
        OpMnemonic op_name = (op_type == BinOpMul) ? OpMul : OpDiv;
        write_x64_op1(context->buf, op_name, right_op, op_bit_size, idx);

        uint8_t op_result_reg = (op_type == BinOpMod) ? Reg_rdx : Reg_rax;
        uint32_t result_bit_size = runtime_type_info_bits(result_obj->type_info);
        ir_mov_reg_to_obj(context, op_result_reg, result_bit_size, *result_obj);
    }
    else if (op_type == BinOpLess || op_type == BinOpLessEql || op_type == BinOpGreater
        || op_type == BinOpGreaterEql || op_type == BinOpEql || op_type == BinOpNotEql) {
        if (left.type == IrVarRef) {
            debug_assert(left.ref.type == IrRefLocal, "\n");
            left = at_index(&context->fn->local_objs, left.ref.idx, IrObj);
        }
        if (right.type == IrVarRef) {
            debug_assert(right.ref.type == IrRefLocal, "\n");
            right = at_index(&context->fn->local_objs, right.ref.idx, IrObj);
        }

        if (left.type == IrLiteral) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            ir_mov_obj_to_reg(context, left, tmp_reg, left.type_info.bit_size);
            left = dummy_var(left.type_info, IrLocReg, tmp_reg);
        }

        if ((left.loc != IrLocReg && right.loc != IrLocReg) || (left.type == IrLiteral && right.type == IrLiteral)) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            ir_mov_obj_to_reg(context, right, tmp_reg, right.type_info.bit_size);
            right = dummy_var(right.type_info, IrLocReg, tmp_reg);
        }

        // TODO: is this really necessary?
        if (left.type == IrIndexOf) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            ir_mov_obj_to_reg(context, left, tmp_reg, left.type_info.bit_size);
            left = dummy_var(left.type_info, IrLocReg, tmp_reg);
        }
        if (right.type == IrIndexOf) {
            uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
            ir_mov_obj_to_reg(context, right, tmp_reg, right.type_info.bit_size);
            right = dummy_var(right.type_info, IrLocReg, tmp_reg);
        }

        // do the comparision
        IndexingInfo idx = {0};
        OperandType op1 = op_from_ir_obj(context, &reg_alloc_state, left, &idx);
        OperandType op2 = op_from_ir_obj(context, &reg_alloc_state, right, &idx);
        debug_assert(op1.type != Imm, "\n");
        write_x64_op2(context->buf, OpCmp, op2, op1, left.type_info.bit_size, idx);

        // see the bool result
        OperandType op = op_from_ir_obj(context, &reg_alloc_state, *result_obj, &idx);
        OpMnemonic op_name = 0;
        if (op_type == BinOpLess) op_name = any_signed ? OpSetLess : OpSetBlw;
        else if (op_type == BinOpLessEql) op_name = OpSetBlwEql;
        else if (op_type == BinOpGreater) op_name = OpSetAbv;
        else if (op_type == BinOpGreaterEql) op_name = OpSetNLess;
        else if (op_type == BinOpEql) op_name = OpSetEql;
        else if (op_type == BinOpNotEql) op_name = OpSetNEql;
        else debug_crash("stub: %s\n", bin_op_type2str[op_type]);
        write_x64_op1(context->buf, op_name, op, 8, idx);
    }
    else if (op_type == BinOpLogicalOr || op_type == BinOpLogicalAnd) {
        // what we're doing bellow is essentially this assembly:
        // mov <tmp_reg>, 0
        // cmp <left>, 0
        // cmovne <tmp_reg>, <left> 
        // cmp <right>, 0
        // cmovne <tmp_reg>, <right>
        // (for logical AND: <tmp_reg> starts with 1, and mov's are `cmove`)

        // conditional moves only support register destinations
        uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);
        OperandType tmp_op = { .type = Register, .reg = tmp_reg };

        if (left.type == IrVarRef) left = obj_from_ref(context, left.ref);
        if (right.type == IrVarRef) right = obj_from_ref(context, right.ref);

        IndexingInfo left_idx = {0};
        OperandType left_op = op_from_ir_obj(context, &reg_alloc_state, left, &left_idx);
        IndexingInfo right_idx = {0};
        OperandType right_op = op_from_ir_obj(context, &reg_alloc_state, right, &right_idx);

        IndexingInfo dummy_idx = {0};
        OperandType zero_op = { .type = Imm, .imm = 0, .imm_size = 1 };
        OperandType one_op = { .type = Imm, .imm = 1, .imm_size = 1 };

        OpMnemonic cmov_op_name = 0; 
        if (op_type == BinOpLogicalOr) cmov_op_name = OpMovNEql;
        else if (op_type == BinOpLogicalAnd) cmov_op_name = OpMovEql;
        else debug_crash("stub: %s\n", bin_op_type2str[op_type]);

        OperandType start_value = (op_type == BinOpLogicalOr) ? zero_op : one_op;

        write_x64_op2(context->buf, OpMov, start_value, tmp_op, bit_size, dummy_idx);
        write_x64_op2(context->buf, OpCmp, zero_op, left_op, bit_size, left_idx);
        write_x64_op2(context->buf, cmov_op_name, left_op, tmp_op, bit_size, left_idx);
        write_x64_op2(context->buf, OpCmp, zero_op, right_op, bit_size, right_idx);
        write_x64_op2(context->buf, cmov_op_name, right_op, tmp_op, bit_size, right_idx);

        ir_mov_reg_to_obj(context, tmp_reg, bit_size, *result_obj);
    }
    else {
        debug_crash("stub: %s\n", bin_op_type2str[op_type]);
    }

    // restore any temporary registers that were used
    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_instr_cast(PassContext* context, IrInstr* instr) {
    IrObj* result_obj = &at_index(&context->fn->local_objs, instr->local_idx, IrObj);
    allocate_stack(context, result_obj);

    IrObj src_obj = obj_from_ref(context, instr->cast_src_ref);
    IrTypeInfo src_type = src_obj.type_info;
    uint32_t src_type_bits = runtime_type_info_bits(src_type);
    IrTypeInfo dst_type = instr->cast_dst_type;
    uint32_t dst_type_bits = runtime_type_info_bits(dst_type);
    bool any_signed = (src_type.is_signed || dst_type.is_signed);

    if (src_type_bits == dst_type_bits) {
        ir_mov_objs(context, src_obj, *result_obj);
        return;
    }

    // `movsx` and `movzx` both require destination to be a register
    RegAllocState reg_alloc_state = {0};
    uint8_t tmp_reg = alloc_tmp_reg(context, &reg_alloc_state);

    IndexingInfo idx = {0};
    OperandType src_op = op_from_ir_obj(context, &reg_alloc_state, src_obj, &idx);
    if (any_signed) {
        write_x64_movsx(context->buf, src_op, src_type_bits, tmp_reg, dst_type_bits, idx);
    }
    else {
        write_x64_movzx(context->buf, src_op, src_type_bits, tmp_reg, dst_type_bits, idx);
    }

    ir_mov_reg_to_obj(context, tmp_reg, dst_type_bits, *result_obj);

    // restore any temporary registers that were used
    reg_alloc_state_cleanup(context, &reg_alloc_state);
}

void ir_write_instr(PassContext* context, IrInstr* instr) {
    //debug_print("%s @ fn '%s', idx=%lu\n",
    //    ir_instr_type2str[instr->type], context->fn->name, context->instr_idx);

    // debug line information
    static SourceLocation last_src = {0};
    if (instr->src_loc.line > last_src.line || instr->src_loc.file != last_src.file) {
        LineAddr line_addr = { .src_loc = instr->src_loc, .addr = context->buf->pos };
        append(&context->dbg_info->line_addrs, line_addr);
        last_src = instr->src_loc;
    }

    switch (instr->type) {
        case IrAlloc: ir_write_instr_alloc(context, instr); break;
        case IrCall: ir_write_instr_call(context, instr); break;
        case IrReturn: ir_write_instr_return(context, instr); break;
        case IrAssign: ir_write_instr_assign(context, instr); break;
        case IrBranch: ir_write_instr_branch(context, instr); break;
        case IrCondBranch: ir_write_instr_cond_branch(context, instr); break;
        case IrUnOp: ir_write_instr_un_op(context, instr); break;
        case IrBinOp: ir_write_instr_bin_op(context, instr); break;
        case IrCast: ir_write_instr_cast(context, instr); break;
    
        default: debug_crash("stub: %s\n", ir_instr_type2str[instr->type]);
    }
}

void ir_write_fn_body(IrFunction* ir_fn, Buffer* buf, List* patches, IrInfo* ir_info, DebugInfo* dbg_info) {
    PassContext context = {
        .info = ir_info,
        .fn = ir_fn,
        .buf = buf,
        .patches = patches,
        .dbg_info = dbg_info,
    };
    for (size_t i = 0; i < ir_fn->instrs.length; i++) {
        IrInstr* instr = &at_index(&ir_fn->instrs, i, IrInstr);
        context.instr_idx = i;
        at_index(&ir_fn->instr_offsets, i, size_t) = buf->pos;
        ir_write_instr(&context, instr);
    }
}

void ir_write_fn_entry_and_exit(IrFunction* ir_fn, Buffer* buf, List* patches, IrInfo* ir_info, DebugInfo* dbg_info) {
    IndexingInfo dummy_idx = {0};
    OperandType rbp_op = { .type = Register, .reg = Reg_rbp };
    OperandType rsp_op = { .type = Register, .reg = Reg_rsp };
    OperandType stack_op = { .type = Imm, .imm = ir_fn->stack_space, .imm_size = 4 }; 

    // prologue: allocate stack, setup frame base pointer (in rbp), save used registers
    Buffer prologue_buf = new_buffer(512);
    write_x64_op1(&prologue_buf, OpPush, rbp_op, 64, dummy_idx);
    write_x64_op2(&prologue_buf, OpMov, rsp_op, rbp_op, 64, dummy_idx);
    if (ir_fn->stack_space != 0) write_x64_op2(&prologue_buf, OpSub, stack_op, rsp_op, 64, dummy_idx);
    for (size_t i = 0; i < 16; i++) {
        OperandType reg = { .type = Register, .reg = i };
        if (ir_fn->reg_dirty[i]) write_x64_op1(&prologue_buf, OpPush, reg, 64, dummy_idx);
    }

    // move prologue code into the function buffer
    buffer_insert_gap(buf, 0, prologue_buf.pos);
    buffer_write_at(buf, 0, prologue_buf.data, prologue_buf.pos);
    size_t prologue_bytes_needed = prologue_buf.pos;
    free_buffer(&prologue_buf);
    //debug_print("prologue_bytes_needed=0x%lx\n", prologue_bytes_needed);

    // everything that referenced an address in the function body is now pointing
    // to its old location in the buffer before we pushed the function body
    // forward to make space for the prologue. so let's fix that
    for (size_t i = 0; i < patches->length; i++) {
        at_index(patches, i, PatchInfo).offset += prologue_bytes_needed;
    }
    for (size_t i = 0; i < ir_fn->instr_offsets.length; i++) {
        at_index(&ir_fn->instr_offsets, i, size_t) += prologue_bytes_needed;
    }
    for (size_t i = 0; i < dbg_info->line_addrs.length; i++) {
        at_index(&dbg_info->line_addrs, i, LineAddr).addr += prologue_bytes_needed;
    }

    // epilogue
    size_t epilogue_start = buf->pos;
    for (size_t i = 16; i > 0; i--) {
        OperandType reg = { .type = Register, .reg = i - 1 };
        if (ir_fn->reg_dirty[i - 1]) write_x64_op1(buf, OpPop, reg, 64, dummy_idx);
    }
    if (ir_fn->stack_space != 0) write_x64_op2(buf, OpAdd, stack_op, rsp_op, 64, dummy_idx);
    write_x64_op1(buf, OpPop, rbp_op, 64, dummy_idx);
    if (ir_fn->return_type.bit_size == 0) write_x64_clean_reg(buf, Reg_r15);
    write_x64_op0(buf, OpRet);

    // patch all the return and branch jumps and remove them from the list
    size_t i = 0;
    while (i < patches->length) {
        PatchInfo patch = at_index(patches, i, PatchInfo);
        if (patch.type != PatchJmp2Instr && patch.type != PatchJmp2Epilogue) {
            i++; continue;
        }

        size_t target = epilogue_start;
        if (patch.type == PatchJmp2Instr && patch.instr_idx < ir_fn->instrs.length) {
            target = at_index(&ir_fn->instr_offsets, patch.instr_idx, size_t);
        }
        int32_t jmp_dist = target - (patch.offset + 4);
        write_i32_at(buf, patch.offset, jmp_dist);

        remove_index(patches, i);
    }
}

// return offset into `plt_buf` where the new entry starts
size_t add_plt_slot(Buffer* plt_buf) {
    // the PLT is essentially a jmp table for our extern functions
    // all entries in this table is 16 (0x10) bytes long
    // the first entry is special: is uses the first 3 GOT entries (also special)
    // to jump to the runtime linker code.
    // the rest of the PLT entries work like this:
    // jmp qword ptr [<address of GOT entry>]
    //      - this address is usually written in a position indepent form (relative to $rip)
    //      - if the function was already loaded by the linker then the corresponding
    //          GOT entry contains the actual address of the function and we jump into it.
    //      - because most runtime linkers do lazy-loading the GOT entry will start
    //          initialized to 0, so that this jump is essentially a nop (i.e. it jumps to
    //          the instruction immediately after it)
    // push <entry index>
    // jmp #first_plt_entry
    //      - the two instructions essentially "call" do runtime linker code passing
    //          via stack the index of the function to load (hence the push instruction)

    // first time adding things to plt, have to initialize the first slot
    if (plt_buf->pos == 0) {
        IndexingInfo rip_idx = { .use_rip_relative = true, .disp_size = 4 };
        OperandType mem_op = { .type = Memory };
        // push qword ptr [<GOT entry 1>]
        rip_idx.disp = (got_plt_addr + 0x08) - (plt_addr + plt_buf->pos + 6);
        write_x64_op1(plt_buf, OpPush, mem_op, 64, rip_idx);
        // jmp qword ptr [<GOT entry 2>]
        rip_idx.disp = (got_plt_addr + 0x10) - (plt_addr + plt_buf->pos + 6);
        write_x64_op1(plt_buf, OpJmp, mem_op, 64, rip_idx); 
        // a 4 byte nop to round the entry to 16 bytes
        uint8_t nop_4_bytes[4] = {0x0f, 0x1f, 0x40, 0x00};
        buffer_write(plt_buf, nop_4_bytes, 4);
    }

    size_t plt_offset = plt_buf->pos;

    size_t entry_idx = (plt_offset / 0x10) - 1; // each entry is 16 bytes, first one is special
    size_t entry_got_offset = (3 + entry_idx) * 8; // first 3 GOT entries are special
    IndexingInfo no_indexing = {0};
    IndexingInfo rip_indexing = { .use_rip_relative = true, .disp_size = 4 };

    // jmp qword ptr [<address of GOT entry>]
    OperandType imm_op = { .type = Imm, .imm_size = 4 };
    OperandType mem_op = { .type = Memory };
    rip_indexing.disp = (got_plt_addr + entry_got_offset) - (plt_addr + plt_buf->pos + 6);
    write_x64_op1(plt_buf, OpJmp, mem_op, 64, rip_indexing); 
    // push <entry_idx>
    imm_op.imm = entry_idx;
    write_x64_op1(plt_buf, OpPush, imm_op, 64, no_indexing);
    // jmp #first_plt_entry
    imm_op.imm = 0 - (plt_buf->pos + 5);
    write_x64_op1(plt_buf, OpJmp, imm_op, 64, no_indexing);

    return plt_offset;
}

void ir_write_fn(
    IrFunction* ir_fn,
    Buffer* buf, Buffer* plt_buf, List* plt_entries,
    List* patches,
    IrInfo* ir_info, DebugInfo* dbg_info
) {
    // extern functions only contain entries in the PLT and no actual code of our own
    // the `buf_offset` is set to its corresponding PLT entry, which makes function calls
    // works as intended
    if (ir_fn->is_extern) {
        size_t plt_entry_offset = add_plt_slot(plt_buf);
        ir_fn->buf_offset = plt_entry_offset;
        PltEntry plt_entry = { .name = ir_fn->name, .plt_offset = plt_entry_offset };
        append(plt_entries, plt_entry);
        return;
    }

    // prep function for writing
    memset(ir_fn->reg_in_use, 0, 16);
    memset(ir_fn->reg_dirty, 0, 16);
    if (ir_fn->call_conv == CallConvCyb) {
        int32_t stack_offset = 0x10; // saved rbp and return address from call instruction
        for (size_t i = 0; i < ir_fn->args_type_info.length; i++) {
            IrObj* arg_obj = &at_index(&ir_fn->local_objs, i, IrObj);
            arg_obj->loc = IrLocStack;
            arg_obj->stack_offset = stack_offset;
            stack_offset += runtime_type_info_bits(arg_obj->type_info) / 8;

            // for main, because it's arguments come from the kernel, the pointers
            // for argv are 8-byte aligned. this means there are 4 bytes of padding
            // after argc in the stack.
            if (strcmp(ir_fn->name, "main") == 0 && i == 0) {
                stack_offset = next_align_to(stack_offset, 8);
            }
        }
    }
    else {
        debug_crash("stub: %s\n", call_conv2str[ir_fn->call_conv]);
    }
    ir_fn->instr_offsets = create_list(size_t);
    reserve(&ir_fn->instr_offsets, ir_fn->instrs.length);

    ir_write_fn_body(ir_fn, buf, patches, ir_info, dbg_info);
    // now that we know what registers and how much stack space we'll need
    // go ahead and write the function prologue and epilogue
    ir_write_fn_entry_and_exit(ir_fn, buf, patches, ir_info, dbg_info);
}

void ir_pass_cast_literals(IrInfo* ir_info);
void ir_pass_liveness(IrInfo* ir_info);

RenderResult render_ir_x64(IrInfo* ir_info, DebugInfo* dbg_info) {
    RenderResult render_result = {
        .asm_buffer = new_buffer(0x1000),
        .data_buffer = new_buffer(0x1000),
        .plt_buffer = new_buffer(0x1000),
        .plt_entries = create_list(PltEntry),
        .entry_point = 0,
    };
    Buffer* asm_buffer = &render_result.asm_buffer;
    Buffer* data_buffer = &render_result.data_buffer;
    Buffer* plt_buffer = &render_result.plt_buffer;
    List* plt_entries = &render_result.plt_entries;

    // lets do this in multiple passes because that makes it easier to reason about

    ir_pass_cast_literals(ir_info);
    ir_pass_liveness(ir_info);

    // ### now we're ready for emitting actual cpu instructions ###

    List patches = create_list(PatchInfo);

    // write static objects (data_obj) things that go in the data buffer
    for (size_t i = 0; i < ir_info->data_objs.length; i++) {
        IrObj* obj = &at_index(&ir_info->data_objs, i, IrObj);
        debug_assert(obj->type == IrLiteral, "\n");
        debug_assert(obj->literal_type == IrLiteralString, "\n");

        obj->loc = IrLocMem;
        obj->buf_offset = data_buffer->pos;
        write_str(data_buffer, obj->str_literal);
    }

    // write all the globals to data_buffer
    for (size_t i = 0; i < ir_info->global_objs.length; i++) {
        IrObj* obj = &at_index(&ir_info->global_objs, i, IrObj);
        obj->loc = IrLocMem;
        obj->buf_offset = data_buffer->pos;
        switch (obj->type) {
            case IrLiteral: {
                debug_assert(obj->literal_type == IrLiteralInt, "stub\n");
                switch (type_info_bits(obj->type_info)) {
                    case 8: write_u8(data_buffer, obj->int_literal); break;
                    case 16: write_u16(data_buffer, obj->int_literal); break;
                    case 32: write_u32(data_buffer, obj->int_literal); break;
                    case 64: write_u64(data_buffer, obj->int_literal); break;
                    default: debug_crash("bit_size=%u\n", type_info_bits(obj->type_info));
                }
                break;
            }
            case IrAddrStub: {
                PatchInfo patch = {
                    .type = PatchAddressOf,
                    .offset = data_buffer->pos, .write_in_data_buffer = true,
                    .ref = obj->ref,
                };
                append(&patches, patch);
                data_buffer->pos += 8; // obj of type IrAddrStub is always a pointer
                break;
            }
            case IrVar: {
                debug_assert(obj->is_global_array_hack, "\n");
                write_u64(data_buffer, data_vaddr + data_buffer->pos + 8); // array start right after pointer
                for (size_t i = 0; i < obj->global_array_hack.length; i++) {
                    IrObj elem_obj = at_index(&obj->global_array_hack, i, IrObj);
                    debug_assert(elem_obj.type == IrLiteral, "\n");
                    debug_assert(elem_obj.literal_type == IrLiteralInt, "\n");
                    switch (type_info_bits(base_type_of(obj->type_info))) {
                        case 8: write_u8(data_buffer, elem_obj.int_literal); break;
                        case 16: write_u16(data_buffer, elem_obj.int_literal); break;
                        case 32: write_u32(data_buffer, elem_obj.int_literal); break;
                        case 64: write_u64(data_buffer, elem_obj.int_literal); break;
                        default: debug_crash("bit_size=%u\n", type_info_bits(obj->type_info));
                    }
                }
                break;
            }
            default: debug_crash("stub: %s\n", ir_obj_type2str[obj->type]);
        }
    }

    // write each function into it's own buffer (also need to keep track of all the PatchInfo's)
    List fn_buffers = create_list(Buffer);
    List fn_patches = create_list(List); // a list of PatchInfo's for each function
    List fn_dbg_infos = create_list(DebugInfo);
    for (size_t i = 0; i < ir_info->functions.length; i++) {
        Buffer fn_buf = new_buffer(0x1000);
        List patches = create_list(PatchInfo);
        IrFunction* ir_fn = &at_index(&ir_info->functions, i, IrFunction);
        DebugInfo fn_dbg_info = {0};
        fn_dbg_info.line_addrs = create_list(LineAddr);
        ir_write_fn(
            ir_fn,
            &fn_buf, plt_buffer, plt_entries,
            &patches,
            ir_info, &fn_dbg_info
        );
        append(&fn_buffers, fn_buf);
        append(&fn_patches, patches);
        append(&fn_dbg_infos, fn_dbg_info);
    }

    // join all the functions into a single buffer
    for (size_t i = 0; i < ir_info->functions.length; i++) {
        size_t fn_buf_pos = asm_buffer->pos;

        Buffer* fn_buf = &at_index(&fn_buffers, i, Buffer);
        buffer_write(asm_buffer, fn_buf->data, fn_buf->pos);

        IrFunction* fn = &at_index(&ir_info->functions, i, IrFunction);
        if (!fn->is_extern) fn->buf_offset = fn_buf_pos;

        // add the function patches to the global list
        List* fn_patch_list = &at_index(&fn_patches, i, List);
        for (size_t j = 0; j < fn_patch_list->length; j++) {
            PatchInfo patch = at_index(fn_patch_list, j, PatchInfo);
            patch.offset += fn_buf_pos;
            append(&patches, patch);
        }

        // fix line debug information and add them to the list
        DebugInfo fn_dbg_info = at_index(&fn_dbg_infos, i, DebugInfo);
        for (size_t j = 0; j < fn_dbg_info.line_addrs.length; j++) {
            at_index(&fn_dbg_info.line_addrs, j, LineAddr).addr += fn_buf_pos;
        }
        append_list(&dbg_info->line_addrs, &fn_dbg_info.line_addrs);

        FuncInfo dbg_func = {
            .name = fn->name,
            .start_addr = fn->buf_offset + (fn->is_extern ? plt_addr : code_addr),
            .size = fn_buf->pos,
        };
        append(&dbg_info->funcs, dbg_func);

        free_buffer(fn_buf);
        free_list(fn_patch_list);
        free_list(&fn_dbg_info.line_addrs);
    }
    free_list(&fn_buffers);
    free_list(&fn_patches);
    free_list(&fn_dbg_infos);

    // start up code to call main
    render_result.entry_point = asm_buffer->pos;
    // note: args for main already come in the order we want from the kernel
    PatchInfo _start_call_main_patch = ir_call_stub(asm_buffer, "main");
    append(&patches, _start_call_main_patch);
    // return value from main is in r15. exit with that value
    write_x64_reg_into_reg(asm_buffer, Reg_r15, Reg_rdi, 32);
    write_x64_int_lit_into_reg(asm_buffer, 60, Reg_rax, 32); // 60 = sys_exit
    write_x64_op0(asm_buffer, OpSyscall);
    FuncInfo startup_func = {
        .name = "_start",
        .start_addr = render_result.entry_point + code_addr,
        .size = asm_buffer->pos - render_result.entry_point,
    };
    append(&dbg_info->funcs, startup_func);

    // fix all cross references (address stubs, function calls)
    // note: because patches from inside functions could depend on global
    // variables we have to patch those first
    for (size_t i = 0; i < patches.length; i++) {
        PatchInfo patch = at_index(&patches, i, PatchInfo);
        if (!patch.write_in_data_buffer) continue; // skip stuff not in data_buffer
        debug_assert(patch.ref.type == IrRefData, "%s\n", ir_obj_ref_type2str[patch.ref.type]);
        IrObj ref_obj = at_index(&ir_info->data_objs, patch.ref.idx, IrObj);
        switch (patch.type) {
            case PatchAddressOf: {
                size_t addr = ref_obj.buf_offset + data_vaddr;
                write_u64_at(data_buffer, patch.offset, addr);
                break;
            }
            case PatchJmp2Instr: case PatchJmp2Epilogue: case PatchCall: {
                debug_crash("we shouldn't have any %s at this point\n", patch_type2str[patch.type]);
            }
        }
    }
    for (size_t i = 0; i < patches.length; i++) {
        PatchInfo patch = at_index(&patches, i, PatchInfo);
        if (patch.write_in_data_buffer) continue; // skip stuff in data_buffer

        switch (patch.type) {
            case PatchAddressOf: {
                debug_assert(patch.ref.type != IrRefLocal, "\n");
                PassContext dummy_context = { .info = ir_info, };
                IrObj ref_obj = obj_from_ref(&dummy_context, patch.ref);

                write_u64_at(asm_buffer, patch.offset, ref_obj.buf_offset + data_vaddr);
                break;
            }
            case PatchJmp2Instr: case PatchJmp2Epilogue: {
                debug_crash("we shouldn't have any of these at this point\n");
                break;
            }
            case PatchCall: {
                for (size_t i = 0; i < ir_info->functions.length; i++) {
                    IrFunction ir_fn = at_index(&ir_info->functions, i, IrFunction);
                    if (strcmp(ir_fn.name, patch.fn_name) == 0) {
                        size_t offset_diff = ir_fn.buf_offset - (patch.offset + 4);
                        if (ir_fn.is_extern) offset_diff += (plt_addr - code_addr);
                        write_i32_at(asm_buffer, patch.offset, offset_diff);
                    }
                }
                break;
            }
        }
    }

    return render_result;
}

// cast_literals pass: go through all the literals and coerce them into
// whatever type is needed.
void ir_pass_cast_literals_instr(PassContext* context, IrInstr* instr) {
    switch (instr->type) {
        case IrAlloc: case IrBranch: case IrCondBranch: case IrUnOp: case IrCast:
            break;

        case IrCall: {
            for (size_t i = 0; i < instr->args.length; i++) {
                IrObj* arg = &at_index(&instr->args, i, IrObj);
                IrTypeInfo dst_type = at_index(&instr->callee_arg_types, i, IrTypeInfo);
                change_obj_type_if_needed(arg, dst_type);
            }
            break;
        }
        case IrReturn: {
            IrTypeInfo dst_type = context->fn->return_type;
            change_obj_type_if_needed(&instr->ret_obj, dst_type);
            break;
        }
        case IrAssign: {
            IrObj dst_obj = instr->assign_dst_obj;
            if (dst_obj.type == IrVarRef || dst_obj.type == IrIndexOf) {
                IrTypeInfo dst_type = dst_obj.type_info;
                change_obj_type_if_needed(&instr->assign_src_obj, dst_type);
            }
            break;
        }
        case IrBinOp: {
            uint8_t left_bits = runtime_type_info_bits(instr->left_obj.type_info);
            uint8_t right_bits = runtime_type_info_bits(instr->right_obj.type_info);
            uint8_t bigger_bit_size = (left_bits > right_bits) ? left_bits : right_bits;

            if (instr->left_obj.type == IrLiteral && instr->right_obj.type == IrLiteral) {
                instr->left_obj.type_info.bit_size = bigger_bit_size;
                instr->right_obj.type_info.bit_size = bigger_bit_size;
            }
            else if (instr->left_obj.type == IrLiteral) {
                instr->left_obj.type_info.bit_size = right_bits;
            }
            else if (instr->right_obj.type == IrLiteral) {
                instr->right_obj.type_info.bit_size = left_bits;
            }

            break;
        }
    }
}
void ir_pass_cast_literals(IrInfo* ir_info) {
    call_for_every_instr(ir_pass_cast_literals_instr, ir_info);
}

// liveness pass: for every single variable in a function calculate
// the last instruction where it is referenced/needed.
// this information is stored in `last_instr_used` in `IrObj`. 
void ir_pass_liveness_instr(PassContext* context, IrInstr* instr) {
    IrFunction* ir_fn = context->fn;
    List* locals = &ir_fn->local_objs;
    size_t idx = context->instr_idx;

    if (instr_stores_local_result(*instr)) {
        at_index(locals, instr->local_idx, IrObj).last_instr_used = idx;
    }

    #define set_local_last_instr(locals, obj, idx) { \
        if (obj.type == IrAddrStub && obj.ref.type == IrRefLocal) { \
            at_index(locals, obj.ref.idx, IrObj).last_instr_used = idx; \
        } \
        else if (obj.type == IrVarRef && obj.ref.type == IrRefLocal) { \
            at_index(locals, obj.ref.idx, IrObj).last_instr_used = idx; \
        } \
        else if (obj.type == IrIndexOf && obj.ref.type == IrRefLocal) { \
            at_index(locals, obj.ref.idx, IrObj).last_instr_used = idx; \
        } \
        else if (obj.type == IrIndexOf && obj.index_ref.type == IrRefLocal) { \
            at_index(locals, obj.index_ref.idx, IrObj).last_instr_used = idx; \
        } \
        else if (obj.type == IrMemberOf && obj.ref.type == IrRefLocal) { \
            at_index(locals, obj.ref.idx, IrObj).last_instr_used = idx; \
        } \
    }

    switch (instr->type) {
        case IrAlloc: case IrBranch: break;

        case IrCall: {
            for (size_t i = 0; i < instr->args.length; i++) {
                IrObj obj = at_index(&instr->args, i, IrObj);
                set_local_last_instr(locals, obj, idx);
            }
            break;
        }
        case IrReturn: {
            IrObj obj = instr->ret_obj;
            set_local_last_instr(locals, obj, idx);
            break;
        }
        case IrAssign: {
            IrObj obj = instr->assign_src_obj;
            set_local_last_instr(locals, obj, idx);
            obj = instr->assign_dst_obj;
            set_local_last_instr(locals, obj, idx);
            break;
        }
        case IrCondBranch: {
            IrObj obj = instr->cond_obj;
            set_local_last_instr(locals, obj, idx);
            break;
        }
        case IrUnOp: {
            IrObj obj = instr->un_op_obj;
            set_local_last_instr(locals, obj, idx);
            break;
        }
        case IrBinOp: {
            IrObj obj = instr->left_obj;
            set_local_last_instr(locals, obj, idx);
            obj = instr->right_obj;
            set_local_last_instr(locals, obj, idx);
            break;
        }
        case IrCast: {
            IrObjRef ref = instr->cast_src_ref;
            IrObj dummy_obj = { .type = IrVarRef, .ref = ref };
            set_local_last_instr(locals, dummy_obj, idx);
            break;
        }
    }

    #undef set_local_last_instr
}
void ir_pass_liveness(IrInfo* ir_info) {
    // local variables that never get used would get correctly marked
    // later because some instruction would have to allocate it. this
    // is not true for function arguments. this takes care of that.
    for (size_t i = 0; i < ir_info->functions.length; i++) {
        IrFunction* ir_fn = &at_index(&ir_info->functions, i, IrFunction);
        for (size_t j = 0; j < ir_fn->local_objs.length; j++) {
            IrObj* ir_obj = &at_index(&ir_fn->local_objs, j, IrObj);
            ir_obj->last_instr_used = 0;
        }
    }

    call_for_every_instr(ir_pass_liveness_instr, ir_info);
}
