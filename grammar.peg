# grammar definition for Cyb
# (see "https://en.wikipedia.org/wiki/Parsing_expression_grammar" for information
# on the syntax used in this file)

# TODO:
# - the optional semicolon to separate different statements on the same line
# - using variables as the array size
# - special indexed for loops
# - basically anything that wasn't implement at the time of writing (02/04/2021)

Root <- skip (Import / ExternFn / TopLevelDecl / Function)* eof

# --- file level ---
Import <- KEYWORD_import StringLit

ExternFn <- KEYWORD_extern Identifier FnType

TopLevelDecl
    <- StructDef
     / KEYWORD_fnptr Identifier FnType
     / TypeName VarName (EQUAL Literal)?

StructDef <- KEYWORD_struct Identifier LBRACE VarDecl* RBRACE

Function <- Identifier FnType Block
FnType <- LPAREN FnArgList? RPAREN (COLON TypeName)?
FnArgList <- TypeName VarName (COMMA TypeName VarName)*

# --- block level ---
Block <- LBRACE Statement* RBRACE
Statement
    <- VarDecl
     / VarAssign
     / IfStatement
     / ForStatement
     / WhileStatement
     / KEYWORD_return Expr?
     / KEYWORD_break
     / Expr
StmtBlock <- Statement / Block

VarDecl <- TypeName VarName (EQUAL Expr)?
VarAssign <- Expr AssignOperator Expr
AssignOperator
    <- AMPERSANDEQUAL / ASTERISKEQUAL / CARETEQUAL / EQUAL / EXCLAMATIONMARKEQUAL
     / LARROW2EQUAL / MINUSEQUAL / PERCENTEQUAL / PIPEEQUAL / PLUSEQUAL
     / RARROW2EQUAL / SLASHEQUAL / TILDEEQUAL

IfStatement
    <- KEYWORD_if LPAREN Expr RPAREN StmtBlock (KEYWORD_else (StmtBlock / IfStatement))?

ForStatement
    <- KEYWORD_for LPAREN VarDecl SEMICOLON Expr SEMICOLON Expr RPAREN StmtBlock

WhileStatement
    <- KEYWORD_while LPAREN Expr RPAREN StmtBlock

# --- line level ---
Expr
    <- UnaryOp
     / BinaryOp
     / FnCall
     / Indexing
     / Var

Indexing <- Var LBRACKET Var RBRACKET

EnclosedExpr <- LPAREN Expr RPAREN
InnerOpExpr <- EnclosedExpr / Indexing / Var

UnaryOp <- (LeftUnaryOperator InnerOpExpr) / (InnerOpExpr RightUnaryOperator)
LeftUnaryOperator <- AT / EXCLAMATIONMARK / MINUS / TILDE / AMPERSAND
RgithUnaryOperator <- PLUS2 / MINUS2

InnerBinOpExpr <- UnaryOp / InnerOpExpr

BinaryOp <- InnerBinOpExpr BinaryOperator InnerBinOpExpr
BinaryOperator
     <- AMPERSAND / ASTERISK / CARET / EQUAL2 / LARROW / LARROW2 / LARROWEQUAL
      / MINUS / PERCENT / PERCENTEQUAL / PIPE / PIPE2 / PLUS / RARROW / RARROW2
      / RARROWEQUAL / SLASH

FnCall <- Identifier LPAREN (Expr (COMMA Expr)*)? RPAREN

Var
    <- VarName
     / VarName DOT VarName
     / Literal


# --- valid identifiers for variable names, type names and constants ---
# (got a lot of these from the zig-spec repo)
eof <- !.
Comment <- "//" [^\n]*
skip <- ([ \n] / Comment)*

VarName <- Identifier
TypeName <- AMPERSAND* Identifier (LBRACKET IntegerLit RBRACKET)?
Identifier <- !Keyword [A-Za-z_] [A-Za-z0-9_]* skip

char_escape <- "\\" [nr\\t'"]
string_char <- char_escape / [^\\"\n]
StringLit <- "\"" string_char* "\"" skip
CharLit <- "'" (char_escape / [A-Za-z0-9]) "'" skip

bin_int <- "0b" [01]+
oct_int <- "0o" [0-7]+
dec_int <- [0-9]+
hex_int <- "0x" [0-9a-fA-F]+
IntegerLit <- MINUS? (bin_int / oct_int / dec_int / hex_int) skip

FloatLit <- MINUS? [0-9]+ "." [0-9]+ skip

BooleanLit <- (KEYWORD_true / KEYWORD_false) skip

ArrayLit <- LBRACKET (Expr (COMMA Literal)*)? RBRACKET

StructLit <- LBRACE (StructLitMember (COMMA StructLitMember)*)? COMMA? RBRACE
StructLitMember <- DOT Identifier EQUAL Expr

Literal <- StringLit / CharLit / IntegerLit / FloatLit / BooleanLit / ArrayLit / StructLit

AT                   <- '@'                skip
AMPERSAND            <- '&'      ![=]      skip
AMPERSANDEQUAL       <- '&='               skip
ASTERISK             <- '*'      ![*%=]    skip
ASTERISKEQUAL        <- '*='               skip
CARET                <- '^'      ![=]      skip
CARETEQUAL           <- '^='               skip
COLON                <- ':'                skip
COMMA                <- ','                skip
DOT                  <- '.'      ![*.?]    skip
EQUAL                <- '='      ![>=]     skip
EQUALEQUAL           <- '=='               skip
EXCLAMATIONMARK      <- '!'      ![=]      skip
EXCLAMATIONMARKEQUAL <- '!='               skip
LARROW               <- '<'      ![<=]     skip
LARROW2              <- '<<'     ![=]      skip
LARROW2EQUAL         <- '<<='              skip
LARROWEQUAL          <- '<='               skip
LBRACE               <- '{'                skip
LBRACKET             <- '['                skip
LPAREN               <- '('                skip
MINUS                <- '-'      ![%=>]    skip
MINUSEQUAL           <- '-='               skip
PERCENT              <- '%'      ![=]      skip
PERCENTEQUAL         <- '%='               skip
PIPE                 <- '|'      ![|=]     skip
PIPE2                <- '||'               skip
PIPEEQUAL            <- '|='               skip
PLUS                 <- '+'      ![%+=]    skip
PLUS2                <- '++'               skip
PLUSEQUAL            <- '+='               skip
RARROW               <- '>'      ![>=]     skip
RARROW2              <- '>>'     ![=]      skip
RARROW2EQUAL         <- '>>='              skip
RARROWEQUAL          <- '>='               skip
RBRACE               <- '}'                skip
RBRACKET             <- ']'                skip
RPAREN               <- ')'                skip
SEMICOLON            <- ';'                skip
SLASH                <- '/'      ![=]      skip
SLASHEQUAL           <- '/='               skip
TILDE                <- '~'      ![=]      skip
TILDEEQUAL           <- '~='               skip

end_of_word <- ![a-zA-Z0-9_] skip
KEYWORD_break       <- 'break'       end_of_word
KEYWORD_const       <- 'const'       end_of_word
KEYWORD_continue    <- 'continue'    end_of_word
KEYWORD_defer       <- 'defer'       end_of_word
KEYWORD_else        <- 'else'        end_of_word
KEYWORD_enum        <- 'enum'        end_of_word
KEYWORD_false       <- 'false'       end_of_word
KEYWORD_for         <- 'for'         end_of_word
KEYWORD_if          <- 'if'          end_of_word
KEYWORD_return      <- 'return'      end_of_word
KEYWORD_struct      <- 'struct'      end_of_word
KEYWORD_switch      <- 'switch'      end_of_word
KEYWORD_true        <- 'true'        end_of_word
KEYWORD_while       <- 'while'       end_of_word

KEYWORD_import      <- '#import'     end_of_word

Keyword <- KEYWORD_break / KEYWORD_const / KEYWORD_continue / KEYWORD_defer
         / KEYWORD_else / KEYWORD_enum / KEYWORD_for / KEYWORD_if
         / KEYWORD_return / KEYWORD_struct / KEYWORD_switch / KEYWORD_while
        # true/false are keywords only so we can exclude them from being identifier easily
         / KEYWORD_true / KEYWORD_false
         / KEYWORD_import
