#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/user.h>

#include <errno.h>

#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <string.h>

// call ptrace and print errors if there are any
long checked_ptrace(enum __ptrace_request req, pid_t pid, void* addr, void* data) {
    // PTRACE_PEEK returning -1 is totally valid so we have to clear errno before
    // calling ptrace so we can check it afterwards
    errno = 0;

    long ret_val = ptrace(req, pid, addr, data);

    if (errno != 0) {
        fprintf(stderr, "ptrace call failed with errno=%d\n", errno);
        char tmp[100]; snprintf(tmp, 100, "errno=%d", errno); perror(tmp);
        abort();
    }

    return ret_val;
}

struct user_regs_struct get_regs(pid_t pid) {
    struct user_regs_struct r;
    checked_ptrace(PTRACE_GETREGS, pid, 0, &r);
    return r;
}

uint64_t addr_bytes(pid_t pid, uint64_t addr) {
    return checked_ptrace(PTRACE_PEEKTEXT, pid, (void*) addr, 0);
}

void print_bytes_at(pid_t pid, uint64_t addr) {
    uint8_t bytes[8];
    *((uint64_t*) bytes) = addr_bytes(pid, addr);
    for (size_t i = 0; i < 8; i++) { printf("%02x ", bytes[i]); }
}

void print_stack_frame(pid_t pid, struct user_regs_struct regs, uint32_t extra_lines) {
    uint64_t cursor_addr = regs.rsp - (8 * extra_lines);
    while (cursor_addr < regs.rsp) {
        printf("      0x%08lx :: ", cursor_addr);
        print_bytes_at(pid, cursor_addr); printf("\n");
        cursor_addr += 8;
    }

    printf("rsp-> 0x%08lx :: ", cursor_addr);
    print_bytes_at(pid, cursor_addr); printf("\n");
    cursor_addr += 8;

    if (regs.rbp > regs.rsp) {
        while (cursor_addr < regs.rbp) {
            printf("      0x%08lx :: ", cursor_addr);
            print_bytes_at(pid, cursor_addr); printf("\n");
            cursor_addr += 8;
        }
        printf("rbp-> 0x%08lx :: ", cursor_addr);
        print_bytes_at(pid, cursor_addr); printf("\n");
        cursor_addr += 8;
    }

    for (size_t i = 0; i < extra_lines; i++) {
        printf("      0x%08lx :: ", cursor_addr);
        print_bytes_at(pid, cursor_addr); printf("\n");
        cursor_addr += 8;
    }
}

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("usage: %s <tracee>\n", argv[0]);
        return -1;
    }

    pid_t pid = fork();
    if (pid == 0) {
        checked_ptrace(PTRACE_TRACEME, 0, 0, 0);
        execl(argv[1], argv[1], NULL);
    }
    int32_t wait_status;
    wait(&wait_status);

    char* buf = NULL;
    char last_c;
    while (true) {
        struct user_regs_struct regs = get_regs(pid);
        uint8_t bytes[8]; *((uint64_t*) bytes) = addr_bytes(pid, regs.rip);

        printf("rip=%p -> ", (void*) regs.rip);
        print_bytes_at(pid, regs.rip); printf("\n");

        if (WIFEXITED(wait_status)) {
            printf("tracee terminated normally with exit code %u\n", WEXITSTATUS(wait_status));
        }

        if (WIFSIGNALED(wait_status)) {
            int exit_sig = WTERMSIG(wait_status);
            printf("tracee was terminated by a signal (%u) '%s'\n",
                exit_sig, strsignal(exit_sig));
        }

        printf("> ");
        size_t read = 0;
        getline(&buf, &read, stdin);
        char c = buf[0];
        if (c == '\n') c = last_c;
        switch (c) {
            case 'h':
                printf(
                    "'h': print this message\n"
                    "'q': exit program\n"
                    "'f': print stack frame ('f d' also prints 'd' extra lines, d=1-9)\n"
                    "'s': single step\n"
                    "'r': print registers\n"
                    "'rf': print registers (including segments registers)\n"
                );
                break;
            case 'q': return 0;
            case 'f':
                uint32_t extra_lines = 1;
                if (buf[1] == ' ' && buf[2] != '\0') {
                    if (buf[2] >= '0' && buf[2] <= '9') extra_lines = buf[2] - '0';
                }
                print_stack_frame(pid, regs, extra_lines);
                break;
            case 's':
                checked_ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
                wait(&wait_status);
                break;
            case 'r':
                printf("rax=0x%016llx | rcx=0x%016llx | rdx=0x%016llx | rbx=0x%016llx\n"
                    "rsp=0x%016llx | rbp=0x%016llx | rsi=0x%016llx | rdi=0x%016llx\n",
                    regs.rax, regs.rcx, regs.rdx, regs.rbx,
                    regs.rsp, regs.rbp, regs.rsi, regs.rdi);
                printf(" r8=0x%016llx |  r9=0x%016llx | r10=0x%016llx | r11=0x%016llx\n"
                    "r12=0x%016llx | r13=0x%016llx | r14=0x%016llx | r15=0x%016llx\n",
                    regs.r8, regs.r9, regs.r10, regs.r11,
                    regs.r12, regs.r13, regs.r14, regs.r15);
                if (read > 1 && buf[1] == 'f') {
                    printf(" cs=0x%016llx |  ss=0x%016llx |  ds=0x%016llx\n"
                        " es=0x%016llx |  fs=0x%016llx |  gs=0x%016llx\n",
                        regs.cs, regs.ss, regs.ds, regs.es, regs.fs, regs.gs);
                }
                break;
        }
        last_c = c;

    }
}
