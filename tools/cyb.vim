" syntax file for Cyb programming language

if exists("b:current_syntax")
    finish
endif


syntax keyword cybKeyword if else for while struct return break fnptr extern

syntax match cybCompDirs "#import\|#macro\|#comprun\|#if\|#else\|#elif"

syntax keyword cybType u8 u16 u32 u64 i8 i16 i32 i64 f32 f64 bool

syntax keyword cybBoolean true false

syntax region cybComment start="//" end="$"

syntax region cybBlock start="{" end="}" transparent fold

syntax region cybString start="\"" end="\"" skip="\\\""
syntax region cybChar start="'" end="'" skip="\\'"
" special chars are defined after String/Char and so get priority in highlighting
syntax match cybSpecialChar "\\n\|\\t\|\\r\|\\0\|\\\\\|\\'\|\\\"" containedin=cybString,cybChar

syntax match cybDecNumber display   "\v<\d%(_?\d)*%(\.\.@!)?%(\d%(_?\d)*)?%([eE][+-]?\d%(_?\d)*)?"
syntax match cybHexNumber display "\v<0x\x%(_?\x)*%(\.\.@!)?%(\x%(_?\x)*)?%([pP][+-]?\d%(_?\d)*)?"
syntax match cybOctNumber display "\v<0o\o%(_?\o)*"
syntax match cybBinNumber display "\v<0b[01]%(_?[01])*"

" link our definitions to vim's system
highlight default link cybBinNumber cybNumber
highlight default link cybDecNumber cybNumber
highlight default link cybOctNumber cybNumber
highlight default link cybHexNumber cybNumber

highlight default link cybCompDirs    PreProc
highlight default link cybKeyword     Keyword
highlight default link cybType        Type
highlight default link cybBoolean     Boolean
highlight default link cybComment     Comment
highlight default link cybString      String
highlight default link cybChar        Character
highlight default link cybSpecialChar Special
highlight default link cybNumber      Constant

let b:current_syntax = "cyb"
