#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <fcntl.h>

typedef struct {
    char* filepath;
    char* args[50];
    char* expected_input;
    uint8_t expected_exit_code;
    char* expected_output;
} TestDesc;

TestDesc all_tests[] = {
    {
        .filepath = "tests/test_argc",
        .args = { "1", "2", NULL, },
        .expected_exit_code = 3,
    },
    {
        .filepath = "tests/test_arg_order",
        .expected_exit_code = 1,
    },
    {
        .filepath = "tests/test_array",
    },
    {
        .filepath = "tests/test_binary_ops",
    },
    {
        .filepath = "tests/test_data",
        .expected_output = "I'm a computer, I'm a computery guy\n",
    },
    {
        .filepath = "tests/test_fn_call",
        .expected_exit_code = 4,
    },
    {
        .filepath = "tests/test_for",
        .expected_output = "tests/testtests/testtests/testtests/testtests/test",
    },
    {
        .filepath = "tests/test_if",
        .args = { "2nd", NULL, },
        .expected_exit_code = 11,
    },
    {
        .filepath = "tests/test_lib",
        .expected_output =
            "255\n65535\n1234567890\n12345678901234567890\n"
            "-128\n-32768\n-1234567890\n-1234567890123456789\n"
            "0x0\n0x10abcdef\n",
    },
    {
        .filepath = "tests/test_local_var",
        .expected_exit_code = 7,
    },
    {
        .filepath = "tests/test_logical",
        .expected_output =
            "true = true\nfalse = false\n!true = false\n!false = true\n"
            "true && true = true\ntrue && false = false\nfalse && false = false\nfalse && true = false\n"
            "true || true = true\ntrue || false = true\nfalse || false = false\nfalse || true = true\n"
            "!(true || false) = false\n!(true && false) = true\n"
            "!true || !false = true\n"
            "(true && true) || (false && !true) = true\n"
            "(true && true) && (false && !true) = false\n",
    },
    {
        .filepath = "tests/test_print_num",
        .expected_output = "1234567890\n42\n0\n100\n256\n",
    },
    {
        .filepath = "tests/test_stdin",
        .expected_input = "neko\n",
        .expected_exit_code = 5, // "neko\n"
        .expected_output = "test test\nname? \nhello, neko\ngoodbye :)\n"
    },
    {
        .filepath = "tests/test_struct",
        .expected_output =
            "true <- [after setting to 'true']\n"
            "false <- [a2.no (should be 'false']\n"
            "false <- [after copy from a2 (should be 'false')]\n",
    },
    {
        .filepath = "tests/test_sys_write",
        .expected_exit_code = 20,
        .expected_output = "tests/test_sys_write",
    },
    {
        .filepath = "tests/test_var_sizes",
        .expected_exit_code = 1,
    },
    {
        .filepath = "tests/test_while",
        .expected_output = "ttetestestteststests/tests/ttests/tetests/testests/testtests/test_tests/test_w",
    },
};

bool run_test(TestDesc test) {
    int stdin_pipe[2];
    assert(pipe(stdin_pipe) != -1);
    int stdout_pipe[2];
    assert(pipe(stdout_pipe) != -1);

    if (test.expected_input != NULL) {
        ssize_t written = write(stdin_pipe[1], test.expected_input, strlen(test.expected_input));
        assert(written != -1);
    }

    pid_t child_pid = fork();
    if (child_pid == 0) { // child

        // redirect stdin/stdout to parent process
        assert(dup2(stdin_pipe[0], STDIN_FILENO) != -1);
        assert(dup2(stdout_pipe[1], STDOUT_FILENO) != -1);

        char* argv[50] = { test.filepath };
        for (size_t i = 1; i < 50; i++) argv[i] = test.args[i - 1];

        // in case this test segfaults or something like that, we might want
        // to examine it, to print better information. so lets trace the test
        ptrace(PTRACE_TRACEME, 0, 0, 0);

        execv(test.filepath, argv);

        // if exec returns, then something went wrong
        fprintf(stderr, "errno=%u: %s\n", errno, strerror(errno));
        abort();
    }

    int wait_status = 0;
    // traced child programs get stopped with signal SIGTRAP at the first instruction
    waitpid(child_pid, &wait_status, 0);
    ptrace(PTRACE_CONT, child_pid, 0, 0);

    do {
        waitpid(child_pid, &wait_status, 0);

        bool got_signaled = false;
        int sig;
        if (WIFSTOPPED(wait_status)) { got_signaled = true; sig = WSTOPSIG(wait_status); }
        if (WIFSIGNALED(wait_status)) { got_signaled = true; sig = WTERMSIG(wait_status); }
        if (got_signaled) {
            struct user_regs_struct regs; ptrace(PTRACE_GETREGS, child_pid, 0, &regs);
            fprintf(stderr, "signaled: %u, '%s' @ address=0x%llx; ", sig, strsignal(sig), regs.rip);
            return false;
        }
    } while (!WIFEXITED(wait_status));

    if (test.expected_output != NULL) {
        char read_buf[0x1000];
        ssize_t bytes_read = read(stdout_pipe[0], read_buf, 0x1000);
        if (bytes_read == -1) perror("\n");
        assert(bytes_read != -1);
        read_buf[bytes_read] = '\0';

        if (strcmp(test.expected_output, read_buf) != 0) {
            fprintf(stderr, "output was:\n'%s'\nexpected:\n'%s'\n ",
                read_buf, test.expected_output);
            return false;
        }
    }

    if (WEXITSTATUS(wait_status) != test.expected_exit_code) {
        fprintf(stderr, "exit_code was %u (expected %u); ",
            WEXITSTATUS(wait_status), test.expected_exit_code);
        return false;
    }

    return true;
}

int main() {
    size_t num_tests = sizeof(all_tests) / sizeof(TestDesc);

    size_t num_passed = 0;
    for (size_t i = 0; i < num_tests; i++) {
        TestDesc test = all_tests[i];
        printf("running '%s'... ", test.filepath);
        fflush(stdout);
        bool passed = run_test(test);
        printf("%s\n", passed ? "passed":"failed");
        if (passed) num_passed += 1;
    }

    printf("[%lu/%lu] tests passed\n", num_passed, num_tests);
}
